defmodule Math do
  #  module is like package in python or java

  def div(_, 0) do
    {:error, "Cannot divide by zero"}
  end

  def div(x, y) do
    {:ok, "value is #{x/y}"}
  end

end

IO.inspect Math.div(1, 0) # :error
IO.inspect Math.div(5, 3) # :ok

