package tutorial3

import org.apache.http.HttpHost
import org.apache.http.auth.AuthScope
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.client.CredentialsProvider
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.serialization.StringDeserializer
import org.elasticsearch.action.bulk.BulkRequest
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestClientBuilder
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.common.xcontent.XContentType
import org.slf4j.LoggerFactory
import java.lang.IllegalStateException
import java.time.Duration
import java.util.*

class ElasticSearchBunchConsumer {


    companion object {
        fun createConsumer(topic: String): KafkaConsumer<String, String> {
            val bootstrapServer = "127.0.0.1:9092"
            val groupId = "kafka-demo-elasticsearch"

            // create consumer config
            val properties = Properties()
            properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer)
            properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer::class.java.name)
            properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer::class.java.name)
            properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId)
            properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
            // disable autocommit offsets
            properties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
            properties.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "1000")

            // create consumer
            val consumer: KafkaConsumer<String, String> = KafkaConsumer(properties)
            consumer.subscribe(listOf(topic))
            return consumer
        }

        fun createClient(): RestHighLevelClient {

            val hostname: String = "talk-to-nobody-2118385836.ap-southeast-2.bonsaisearch.net"
            val username: String = "9okvregv82"
            val password: String = "fmew9uavmc"

            val credentialsProvider: CredentialsProvider = BasicCredentialsProvider()
            credentialsProvider.setCredentials(AuthScope.ANY, UsernamePasswordCredentials(username, password))

            val builder: RestClientBuilder = RestClient
                    .builder(HttpHost(hostname, 443, "https"))
                    .setHttpClientConfigCallback { httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider) }

            return RestHighLevelClient(builder)
        }
    }

}



fun main() {
    val logger = LoggerFactory.getLogger(ElasticSearchBunchConsumer::class.java)
    val esClient = ElasticSearchBunchConsumer.createClient()
    val consumer = ElasticSearchBunchConsumer.createConsumer("twitter_tweets")
    while (true) {
        val records: ConsumerRecords<String, String> =
                consumer.poll(Duration.ofMillis(100))

        logger.info("Received: {} records", records.count())

        val bulkRequest = BulkRequest()
        for (record in records) {
            // kafka way to get id
//            val id = "${record.topic()}_${record.partition()}_${record.offset()}"

            // id from twitter
            try {
                val id: String? = record?.let { extractIdFromString(record.value()) }
                id?.let {
                    val indexRequest: IndexRequest = IndexRequest("twitter", "tweets", id)
                            .source(record.value(), XContentType.JSON)

                    bulkRequest.add(indexRequest)
                }

            } catch (e: IllegalStateException) {

            }



//            val response: IndexResponse = esClient.index(indexRequest, RequestOptions.DEFAULT)
//            val id = response.id
//            logger.info(response.id)

//            Thread.sleep(1_000)
        }

        if (records.count() > 0) {
            val result = esClient.bulk(bulkRequest, RequestOptions.DEFAULT)
            logger.info("Committing offsets...")
            consumer.commitSync()
            logger.info("Offsets has been committed")
        }

        Thread.sleep(100)
    }

//    esClient.close()
}