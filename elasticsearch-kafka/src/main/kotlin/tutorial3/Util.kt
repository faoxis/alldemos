package tutorial3

import com.google.gson.JsonParser

val jsonParser = JsonParser()
fun extractIdFromString(twitterJson: String): String? {
    return twitterJson?.let {
        jsonParser
                .parse(twitterJson)
                .asJsonObject
                .get("id_str")
                ?.asString
    }
}
