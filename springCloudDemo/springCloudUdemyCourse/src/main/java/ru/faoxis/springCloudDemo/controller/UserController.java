package ru.faoxis.springCloudDemo.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.faoxis.springCloudDemo.domain.User;

import javax.validation.Valid;

@RestController
public class UserController {

    public User save(@Valid @RequestBody User user) {
        return user.setId(1);
    }

}
