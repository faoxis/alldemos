package ru.faoxis.springCloudDemo.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@Accessors(chain = true)
public class User {

    private long id;

    private String name;

    @Min(18)
    @Max(60)
    private String age;

}
