package functionalProgrammingInScala.chapter3FunctionalDataStructures

import org.scalatest._

class MyListSpec extends FlatSpec with Matchers {

  "A MyList" should "be able to add all its numbers" in {
    MyList.sum(MyList(1, 2, 3)) should be (6)
    MyList.sum(MyList()) should be (0)
    MyList.sum(MyList(1, 2, 3, 4, 5, 6, 7, 8, 9)) should be (45)
  }

  "A MyList" should "be able to product all its numbers" in {
    MyList.product(MyList(1, 2, 3)) should be (6.0)
    MyList.product(MyList()) should be (1.0)
    MyList.product(MyList(1, 2, 3, 4, 5, 0, 7, 8, 9)) should be (0.0)
  }

  "A tail method" should "be able to get elements of list in" in {
    val tail = MyList.tail(MyList(1,2,3,4,5))
    MyList.sum(tail) should be (14)
  }

  "A setHead method" should "be able to replace the fist element of MyList" in {
    val updatedList = MyList.setHead(0, MyList(1, 1, 1))
    MyList.sum(updatedList) should be (2)
  }

  "A drop method" should "be able to drop first n elements" in {
    val listAfterDrop = MyList.drop(3, MyList(1, 1, 1, 1))
    MyList.sum(listAfterDrop) should be (1)
  }

  "A dropWhile method" should "be able to drop elements from MyList while function returns true" in {
    val list = MyList(1, 1, 1, 2, 2, 2)
    val three2s = MyList.dropWhile(list, 1 == (_ : Int))
    MyList.sum(three2s) should be (6)
  }

  "A init method" should "be able to drop list element from MyList" in {
    val list = MyList(1, 2, 13)
    val updatedList = MyList.init(list)
    MyList.sum(updatedList) should be (3)
  }

}
