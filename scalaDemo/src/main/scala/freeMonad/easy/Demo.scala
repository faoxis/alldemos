package freeMonad.easy

import language.higherKinds

object Demo extends App {

  sealed trait Interact[A]
  case class Tell(message: String) extends Interact[Unit]
  case class Ask(prompt: String) extends Interact[String]

  trait Monad[M[_]] {
    def pure[A](a: A): M[A]
    def flatMap[A, B](ma: M[A])(f: A => M[B]): M[B]
  }

  sealed trait Free[F[_], A] {
    def flatMap[B](f: A => Free[F, B]): Free[F, B] =
      this match {
        case Return(a)  => f(a)
        case Bind(i, k) => Bind(i, k andThen (_ flatMap f))
      }

    // if we have flatMap we can have map as well
    def map[B](f: A => B): Free[F, B] =
      flatMap(a => Return(f(a)))

    def foldMap[G[_]](f: F ~> G)(implicit monad: Monad[G]): G[A] =
      this match {
        case Return(a)  => monad.pure(a)
        case Bind(i, k) =>
          monad.flatMap(f(i))(a => k(a).foldMap(f))
      }
  }
  case class Return[F[_], A](a: A) extends Free[F, A]
  case class Bind[F[_], I, A](i: F[I], k: I => Free[F, A]) extends Free[F, A]

//  import scala.language.implicitConversions

  implicit def liftIntoFree[F[_], A](fa: F[A]): Free[F, A] =
    Bind(fa, (a: A) => Return(a))

  val prog: Free[Interact, Unit] = for {
    firstname <- Ask("What's your firstname?")
    surname   <- Ask("What's your surname?")
    _         <- Tell(s"Hello, $firstname $surname")
  } yield ()

  trait ~>[F[_], G[_]] {
    def apply[A](fa: F[A]): G[A]
  }

  type Id[A] = A

  object Console extends (Interact ~> Id) {
    def apply[A](i: Interact[A]): Id[A] =
      i match {
        case Tell(message) => println(message)
        case Ask(prompt)   =>
          println(prompt)
          scala.io.StdIn.readLine()
      }
  }

  implicit val idMonad: Monad[Id] = new Monad[Id] {
    def pure[A](a: A): Id[A] = a
    def flatMap[A, B](a: Id[A])(f: A => Id[B]) = f(a)
  }

  prog.foldMap(Console)

}
