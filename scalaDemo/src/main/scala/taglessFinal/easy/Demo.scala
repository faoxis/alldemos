package taglessFinal.easy

import cats.Monad
import cats.data.EitherT
import cats.effect.IO

import language.higherKinds
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import cats.implicits._

import scala.util.Try

object Demo extends App {

  trait Calculator[F[_]] {
    def sum(a: Int, b: Int): F[Either[Throwable, Int]]
    def minus(a: Int, b: Int): F[Either[Throwable, Int]]
  }

  object Calculator {
    def apply[F[_]](implicit calculator: Calculator[F]): Calculator[F] = calculator

    implicit val futureCalculator: Calculator[Future] = new Calculator[Future] {
      override def sum(a: Int, b: Int) =
          Future {
            Try {
//              throw new RuntimeException("plus is wrong")
              a + b
            }.toEither
          }
      override def minus(a: Int, b: Int) =
        Future {
          Try {
//            throw new RuntimeException("minus is wrong")
            a - b
          }.toEither
        }
    }

    implicit val ioCalculator: Calculator[IO] = new Calculator[IO] {
      override def sum(a: Int, b: Int): IO[Either[Throwable, Int]] = IO { Try(a + b).toEither }

      override def minus(a: Int, b: Int): IO[Either[Throwable, Int]] = IO { Try(a - b).toEither }
    }

  }

  def getPlusAndMinus[F[_]: Monad: Calculator](a: Int, b: Int): F[Either[Throwable, Int]] =
    (for {
      sum <- EitherT(Calculator[F].sum(a, b))
      res <- EitherT(Calculator[F].minus(sum, b))
    } yield res).value

  getPlusAndMinus[Future](1, 2).onComplete(println)
  Thread.sleep(1000)

  println(getPlusAndMinus[IO](1, 2).unsafeRunSync())

}
