package taglessFinal.miciek

object model {
  final case class Collection(videos: List[String])
  final case class InfluencerItem(views: Int, likes: Int, dislikes: Int, comments: Int)
  final case class CollectionStats(impression: Int, engagements: Int, score: Int)

}