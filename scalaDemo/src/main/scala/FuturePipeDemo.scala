import cats.data.{Validated, ValidatedNec, ValidatedNel}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object FuturePipeDemo extends App {



  sealed trait BorrowerValidation {
    def errorMessage: String
  }

  case object ApplicationIdSize extends BorrowerValidation {
    override def errorMessage: String = "Wrong applicationId size"
  }

  case object FirstNameSize extends BorrowerValidation {
    override def errorMessage: String = "Wrong firstName size"
  }

  case object LastNameSize extends BorrowerValidation {
    override def errorMessage: String = "Wrong lastName size"
  }

  case object MiddleNameSize extends BorrowerValidation {
    override def errorMessage: String = "Wrong middleName size"
  }

  case object EmailFormat extends BorrowerValidation {
    def errorMessage: String = "Email format is wrong"
  }

  case object PhoneFormat extends BorrowerValidation {
    override def errorMessage: String = "Phone number is wrong"
  }


  sealed trait BorrowerValidator {

    def validateApplicationId(appId: String): Either[BorrowerValidation, String] =
      Either.cond(appId.length <= 3, appId, ApplicationIdSize)

    def validateFirstName(firstName: String): Either[BorrowerValidation, String] =
      Either.cond(firstName.length <= 5, firstName, FirstNameSize)


  }
  object BorrowerValidator extends BorrowerValidator

  case class Borrower(appId: String, name: String, somethingElse: String)

  def validateBorrower(borrower: Borrower): Either[BorrowerValidation, Borrower] =
    for {
      _ <- BorrowerValidator.validateApplicationId(borrower.appId)
      _ <- BorrowerValidator.validateFirstName(borrower.name)
    } yield borrower

  validateBorrower(Borrower("Имя", "Фам", "Еще"))
  validateBorrower(Borrower("Ифафыамя", "Фа223милия", "Еще")) match {
    case Right(res) => println(res)
    case Left(err)  => println(err)
  }



  import cats.Applicative
  import cats.data.ValidatedNel
  import cats.syntax.validated._

  sealed trait FormValidator {

    type ValidationResult[A] = ValidatedNel[BorrowerValidation, A]

    private def validateApplicationId(appId: String): ValidationResult[String] =
      if (appId.length <= 3) appId.validNel
      else ApplicationIdSize.invalidNel

    private def validateFirstName(firstName: String): ValidationResult[String] =
      if (firstName.length <= 5) firstName.validNel
      else FirstNameSize.invalidNel


    def validateForm(borr: Borrower) = {
      Applicative[ValidationResult]
        .map2(validateApplicationId(borr.appId), validateFirstName(borr.name))((_, _) => borr)
    }

  }
  object FormValidator extends FormValidator

  FormValidator.validateForm(Borrower("Имя", "Фам", "Еще"))
  println(FormValidator.validateForm(Borrower("Ифафыамя", "Фа223милия", "Еще")).toEither)
//    case Right(res) => println(res)
//    case Left(err)  => println(err)
//  }

  import cats.Applicative
  import cats.data.ValidatedNel
  import cats.syntax.validated._

  type Error = String
  // use a NonEmptyList on the left side
  type ErrorOr[A] = ValidatedNel[Error, A]

  // using invalidNel is a shortcut for
  // NonEmptyList.of("empty first name").invalid[String]
  val validName = "empty first name".validNel
  val errorOrFirstName = "empty first name".invalidNel
  val errorOrLastName = "empty last name".invalidNel
  val errorsOrFullName: ErrorOr[String] =
    Applicative[ErrorOr].map3(validName, errorOrFirstName, errorOrLastName)(_ + " " + _ + " " + _)

  println(errorsOrFullName)



}
