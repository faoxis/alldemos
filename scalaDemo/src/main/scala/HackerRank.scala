import scala.annotation.tailrec

object HackerRank {

  // Complete the extraLongFactorials function below.
  // Complete the extraLongFactorials function below.
  def extraLongFactorials(n: Int): BigInt = {
    @tailrec
    def calcWithAcc(n: Int, acc: BigInt): BigInt =
    n match {
    case 1 => acc
    case _ => calcWithAcc(n - 1, acc * n)
  }
    calcWithAcc(n, 1)
  }

  def main(args: Array[String]) {
    val stdin = scala.io.StdIn

    val n = stdin.readLine.trim.toInt

    println(extraLongFactorials(n))
  }

}
