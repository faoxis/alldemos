package catsDocs.effect

import java.io._

import cats.effect.{ContextShift, IO, Resource}
import cats.implicits._

import scala.reflect.io.File

object IOEffectDemo extends App {

  def inputStream(f: File): Resource[IO, FileInputStream] =
    Resource.make {
      IO(new FileInputStream(f))
    } { inputStream =>
      IO(inputStream.close()).handleErrorWith(_ => IO.unit)
    }

  def outputStream(f: File): Resource[IO, FileOutputStream] =
    Resource.fromAutoCloseable(IO(new FileOutputStream(f)))

  def inputOutputStream(in: File, out: File): Resource[IO, (FileInputStream, FileOutputStream)] =
    for {
      input <- inputStream(in)
      output <- outputStream(out)
    } yield (input, output)

  def transmit(inputStream: InputStream, outputStream: OutputStream, buffer: Array[Byte], acc: Long): IO[Long] =
    for {
      amount <- IO(inputStream.read(buffer, 0, buffer.size))
      count <- if (amount > -1) IO(outputStream.write(buffer, 0, amount)) >> transmit(inputStream, outputStream, buffer, acc + amount)
               else IO.pure(acc)
    } yield count

  def transfer(in: InputStream, out: OutputStream): IO[Long] = {
    for {
      buffer <- IO.pure(new Array[Byte](100))
      count <- transmit(in, out, buffer, 0)
    } yield count
  }

  // simple version
  def race[A, B](lh: IO[A], rh: IO[B])
                (implicit cs: ContextShift[IO]): IO[Either[A, B]]

  def copy(inFile: File, outFile: File): IO[Long] =
    inputOutputStream(inFile, outFile).use { case (in, out) =>
      transfer(in, out)
    }

  val pathToThisFile = "scalaDemo/src/main/scala/catsDocs/effect/"
  copy(File(s"$pathToThisFile + example.txt"),
    File(s"$pathToThisFile + example2.txt"))

}
