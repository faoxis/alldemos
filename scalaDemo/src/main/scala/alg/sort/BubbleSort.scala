package alg.sort

import scala.annotation.tailrec

object BubbleSort extends App {

  def sortByBubble(mass: Array[Int]): Unit = {
    for (i <- 0 until mass.length) {

      var minIndex = i
      var minElem = mass(i)
      for (j <- i until mass.length) {
        if (mass(j) < minElem) {
          minElem =
        }
      }
    }
  }

  def findMin(mass: Array[Int], startIndex: Int): (Int, Int) = {

    type Element = Int
    type Index = Int

    @tailrec
    def findMinWithAcc(min: (Element, Index), mass: Array[Index], currentIndex: Index): (Element, Index) = {
      if (currentIndex == mass.length - 1) min
      else {
        val newMin = if (mass(currentIndex) >= min._1) min
                     else (mass(currentIndex), currentIndex)
        findMinWithAcc(newMin, mass, currentIndex + 1)
      }
    }

//    findMinWithAcc(())
  }

}
