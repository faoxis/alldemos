package alg.sort

import java.util

object InsertSort extends App {

  def insertSort(mass: Array[Int]) = {
    for (i <- 1 until mass.length) {
      if (mass(i - 1) > mass(i)) {
        for (j <- i until 0 by -1) {
          if (mass(j - 1) > mass(j)) {
            val tmp = mass(j)
            mass(j) = mass(j-1)
            mass(j - 1) = tmp
          }
        }
      }
    }
  }

  val testArray = Array(1, 3, 5, 2, 4)
  insertSort(testArray)
  testArray.foreach(println)

//  for (i <- 10 until 1 by -1) { println(i) }

}
