package alg.tree

object BinaryTree {
  def apply(v: Int): BinaryTree = NotEmptyBinaryTree(EmptyBinaryTree, v, EmptyBinaryTree)

  implicit class AddBinaryTree(self: BinaryTree) {
    def add(newValue: Int): BinaryTree = {
      self match {
        case NotEmptyBinaryTree(left, value, right) =>
          if (value < newValue) left.add(newValue)
          else right(newValue)
      }
    }
  }
}

trait BinaryTree
case class NotEmptyBinaryTree(left: BinaryTree, value: Int, right: BinaryTree) extends BinaryTree
object EmptyBinaryTree extends BinaryTree

object BinaryTreeDemo extends App {

}

