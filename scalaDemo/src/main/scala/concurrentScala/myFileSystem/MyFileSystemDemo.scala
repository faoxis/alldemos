package concurrentScala.myFileSystem

import java.io.File
import java.util.concurrent.{ConcurrentHashMap, ConcurrentMap}
import java.util.concurrent.atomic.AtomicReference

import org.apache.commons.io.FileUtils

import scala.collection.JavaConverters._
import FSState._

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext

object MyFileSystemDemo extends App {
  val fileSystem = new MyFileSystem(".")
  fileSystem.files.foreach { entry => println(entry._1) }
  fileSystem.delete("test.txt")

  Thread.sleep(100)
}

class MyFileSystem(val root: String) {

  class Entry {
    val state = new AtomicReference[State](Idle)
  }

  val files = new ConcurrentHashMap[String, Entry]().asScala
  for (file <- FileUtils.iterateFiles(new File(root), null, false).asScala if !file.isDirectory) {
    files.put(file.getName, new Entry)
  }

  @tailrec
  private def prepareForDelete(entry: Entry): Boolean = {
    val currentState = entry.state.get
    currentState match {
      case Deleting => false
      case Creating => false
      case c: Copying => false
      case Idle => {
        if (entry.state.compareAndSet(currentState, Deleting)) true
        else prepareForDelete(entry)
      }
    }
  }

  def delete(filename: String): Unit = {
    files.get(filename) match {
      case None => println(s"There is no file $filename")
      case Some(entry) => ExecutionContext.global.execute { () =>
        if (prepareForDelete(entry)) {
          if (FileUtils.deleteQuietly(new File(filename))) {
            files.remove(filename)
            println(s"file $filename has been successfully deleted")
          }
        }
      }
    }
  }

}





