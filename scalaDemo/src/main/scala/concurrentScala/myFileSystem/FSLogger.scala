package concurrentScala.myFileSystem

import java.util.concurrent.LinkedBlockingQueue

class FSLogger {
  private val messages = new LinkedBlockingQueue[String]

  val logger = new Thread {
    setDaemon(true)
    override def run() {
      while (true) {
        val msg = messages.take()
        log(msg)
      }
    }
  }

  def logMessage(msg: String) = messages.add(msg)

  logger.start()
  def log(msg: String) {
    println(s"${Thread.currentThread.getName}: $msg")
  }
}

