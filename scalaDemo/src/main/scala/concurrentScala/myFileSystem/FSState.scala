package concurrentScala.myFileSystem

object FSState {
  sealed trait State
  object Idle extends State
  object Creating extends State
  class Copying(val n: Int) extends State
  object Deleting extends State
}
