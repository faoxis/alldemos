package advancedScalaWithCats.typeClasses

import cats.Show
import cats.instances.all._
import cats.syntax.all._

object TypeClassesWithCatsDemo extends App {

  val showInt: Show[Int] = Show.apply[Int]
  val showString: Show[String] = Show.apply[String]

  println(showInt.show(42))
  println(42.show)

}
