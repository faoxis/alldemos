package advancedScalaWithCats.typeClasses

case class Person(name: String, age: Int)

sealed trait Json
final case class JsObject(get: Map[String, Json]) extends Json
final case class JsString(data: String) extends Json
final case class JsInt(data: Int) extends Json

// case class
trait JsonWriter[A] {
  def write(value: A): Json
}

// type classes instances
object JsonWriterInstances {

  implicit val stringJsonWriter: JsonWriter[String] = value => JsString(value)
  implicit val intJsonWriter: JsonWriter[Int] = value => JsInt(value)
  implicit val jsObject: JsonWriter[Person] = person => JsObject(Map(
    "name" -> JsString(person.name),
    "age" -> JsInt(person.age)
  ))
}

// syntax - возможное применение type классов
object JsonWriterSyntax {
  implicit class JsonWriterPersonOps(person: Person) {
    def toJson()(implicit jsonWriter: JsonWriter[Person]): Json = jsonWriter.write(person)
  }

  implicit class JsonWriterIntOps(number: Int) {
    def toJson()(implicit jsonWriter: JsonWriter[Int]): Json = jsonWriter.write(number)
  }

  implicit class JsonWriterStringOps(string: String) {
    def toJson()(implicit jsonWriter: JsonWriter[String]): Json = jsonWriter.write(string)
  }
}

object JsonSerializatorDemo extends App {
  import JsonWriterInstances._
  import JsonWriterSyntax._

  println(Person("Petr", 39).toJson)
  println(39.toJson)
  println("Petr".toJson())
}



