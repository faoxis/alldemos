package advancedScalaWithCats.monadTransformation

import cats.data.OptionT

object SecondDemo extends App {

  // Если нужно сделать трасформацию с монадой к конструктором
  type Error = String
  type ErrorOr[A] = Either[Error, A]
  type ErrorOptionOr[A] = OptionT[ErrorOr, A]

  // Так не скомпилируется
//  type ErrorOptionOr[A] = OptionT[EitherT, A]

  // Таким образом можно наращивать обертки над монадами
  import scala.concurrent.Future
  import cats.data.{EitherT, OptionT}

  type FutureEither[A] = EitherT[Future, String, A]
  type FutureEitherOption[A] = OptionT[FutureEither, A] // 3 упаковки

  import scala.concurrent.ExecutionContext.Implicits.global
  import cats.instances.future._
  import cats.syntax.applicative._

  val answer: FutureEitherOption[Int] =
    for {
      a <- 10.pure[FutureEitherOption]
      b <- 32.pure[FutureEitherOption]
    } yield a + b

  Thread.sleep(10)
  println(answer)


}
