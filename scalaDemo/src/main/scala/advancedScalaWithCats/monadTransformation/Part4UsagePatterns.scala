package advancedScalaWithCats.monadTransformation

import cats.data.Writer

import scala.util.Try
import cats.instances.list._

object Part4UsagePatterns extends App {

  // Обычно трансформации происходят в рамках некоторого модуля.
  // Когда мы возращаем значение из модуля, мы распаковываем его.
  // Пример.
  type Error     = String
  type Logged[A] = Writer[List[Error], A]

  def parseNumber(str: String): Logged[Option[Int]] =
    Try(str.toInt).toOption match {
      case Some(number) => Writer(List("Successfully parsed"), Some(number))
      case None         => Writer(List(s"Failled to parsed string $str"), None)
    }

  def addNumbers(a: String, b: String, c: String): Logged[Option[Int]] = {

    import cats.data.OptionT
    import cats.syntax.applicative._

    type OptionInWriter[A] = OptionT[Logged, A]

    val result = for {
      a <- OptionT(parseNumber(a))
      b <- OptionT(parseNumber(b))
      c <- OptionT(parseNumber(c))
    } yield a + b + c

    val result2 = OptionT(parseNumber(a))
      .flatMap { x =>
        OptionT(parseNumber(b)).flatMap { y =>
          OptionT(parseNumber(c)).map { z =>
            x + y + z
          }
        }
      }


    result2.value
  }

  println(addNumbers("1", "2", "3"))
}
