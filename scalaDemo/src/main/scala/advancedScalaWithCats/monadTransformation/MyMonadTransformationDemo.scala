package advancedScalaWithCats.monadTransformation

import cats.data.Kleisli

import scala.language.higherKinds

object MyMonadTransformationDemo extends App {

  import cats._



  Option(32).flatMap(x => Option(x))

  def myFlatMap[A, B](m: Option[A], kleisli: A => Option[B]): Option[B] = m.flatMap(kleisli)



  def myFlatMapList[A, B](m: List[Option[A]])(kleisli: A => Option[B]) =
    m.flatMap(x => List(x.flatMap(kleisli)))


}
