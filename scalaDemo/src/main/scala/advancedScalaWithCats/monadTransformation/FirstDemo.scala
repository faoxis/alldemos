package advancedScalaWithCats.monadTransformation




object FirstDemo extends App {

  // One of the first monad transformation
  import cats.data.OptionT
  type ListOption[A] = OptionT[List, A]

  import cats.Monad
  import cats.instances.list._
  import cats.syntax.applicative._



  val a = 10.pure[ListOption]
  val b = 32.pure[ListOption]



  println(a.flatMap(x => b.map(y => x + y)))
  println(for {
    x <- a
    y <- b
  } yield (x + y))

//  val res = a flatMap { x =>
//    b map { y =>
//      x + y
//    }
//  }
//  println(res)


}
