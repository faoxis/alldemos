package advancedScalaWithCats.monadTransformation

import cats.data.OptionT

object Part3ConstructingAndUnpackingInstances extends App {

  // Как мы видели раннее, можно неявно передавать тип через pure (и таким образом запаковывать значение)
  import cats.syntax.either._
  import cats.syntax.option._
  import cats.syntax.applicative._
  import cats.instances.either._

  type ErrorOr[A] = Either[String, A]
  type OptionInEither[A] = OptionT[ErrorOr, A]

  val stack1 = 123.pure[OptionInEither]
  val stack2 = OptionT[ErrorOr, Int](
    123.some.asRight[String]
  )

  // Для распаковки  необходимо воспользоваться свойством value
  val unpackedStack1 = stack1.value
  println(stack1) // OptionT(Right(Some(123)))
  println(unpackedStack1) // Right(Some(123))


  // Часто нам требуется более, чем одна распаковка
  import cats.instances.vector._
  import cats.data.{Writer, EitherT, OptionT}

  type Error = String
  type Logged[A] = Writer[Vector[Error], A]
  type LoggedFallable[A] = EitherT[Logged, Error, A]
  type LoggedFallableOption[A] = OptionT[LoggedFallable, A]

  val packed = 123.pure[LoggedFallableOption]
  println(packed) // OptionT(EitherT(WriterT((Vector(),Right(Some(123))))))

  val partiallyPacked = packed.value
  println(partiallyPacked) // EitherT(WriterT((Vector(),Right(Some(123)))))

  val completelyUnpacked = partiallyPacked.value
  println(completelyUnpacked) // WriterT((Vector(),Right(Some(123))))

  // Таким образом мы получаем полностью распакованное значение (на WriterT можно не обращать внимание)
  // Таким образом, когда мы ходим вызывать flatMap для всех вложенных монад, мы обращаемся к запакованному значению.
  // Когда нам такой функционал больше не нужен, мы делаем распаковку.
}
