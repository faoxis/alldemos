package advancedScalaWithCats.monadTransformation

import cats.data.EitherT
import cats.syntax.applicative._
import cats.instances.future._

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object Part5Exercise extends App {

  type Response[A] = EitherT[Future, String, A]

  val powerLevels = Map(
    "Jazz" -> 6,
    "Bumblebee" -> 8,
    "Hot Rod" -> 10
  )
  def getPowerLevel(autobot: String): Response[Int] = {
    val result = Future { powerLevels.get(autobot) match {
      case Some(number) => Right(number)
      case None => Left(s"Can't get connect to $autobot")
    }}
    EitherT(result)
  }

  def canSpecialMove(
                      ally1: String,
                      ally2: String
                    ): Response[Boolean] =
    for {
      x <- getPowerLevel(ally1)
      y <- getPowerLevel(ally2)
    } yield x + y > 15

  def tacticalReport(
                      ally1: String,
                      ally2: String
                    ): String =
    Await.result (
      canSpecialMove(ally1, ally2).value,
      1.second
    ) match {
      case Left(msg) =>
        s"Commes error message $msg"
      case Right(true) =>
        s"$ally1 and $ally2 are really bigger then 15."
      case Right(false) =>
        s"No. $ally1 and $ally2 are not bigger then 15."
    }


  println(tacticalReport("Bumblebee", "Hot Rod"))


}
