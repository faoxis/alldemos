package advancedScalaWithCats.monad

import scala.language.higherKinds
import cats.instances.all._
import cats.Monad
import cats.Functor

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class MyContainer[A](value: A) {
  import MyContainer._

  def flatMap[B](f: A => MyContainer[B]): MyContainer[B] = f(value)
//  def map[B](f: A => B): MyContainer[B] = (pure compose f(value))(value)

}



object MyContainer {
  def pure[A](value: A): MyContainer[A] = new MyContainer(value)
}

object MonadDemo extends App {
  val futureMonad = Monad[Future]
  futureMonad.flatMap(futureMonad.pure(1))(x => futureMonad.pure(x + 1)).foreach(println)

  Monad[Future]
    .flatMap(Monad[Future].pure(89))(x => Monad[Future].pure(x - 9))
    .foreach(println)

  Functor[Option]
      .map(Option(1))(x => x.toString)
      .foreach(println)



  def intToString(value: Int): String = value.toString
  def stringToInt(value: String): Int = value.toInt


  Thread.sleep(1000)
}
