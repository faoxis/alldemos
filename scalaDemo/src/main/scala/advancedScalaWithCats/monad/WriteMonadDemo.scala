package advancedScalaWithCats.monad

import java.util.logging.Logger

import cats.instances.vector._
import cats.data.Writer
import cats.syntax.applicative._
import cats.syntax.writer._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object WriteMonadDemo extends App {

  type Logged[A] = Writer[Vector[String], A]

  val writer = Writer(Vector(
    "this is the first line",
    "this is the second line"
  ), 123)

  val writer1 = for {
    a <- 10.pure[Logged]
    _ <- Vector("a", "b", "c").tell
    b <- 32.writer(Vector("x", "y", "z"))
  } yield a + b
  println(writer1)

  val t = Writer(Vector[String](), 10)
  println("t: " + t)


  val ttest = for {
    a <- 10.pure[Logged]
    b <- Vector("a", "b", "c").tell
  } yield a
  println(ttest)

  (for {
    a <- Future { "Hello, " }
    b <- Future { a + "there" }
    c <- Future { b + "!" }
  } yield c).foreach(println)
  Thread.sleep(10)

  Future { "Hello, " }
    .flatMap(r => Future { r + "there" })
    .flatMap(r => Future { r + "!" })
    .map(r => r)
    .foreach(println)

}
