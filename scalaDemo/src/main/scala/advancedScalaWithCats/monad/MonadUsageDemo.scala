package advancedScalaWithCats.monad

import cats.{Id, Monad}
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.applicative._
import cats.instances.all._
import cats.syntax.either._

import scala.language.higherKinds


object MonadUsageDemo extends App {
  def sumSquares[M[_]: Monad](a: M[Int], b: M[Int]): M[Int] =
    a.flatMap(x => b.map(y => x * x + y * y))


  println(sumSquares(List(1, 2, 3), List(4, 5)))
  println(sumSquares(1.pure[Option], 2.pure[Option]))
  println(sumSquares(1: Id[Int], 2: Id[Int]))

  def pure[A](value: A): Id[A] = value
  def flatMap[A, B](value: Id[A])(f: A => Id[B]): Id[B] = f(value)
  def map[A, B](value: Id[A])(f: A => B): Id[B] = f(value)

  println(map(4: Id[Int])(_ * 2))
  println(flatMap(4: Id[Int])(_ * 2))

  println(Either.catchNonFatal("foo".toInt).isRight)
  println(Either.catchNonFatal("foo".toInt).getOrElse(123))

}
