package advancedScalaWithCats.monad.exampleStandard

import cats.data.State

import scala.util.{Success, Try}

/**
  * State instance represents atomic operations on the state
  */
object StateExample extends App {
  // creating and unpacking state
  import cats.data.State
  val a = State[Int, String] { state =>
    (state, s"The state is $state")
  }

  println("---------- getting state and result ---------------")
  val (state, result) = a.run(10).value
  println(s"state: $state")
  println(s"result: $result")

  println("---------- getting state only ---------------------")
  val stateOnly = a.runS(10).value
  println(s"state: $stateOnly")

  println("---------- getting result only --------------------")
  val resultOnly = a.runA(10).value
  println(s"result: $resultOnly")


  println("---------- composing and transform state -------------")
  val step1: State[Int, String] = State[Int, String] { num =>
    val ans = num + 1
    (ans, s"Result of step1: $ans")
  }
  val step2 = State[Int, String] { num =>
    val ans = num * 2
    (ans, s"Result of step2: $ans")
  }
  val both = for {
    a <- step1
    b <- step2
  } yield (a, b)
  val (bothState, bothResult) = both.run(20).value
  println(s"State: $bothState")
  println(s"Result: $bothResult")

  println("-------------- block of computations ----------")
  import State._
  val program: State[Int, (Int, Int, Int)] = for {
    a <- get[Int]
    _ <- set[Int](a + 1)
    b <- get[Int]
    _ <- modify[Int](_ + 1)
    c <- inspect[Int, Int](_ * 1000)
  } yield (a, b, c)
  val (programState, programResult) = program.run(1).value
  println(s"State: $programState")
  println(s"Result: $programResult")


  println("---------- post-order calculator --------------------")
  type CalcState[A] = State[List[Int], A]
  def operator(func: (Int, Int) => Int): CalcState[Int] =
    State[List[Int], Int] {
      case a :: b :: tail =>
        val ans = func(a, b)
        (ans::tail, ans)
      case _ => sys.error("Fail")
    }
  def operand(num: Int): CalcState[Int] =
    State[List[Int], Int] { stack =>
      (num :: stack, num)
    }

  def evalOne(sym: String): CalcState[Int] = sym match {
    case "+" => operator(_ + _)
    case "-" => operator(_ - _)
    case "/" => operator(_ / _)
    case "*" => operator(_ * _)
    case num => operand(num.toInt)
  }

  val evalOneRes = for {
    _ <- evalOne("1")
    _ <- evalOne("2")
    res <- evalOne("+")
  } yield res
  println(evalOneRes.run(Nil).value)
  println(evalOneRes.run(List(1)).value)

  val res = evalOne("1")
    .flatMap(x => evalOne("2"))
    .flatMap(y => evalOne("+"))
    .map(z => z)
  println(res.run(Nil).value)
}

