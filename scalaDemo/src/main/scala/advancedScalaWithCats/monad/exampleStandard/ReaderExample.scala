package advancedScalaWithCats.monad.exampleStandard

/**
  * Reader monad needs to append many action in pipeline
 */
object ReaderExample extends App {
  // example class
  case class Cat(name: String, favoriteFood: String)
  val cat = Cat("Garfield", "lasagne")

  // creating reader
  import cats.data.Reader
  val greedKitty: Reader[Cat, String] = Reader(cat => s"Hello, ${cat.name}!")
  // execute reader function
  println(greedKitty.run(cat))

  // making chain of computation (by flatMap method)
  val feedKitty: Reader[Cat, String] =
    Reader(cat => s"Have a nice bowl of ${cat.favoriteFood}!")
  val greetAndFeed: Reader[Cat, String] =
    for {
      msg1 <- greedKitty
      msg2 <- feedKitty
    } yield s"$msg1 $msg2"
  println(greetAndFeed(cat))

  // example of working with database
  case class Db(
                 usernames: Map[Int, String], // all users
                 passwords: Map[String, String] // users' passwords
  )
  type DbReader[A] = Reader[Db, A]

  def findUsername(userId: Int): DbReader[Option[String]] =
    Reader(db => db.usernames.get(userId))

  def checkPassword(
                   username: String,
                   password: String): DbReader[Boolean] =
    Reader(db => db.passwords.get(username).contains(password))

  import cats.syntax.applicative._
  def checkLogin(
                 userId: Int,
                 password: String
                 ): DbReader[Boolean] = for {
    login <- findUsername(userId)
    isOk <- login.map(checkPassword(_, password)).getOrElse(false.pure[DbReader])
  } yield isOk
}
