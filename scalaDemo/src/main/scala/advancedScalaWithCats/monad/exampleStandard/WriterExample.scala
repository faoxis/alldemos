package advancedScalaWithCats.monad.exampleStandard

import scala.concurrent.{Await, Future}


object WriterExample extends App {

  // logged type example
  import cats.data.Writer
  import cats.syntax.applicative._
  type Logged[A] = Writer[Vector[String], A]

  // result without logging
  import cats.instances.vector._
  123.pure[Logged]

  // logging without result
  import cats.syntax.writer._
  Vector("first log", "second log").tell

  // result with logging
  val firstExample = Writer(Vector("first log", "second log"), 123)
  val example = 123.writer(Vector("first log", "second log")) // the same
  example.value // result
  example.written // logging
  val (res, log) = example.run // result and logging

  // efficient making writer
  val writer1 = for {
    a <- 10.pure[Logged]
    _ <- Vector("a").tell
    _ <- Vector("b").tell
    _ <- Vector("c").tell
    b <- 32.writer(Vector("x", "y"))
    _ <- Vector("z").tell
  } yield a + b
  println(writer1)

  // mapWritten can help to interact with logs
  val writer2 = writer1.mapWritten(_.map(_.toUpperCase()))
  println(writer2)
  writer1.mapWritten(_.map(_.toUpperCase()).foreach(x => print(x + " ")))
  println()

  // functions bimap and mapBoth help to change logs and result
  // bimap takes two functions with logs and results
  val writer3 = writer1.bimap (
    log => log.map(_.toUpperCase()),
    res => res - 42
  )
  // mapBoth takes one function with two parameters wich returns the couple
  val writer4 = writer1.mapBoth{ (log, res) =>
    val newLog = log.map(_.toUpperCase())
    val newRes = res - 42
    (newLog, newRes)
  }
  println(s"Is writer3 and writer4 equals? ${writer3 == writer4}")

  // reset will clear the log
  val writer5 = writer1.reset
  println(s"Writer5: $writer5")

  // swap changes result and logs
  val writer6 = writer1.swap
  println(s"Writer1: $writer1")
  println(s"Writer6: $writer6")

  // factorial calculation with writer
  println("------------- factorial calculation -------------")
  def slowly[A](body: => A) = try body finally Thread.sleep(100)

  def factorial(n: Int): Int = {
    val ans = slowly(if (n == 0) 1 else n * factorial(n - 1))
    println(ans)
    ans
  }

  def loggedFactorial(n: Int): Logged[Int] = for {
    ans <- if (n == 0) {
              1.pure[Logged]
            } else {
              slowly(loggedFactorial(n - 1).map(_ * n))
            }
    _ <- Vector(s"factorial $n is $ans").tell
  } yield ans


  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent.duration._

  val Vector((log1, ans1), (log2, ans2)) =
    Await.result(Future.sequence(Vector(
      Future(loggedFactorial(5).run),
      Future(loggedFactorial(5).run)
    )), 5.seconds)
  println(s"log: $log1, result: $ans1")
  println(s"log: $log2, result: $ans2")
}

