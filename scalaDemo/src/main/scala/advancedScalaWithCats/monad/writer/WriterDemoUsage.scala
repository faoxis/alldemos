package advancedScalaWithCats.monad.writer

import cats.data.Writer
import cats.syntax.applicative._
import cats.syntax.writer._
import cats.instances.vector._


import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global


object WriterDemoUsage extends App {
  type Logged[A] = Writer[Vector[String], A]
  def slowly[A](body: => A) = try body finally Thread.sleep(1000)

  def factorial(n: Int): Int  = {
    def helper(n: Int): Logged[Int] = {
      for {
        ans <- if (n == 0) {
          1.pure[Logged]
        } else {
          slowly(helper(n - 1).map(_ * n))
        }
        _ <- Vector(s"ans in $ans").tell
      } yield ans
    }
    val (log, result) = helper(n).run
    log.foreach(println)
    result
  }
  Await.result(Future.sequence(Vector(Future(factorial(5)), Future(factorial(5)))), 10.seconds)
}
