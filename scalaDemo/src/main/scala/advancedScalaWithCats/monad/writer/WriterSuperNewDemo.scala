package advancedScalaWithCats.monad.writer

import cats.data.Writer


object WriterSuperNewDemo extends App {
  type Logged[T] = Writer[Vector[String], T]
}
