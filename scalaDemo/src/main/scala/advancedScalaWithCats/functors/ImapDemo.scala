package advancedScalaWithCats.functors

//trait Codec[A] {
//  def encode(value: A): String
//  def decode(value: String): A
//  def imap[B](dec: A => B, enc: B => A): Codec[B] = {
//    val self = this
//    new Codec[B] {
//      override def encode(value: B): String = self.encode(enc(value))
//      override def decode(value: String): B = dec(self.decode(value))
//    }
//  }
//}
//
//case class Box[A](value: A)
//
//object ImapDemo extends App {
//
//  def encode[A](value: A)(implicit codec: Codec[A]) = codec.encode(value)
//  def decode[A](value: String)(implicit codec: Codec[A]) = codec.decode(value)
//
//  implicit val stringCodec: Codec[String] =
//    new Codec[String] {
//      override def encode(value: String): String = value
//      override def decode(value: String): String = value
//    }
//
//  implicit val intCodec: Codec[Int] =
//    stringCodec.imap(_.toInt, _.toString)
//  implicit val booleanCodec: Codec[Boolean] =
//    stringCodec.imap(_.toBoolean, _.toString)
//  implicit val doubleCodec: Codec[Double] =
//    stringCodec.imap(_.toDouble, _.toString)
//  implicit def boxCodec[A](implicit codec: Codec[A]): Codec[Box[A]] =
//    codec.imap(Box(_), _.value)
//
//  println(encode(5))
//  println(encode(5.0))
//  println(encode("5"))
//  println(decode[Box[String]]("5"))
//
//
//
//}
