package advancedScalaWithCats.functors

case class Cat(name: String, age: Int)
case class Dog(name: String, age: Int)
final case class Box[A](value: A)

trait Printable[A] {

  def format(value: A): String

  def contramap[B](f: B => A): Printable[B] =
    value => this.format(f(value))
}

object PrintableInstances {
  implicit val stringPrintableInstance: Printable[String] =
    (value: String) => "\"" + value + "\""

  implicit val booleanPrintableInstance: Printable[Boolean] =
    (value: Boolean) => if (value) "yes" else "no"

  implicit def boxFormat[A](implicit printable: Printable[A]): Printable[Box[A]] =
    printable.contramap(_.value)

}

object PrintableSyntax {
  implicit class StringPrintableSyntax[A](value: A) {
    implicit def print()(implicit printable: Printable[A]): String = {
      printable.format(value)
    }
  }
}

object ContramapDemo extends App {
  import PrintableInstances._
  import PrintableSyntax._

  format("hello")
  // res3: String = "hello"
  format(true)

  def format[A](value: A)(implicit p: Printable[A]): String =
    p.format(value)

  println(format(Box("Hello World!")))
  println(format(Box(true)))

  println(Box("Hello from box!").print())
  println(Box(true).print())

}
