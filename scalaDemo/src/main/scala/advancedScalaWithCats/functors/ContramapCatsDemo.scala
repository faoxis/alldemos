package advancedScalaWithCats.functors




object ContramapCatsDemo extends App {
  import cats.Contravariant
  import cats.instances.string._
  import cats.Show

  val showString = Show[String]
  val showSymbol = Contravariant[Show]
    .contramap(showString)((sym: Symbol) => s"${sym.name}")
  val showInt = Contravariant[Show]
      .contramap(showString)((number: Int) => number.toString)

  println("Instances:")
  println(showSymbol.show('dave))
  println(showInt.show(42))

  import cats.syntax.contravariant._ // for contramap
  val showSymbol2 = showString.contramap[Symbol](_.name)
  val showInt2 = showString.contramap[Int](_.toString)

  println("Syntax:")
  println(showSymbol2.show('dave))
  println(showInt2.show(42))
}
