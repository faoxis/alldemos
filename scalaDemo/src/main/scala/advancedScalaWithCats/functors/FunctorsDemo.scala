package advancedScalaWithCats.functors

object FunctorsDemo extends App {

  import cats.instances.function._
  import cats.syntax.functor._

  val func1 = (x: Int) => x.toDouble
  val func2 = (y: Double) => y * 2

  val func3 = func1.map(func2)

  /*
  Lows of functor
   */
  val l = List(1, 2, 3) // list is a functor because of map method

  // identity
  println(l.map(a => a) == l)

  // composition
  val g: Int => Int = _ + 1
  val f: Int => Int = _ * 2

//  l.map(g(f(_)))
  l.map(g(_))


  println(l.map(x => g(f(x))) == l.map(f).map(g))

}
