package advancedScalaWithCats.semigroupalAndApplicative

import cats.Semigroupal
import cats.data.Validated

object Part6MethodValidated extends App {

  // Validated использует похожий на Either набор метдов
  import cats.syntax.validated._

  123.valid.map(_ * 100)
  "?".invalid.leftMap(_.toString)
  123.valid[String].bimap(_ + "!", _ * 100)
  "?".invalid[Int].bimap(_ + "!", _ * 100)

  val res = 32.valid.andThen { a =>
    10.valid.map { b =>
      a + b
    }
  }
  println(res) // Valid(42)

  // Как и option Valid имеет метод getOrElse и fold
  val res0 = "error".invalid[Int].getOrElse(0)
  println(res0) // 0

  val fail = "fail".invalid[Int].fold(_ + "!!!", _.toString)
  println(fail) // fail!!!

}
