package advancedScalaWithCats.semigroupalAndApplicative

import cats.Semigroupal
import cats.instances.future._ // for Semigroupal
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.language.higherKinds

object Part3SemigroupalAppliedToDifferentTypes extends App {

  // Некоторые монады ведут себя не совсем так как предполагается

  // ------ Проблема с Future -----
  val f1 = Future{Thread.sleep(100);"Hello"}
  val futurePair = Semigroupal[Future]
    .product(f1, Future(123))
  Await.result(futurePair, 1.second)
  println(futurePair) // Future(Success((Hello,123)))
  // Проблема в том,
  // что вычисление во Future начинается сразу после их объявления,
  // а не при вызове product.

  import cats.syntax.apply._ // for mapN
  case class Cat(
                  name: String,
                  yearOfBirth: Int,
                  favoriteFoods: List[String]
                )
  val futureCat = (
    Future("Garfield"),
    Future(1978),
    Future(List("Lasagne"))
  ).mapN(Cat.apply)
  Await.result(futureCat, 1.second)
  println(futureCat)

  // ------- Проблема с List ------
  import cats.instances.list._

  val res = Semigroupal[List]
    .product(List(1, 2), List(3, 4))
  println(res) // List((1,3), (1,4), (2,3), (2,4))
  // Мы получили декартово произведение, но, в общем, результат непредсказуем

  // ------- Проблема Either ------
  import cats.instances.either._ // for Semigroupal

  type ErrorOr[A] = Either[Vector[String], A]

  val eitherRes = Semigroupal[ErrorOr].product(
    Left(Vector("Error 1")),
    Left(Vector("Error 2"))
  )
  println(eitherRes) // Left(Vector(Error 1))
  // Но мы хотели бы поведение с возращением все результатов


  // Дело в том, что монады наследуют Semigroupal и определяют product в терминах flatMap
  import cats.Monad
  import cats.syntax.flatMap._
  import cats.syntax.functor._

  def product[M[_]: Monad, A, B](x: M[A], y: M[B]): M[(A, B)] =
//    x.flatMap(a => y.map(b => (a, b)))
    for {
      a <- x
      b <- y
    } yield (a, b)


}
