package advancedScalaWithCats.semigroupalAndApplicative

import cats.Semigroupal
import cats.data.Validated

object Part5CombiningInstancesOfValidated extends App {

  // Semigroupal с Validated
  type AllErrorsOr[A] = Validated[String, A]

//  Semigroupal[AllErrorsOr] // Ошибка комплияции
  // При испоьзовании Semigroupal для Validated
  // обязательно нужно делать импорт типа ошибки

  import cats.instances.string._
  Semigroupal[AllErrorsOr]

  import cats.syntax.validated._
  import cats.syntax.apply._

  val stringErrors = (
    "Error 1".invalid[Int],
    "Error 2".invalid[Int]
  ).tupled
  println(stringErrors) // Invalid(Error 1Error 2)

  import cats.instances.list._
  val listErrors = (
    List(500).invalid[Int],
    List(404).invalid[Int]
  ).tupled
  println(listErrors) // Invalid(List(500, 404))

  // Вместо List и Vector можно использовать NonEmptyList и NonEmptyVector
  import cats.data.NonEmptyList

  val nonEmptyListErorrs = (
    NonEmptyList.of("Error 1").invalid[Int],
    NonEmptyList.of("Error 2").invalid[Int]
  ).tupled
  println(nonEmptyListErorrs) // Invalid(NonEmptyList(Error 1, Error 2))

}
