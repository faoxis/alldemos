package advancedScalaWithCats.semigroupalAndApplicative

object Part1Semigroupal extends App {

  // Cartesian позволяет объединять значение
  // Например, если у нас есть F[A] и F[B]

  // Вот так это выглядит в cats
//  trait Cartesian[F[_]] {
//    def product[A, B](fa: F[A], fb: F[B]): F[(A, B)]
//  }

  import cats.Semigroupal
  import cats.instances.option._

  val semigroupal = Semigroupal[Option]
    .product(Some(123), Some("123"))
  println(semigroupal)

  // Некоторые полезные методы

  // tuple2 - tuple22
  println(Semigroupal.tuple3(Option(1), Option(2), Option(3))) // Some((1,2,3))
  println(Semigroupal.tuple3(Option(1), Option(2), Option.empty[Int])) // None

  // map2 - map22
  println(Semigroupal.map3(Option(1), Option(2), Option(3))(_ + _ + _)) // Some(6)
  println(Semigroupal.map3(Option(1), Option(2), Option.empty[Int])(_ + _ + _)) // None

  // Cartesian Builder Syntax
  import cats.syntax.apply._

  println("(Option(123), Option(\"123\"), Option(true)).tupled: ")
  println((Option(123), Option("123"), Option(true)).tupled) // Some((123,123,true))

  println("(Option(123), Option(\"123\"), Option.empty[Boolean]).tupled: ")
  println((Option(123), Option("123"), Option.empty[Boolean]).tupled) // None

  case class Cat(name: String, born: Int, color: String)

  val cat = (
    Option("Garfield"),
    Option(1978),
    Option("Orange & black")
  ).mapN(Cat.apply)
  println(cat) // Some(Cat(Garfield,1978,Orange & black))


  // Итог
  // Если Semigroup нужна для объединения значений,
  // то Semigroupal нужен для объединения контекстов.
  // В Semigroupal мы не проводим вычисление, мы просто объединяем контекст.
}
