package advancedScalaWithCats.semigroupalAndApplicative

object Part2FancyFunctorsAndApplySyntax extends App {

  import cats.Monoid
  import cats.instances.int._ // for Monoid
  import cats.instances.invariant._ // for Semigroupal
  import cats.instances.list._ // for Monoid
  import cats.instances.string._ // for Monoid
  import cats.syntax.apply._ // for imapN

  // Синтаксис Apply также имеет contramapN и imapN методы
  case class Cat (
                 name: String,
                 yearOfBirth: Int,
                 favoriteFoods: List[String]
  )

  val tupleToCat: (String, Int, List[String]) => Cat =
    Cat.apply

  val catToTuple: Cat => (String, Int, List[String]) =
    cat => (cat.name, cat.yearOfBirth, cat.favoriteFoods)

  implicit val catMonoid: Monoid[Cat] = (
    Monoid[String],
    Monoid[Int],
    Monoid[List[String]]
  ).imapN(tupleToCat)(catToTuple)

  // Теперь мы можем "складывать" котов
  import cats.syntax.semigroup._ // for |+|

  val garfield = Cat("Grfield", 1978, List("Lasagne"))
  val heathcliff = Cat("Heathcliff", 1988, List("Junk Food"))

  println(garfield |+| heathcliff) // Cat(GrfieldHeathcliff,3966,List(Lasagne, Junk Food)

}

