package advancedScalaWithCats.semigroupalAndApplicative

import cats.data.Validated

import scala.util.Try

object Part7Exercise extends App {
  case class User(name: String, age: Int)

  type FormData = Map[String, String]
  type FailFast[A] = Either[List[String], A]
  type FailShow[A] = Validated[List[String], A]

  def getValue(name: String)(form: FormData): FailFast[String] =
    form
      .get(name)
      .toRight(List(s"$name field not specified"))

  def parseInt(number: String): FailFast[Int] =
    Try(number.toInt).toOption.toRight(List(s"$number can't be parsed"))

  def nonBlank(name: String)(str: String): FailFast[String] =
    Right(str)
      .filterOrElse(_.nonEmpty, List(s"$name can't be blank"))


  def nonNegative(name: String)(number: Int): FailFast[Int] =
    Right(number)
      .filterOrElse(_ >= 0, List(s"$name can't be less then zero"))

  def readName(data: FormData): FailFast[String] = {
    val fieldName = "name"
    getValue(fieldName)(data)
      .flatMap(x => nonBlank(fieldName)(x))
  }

  def readAge(data: FormData): FailFast[Int] = {
    val fieldName = "age"
    getValue(fieldName)(data)
      .flatMap(nonBlank(fieldName))
      .flatMap(parseInt)
      .flatMap(nonNegative(fieldName))
  }


  import cats.instances.list._ // for Semigroupal
  import cats.syntax.apply._ // for mapN
  import cats.syntax.either._

  def mkUser(data: FormData): Validated[List[String], User] = {
    (
      readName(data).toValidated,
      readAge(data).toValidated
    ).mapN(User.apply)
  }

  println(mkUser(Map("name" -> "John", "age" -> "32"))) // Valid(User(John,32))
  println(mkUser(Map("name" -> "", "age" -> "-1"))) // Invalid(List(name can't be blank, age can't be less zero))

}
