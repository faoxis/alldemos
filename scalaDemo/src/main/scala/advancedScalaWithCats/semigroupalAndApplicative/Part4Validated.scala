package advancedScalaWithCats.semigroupalAndApplicative

import cats.syntax.validated

import scala.util.Try


object Part4Validated extends App {

  // Нельзя реализовать стратегию error accumulating вместо fail fast через монады
  // Поэтому в библиотеке cats имеется альтернативный Either класс Validated

  import cats.Semigroupal
  import cats.data.Validated
  import cats.instances.list._

  type AllErrorsOr[A] = Validated[List[String], A]

  val accErrors = Semigroupal[AllErrorsOr].product(
    Validated.invalid(List("Error 1")),
    Validated.invalid(List("Error 2"))
  )
  println(accErrors) // Invalid(List(Error 1, Error 2))

  accErrors match {
    case Validated.Valid(res) => println(s"Success result $res")
    case Validated.Invalid(errors) => println(s"Erros: $errors")
  }

  // Есть множество способов создать Validated

  // 1
  val v1 = Validated.Valid(123)
  val i2 = Validated.Invalid(List("Badness"))

  // 2
  val v = Validated.valid[List[String], Int](123)
  val i = Validated.invalid[List[String], Int](List("Badness"))

  // 3
  import cats.syntax.validated._
  123.valid[List[String]]
  List("Badness").invalid[Int]

  // 4 (as applicative)
  import cats.syntax.applicative._ // for pure
  import cats.syntax.applicativeError._ // for raiseError

  type ErrorsOr[A] = Validated[List[String], A]

  123.pure[ErrorsOr]
  List("Badness").raiseError[ErrorsOr, Int]

  // 5 (as exception handling like Either)
  trait MyException extends RuntimeException
  class MyException1 extends MyException

  val formatException = Validated
    .catchOnly[MyException]{ throw new MyException1 }

  formatException match {
    case Validated.Valid(res) => println(res)
    case Validated.Invalid(ex) => ex match {
      case e:MyException1 => println("here!")
    }
  }

  // 6 (helper methods)
  Validated.catchOnly[NumberFormatException]("foo".toInt)
  Validated.catchNonFatal(sys.error("Badness"))
  Validated.fromTry(Try("foo".toInt))
  Validated.fromEither[String, Int](Left("Badness"))
  Validated.fromOption[String, Int](None, "Badness")

}
