package advancedScalaWithCats.printabledemo

object SuperTestDemo extends App {
  import PrintableInstances._
  import PrintableSyntax._


  println(2313.print)
  println(Cat("Salem", 11, "black").print)
}
