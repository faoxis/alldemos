package advancedScalaWithCats.printabledemo

final case class Box[A](value: A)

object Box {
  implicit def boxFormatter[A](implicit printable: Printable[A]): Printable[Box[A]] =
    printable.contramap(x => x.value)
}

