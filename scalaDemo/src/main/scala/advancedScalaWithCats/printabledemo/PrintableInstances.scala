package advancedScalaWithCats.printabledemo

object PrintableInstances {

  implicit val stringFormat: Printable[String] = value =>  "\"" + value + "\""
  implicit val intFormat: Printable[Int] =
    value => stringFormat.print(value.toString)
  implicit val booleanFormat: Printable[Boolean] =
    value => if (value) "yes" else "no"

}
