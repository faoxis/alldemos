package advancedScalaWithCats.printabledemo

trait Printable[A] {
  def print(value: A): String
  def contramap[B](f: B => A): Printable[B] = {
    val self = this
    new Printable[B] {
      override def print(value: B): String =
        self.print(f(value))
    }
  }
}

object Printable {
  def print[T](value: T)(implicit printable: Printable[T]): String =
    printable.print(value)
}
