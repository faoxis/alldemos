package advancedScalaWithCats.printabledemo

object PrintableSyntax {

  implicit class printable[T](value: T) {
    def print(implicit printable: Printable[T]): String =
      printable.print(value)
  }

}
