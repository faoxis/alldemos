package advancedScalaWithCats.printabledemo

case class Cat(name: String, age: Int, color: String)

object Cat {
  import PrintableInstances._
  import PrintableSyntax._

  implicit val printableCat: Printable[Cat] = { cat =>
    val name = cat.name.print
    val age = cat.age.print
    val color = cat.color.print

    s"Cat $name, age: $age, color is $color"
  }
}

