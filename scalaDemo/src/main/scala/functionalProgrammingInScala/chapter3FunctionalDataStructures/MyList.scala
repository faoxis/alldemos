package functionalProgrammingInScala.chapter3FunctionalDataStructures

sealed trait MyList[+A]
case object Nil extends MyList[Nothing]
case class Cons[+A] (head: A, tail: MyList[A]) extends MyList[A]

object MyList {

  def apply[A](as: A*): MyList[A] =
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))

  def sum(ints: MyList[Int]): Int = ints match {
    case Nil         => 0
    case Cons(x, xs) => x  + sum(xs)
  }

  def product(numbers: MyList[Double]): Double = numbers match {
    case Nil          => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs)  => x * product(xs)
  }

  def tail[A](l: MyList[A]): MyList[A] = l match {
    case Nil          => Nil
    case l @ Cons(_, _) => l.tail
  }

  def setHead[A](newHead: A, list: MyList[A]): MyList[A] = list match {
    case Nil         => Nil
    case Cons(_, xs) => Cons(newHead, xs)
  }

  def drop[A](n: Int, l: MyList[A]): MyList[A] =
    if (n == 0) l
    else drop(n - 1, tail(l))

  def dropWhile[A](l: MyList[A], f: A => Boolean): MyList[A] = l match {
    case Nil            => Nil
    case c @ Cons(_, _) => if (!f(c.head)) l else dropWhile(c.tail, f)
  }

  def init[A](l: MyList[A]): MyList[A] = l match {
    case Cons (x, Cons(_, Nil)) => Cons(x, Nil)
    case Cons (x, xs)           => Cons(x, init(xs))
    case Nil                    => Nil
  }

}