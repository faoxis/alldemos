package myexperiments.freemonad


sealed trait KVStoreA[A]
case class Put[T](key: String, value: T) extends KVStoreA[Unit]
case class Get[T](key: String) extends KVStoreA[Option[T]]
case class Delete(key: String) extends KVStoreA[Unit]

object FirstDemo extends App {
  import cats.free.Free

  // Steps of free monad

  // 1. Create a Free type base on your ADT
  type KVStore[A] = Free[KVStoreA, A]

  // 2. Create smart contructors using liftF
  import cats.free.Free.liftF

  def put[T](key: String, value: T): KVStore[Unit] =
    liftF[KVStoreA, Unit](Put[T](key, value))

  def get[T](key: String): KVStore[Option[T]] =
    liftF[KVStoreA, Option[T]](Get[T](key))

  def delete(key: String): KVStore[Unit] =
    liftF(Delete(key))

//  def update[T](key: String, value: T): KVStore[Unit] =

}
