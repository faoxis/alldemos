package clientExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityClientDemo {
    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityClientDemo.class, args);
    }
}
