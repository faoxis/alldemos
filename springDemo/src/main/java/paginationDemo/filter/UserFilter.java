package paginationDemo.filter;

import lombok.Data;

@Data
public class UserFilter {
    private Long idFrom;
    private Long idTo;
    private String name;
    private String email;

}
