package hibernatCascadeDemo;

import hibernatCascadeDemo.domain.Clinic;
import hibernatCascadeDemo.domain.Pet;
import hibernatCascadeDemo.repository.ClinicRepository;
import hibernatCascadeDemo.service.ClinicService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
public class HibernateCascadeApplication {
    public static void main(String[] args) {
        ApplicationContext context =
                SpringApplication.run(HibernateCascadeApplication.class, args);

        ClinicRepository repository = context.getBean(ClinicRepository.class);
        Clinic firstClinic = repository.save(
                new Clinic()
                    .setName("First clinic")
                    .setPets(Stream.of(
                            new Pet().setName("Tom").setAge(4),
                            new Pet().setName("Markiz").setAge(8)
                    ).collect(Collectors.toSet()))
        );
        Clinic secondClinic =
                repository.save(new Clinic().setName("Second clinic"));


        ClinicService clinicService = context.getBean(ClinicService.class);
        clinicService.transmitPet(
                firstClinic.getId(),
                secondClinic.getId(),
                firstClinic.getPets().stream().findFirst().get().getId());
    }

}

