package hibernatCascadeDemo.service;

import hibernatCascadeDemo.domain.Clinic;
import hibernatCascadeDemo.domain.Pet;
import hibernatCascadeDemo.repository.ClinicRepository;
import hibernatCascadeDemo.repository.PetRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class ClinicServiceImpl implements ClinicService {

    private ClinicRepository clinicRepository;
    private PetRepository petRepository;

    @Override
    @Transactional
    public void transmitPet(Long firstClinicId, Long secondClinicId, Long petId) {

        Clinic from = clinicRepository.getOne(firstClinicId);
        Clinic to = clinicRepository.getOne(secondClinicId);
        Pet pet = from
                .getPets()
                .stream()
                .filter(p -> p.getId() == petId)
                .findFirst()
                .get();

        from.getPets().remove(pet);
        to.getPets().add(pet);
    }
}
