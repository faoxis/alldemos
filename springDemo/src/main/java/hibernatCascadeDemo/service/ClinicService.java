package hibernatCascadeDemo.service;

import hibernatCascadeDemo.domain.Clinic;
import hibernatCascadeDemo.domain.Pet;

public interface ClinicService {
    void transmitPet(Long firstClinicId, Long secondClinicId, Long petId) ;
}

