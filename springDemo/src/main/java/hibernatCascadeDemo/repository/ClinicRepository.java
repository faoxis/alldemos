package hibernatCascadeDemo.repository;

import hibernatCascadeDemo.domain.Clinic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClinicRepository extends JpaRepository<Clinic, Long> {

}
