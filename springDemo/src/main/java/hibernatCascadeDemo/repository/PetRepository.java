package hibernatCascadeDemo.repository;

import hibernatCascadeDemo.domain.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Long> {
}
