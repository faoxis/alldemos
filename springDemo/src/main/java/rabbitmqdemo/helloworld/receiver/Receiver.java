package rabbitmqdemo.helloworld.receiver;

import org.springframework.stereotype.Component;

@Component
public class Receiver {

    public void receiveMessage(String message) {
        System.out.println("Received <" + message + ">");
    }

    public void receiveMessage(byte[] bytes) {
        receiveMessage(new String(bytes));
    }
}
