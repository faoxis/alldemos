package restTemplateDemo.controller;

import org.springframework.web.bind.annotation.*;
import restTemplateDemo.exception.Throw405Exception;

@RestController
@RequestMapping("/test")
public class TestController {

    @PostMapping("ok")
    public String getOk() {
        return "OK";
    }

    @PostMapping("405")
    public String get405() {
        throw new Throw405Exception();
    }

}
