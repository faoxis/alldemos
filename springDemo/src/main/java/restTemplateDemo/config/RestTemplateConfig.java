package restTemplateDemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class RestTemplateConfig {

    @Bean
    public RestTemplate restTemplate() {

        ClientHttpRequestFactory factory =
                new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
        RestTemplate restTemplate = new RestTemplate(factory);
        List<ClientHttpRequestInterceptor> interceptors =
                restTemplate.getInterceptors().isEmpty() ? new ArrayList<>() : restTemplate.getInterceptors();

        ClientHttpRequestInterceptor interceptor = (request, body, execution) -> {
            ClientHttpResponse response;
            try {
                response = execution.execute(request, body);
                if (response.getStatusCode() == HttpStatus.OK) {
                    System.out.println(response.getBody());
                    InputStream inputStream = response.getBody();
                    byte bytes[] = new byte[inputStream.available()];
                    inputStream.read(bytes);
                    System.out.println(new String(bytes));
                } else {
                    InputStream inputStream = response.getBody();
                    byte bytes[] = new byte[inputStream.available()];
                    inputStream.read(bytes);
                    System.out.println(new String(bytes));
                    System.out.println("-----------------");

                }

                return response;
            } catch (HttpClientErrorException e) {
                System.out.println(e.getStatusCode());
                System.out.println(e.getResponseBodyAsString());
                throw e;
            }
        };

        interceptors.add(interceptor);
        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }

}
