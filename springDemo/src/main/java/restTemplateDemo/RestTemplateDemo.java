package restTemplateDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class RestTemplateDemo {
    public static void main(String[] args) {
        ApplicationContext context =
                SpringApplication.run(RestTemplateDemo.class);

        RestTemplate restTemplate = context.getBean(RestTemplate.class);


        ResponseEntity<String> successResponse = restTemplate
                .postForEntity("http://localhost:8080/test/ok", null, String.class);
        System.out.println(successResponse.getStatusCode());
        System.out.println(successResponse.getBody());

        System.out.println("------------------------------");
        try {
            ResponseEntity<String> error405Response = restTemplate
                    .postForEntity("http://localhost:8080/test/405", null, String.class);
        } catch (HttpClientErrorException e) {
            System.out.println(e.getStatusCode());
            System.out.println(e.getResponseBodyAsString());
        }
//        System.out.println(error405Response.getStatusCode());
//        System.out.println(error405Response.getBody());
    }
}
