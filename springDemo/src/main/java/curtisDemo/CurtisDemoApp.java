package curtisDemo;

import curtisDemo.all.ClientCompany;
import curtisDemo.all.CompaniesRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class CurtisDemoApp {
    public static void main(String[] args) {
        ApplicationContext context =
                SpringApplication.run(CurtisDemoApp.class, args);

        CompaniesRepository repository =
                context.getBean(CompaniesRepository.class);
        ClientCompany clientCompany = new ClientCompany();
        clientCompany.actual = true;
        clientCompany.ceoFullName = "Full";
        clientCompany.companyName = "name";
        clientCompany.ceoPassport = "passport";

        repository.save(clientCompany);
    }
}
