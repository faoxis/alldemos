package curtisDemo.all;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompaniesProvider {

    @Autowired
    private CompaniesRepository companiesRepository;

    List<String> getClientCompanyNames() {
        List<String> names =
                companiesRepository.findCompanyNamesByActualTrue();
        return names;
    }



}
