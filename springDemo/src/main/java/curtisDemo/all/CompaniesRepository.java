package curtisDemo.all;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CompaniesRepository extends CrudRepository<ClientCompany, String> {
    List<String> findCompanyNamesByActualTrue();
}
