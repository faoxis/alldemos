package curtisDemo.all;

import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ClientCompany {
    @Id
    public String companyName;
    public String ceoFullName;
    public String ceoPassport;
    public boolean actual;
}
