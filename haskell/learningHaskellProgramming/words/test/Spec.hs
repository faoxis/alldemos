import Test.Hspec
import Lib

main :: IO ()
main = hspec $ do
         describe "How to write a test" $ do
           it "Should be able to run tests" $ do
             someString `shouldBe` "someString"
             someString `shouldBe` "someString"
             someString `shouldBe` "someString"
             formatGrid ["a", "b"]  `shouldBe` "a\nb\n"

