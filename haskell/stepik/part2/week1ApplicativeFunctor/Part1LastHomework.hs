-- Задача
--Следующий тип данных задает гомогенную тройку элементов, которую можно рассматривать как трехмерный вектор:
data Triple a = Tr a a a  deriving (Eq,Show)

--Сделайте этот тип функтором и аппликативным функтором с естественной для векторов семантикой покоординатного применения:
--GHCi> (^2) <$> Tr 1 (-2) 3
--Tr 1 4 9
--GHCi> Tr (^2) (+2) (*3) <*> Tr 2 3 4
--Tr 4 5 12

instance Functor Triple where
    fmap f (Tr x y z) = Tr (f x) (f y) (f z)

instance Applicative Triple where
    pure x                          = Tr x x x
    (Tr g1 g2 g3) <*> (Tr a1 a2 a3) = Tr (g1 a1) (g2 a2) (g3 a3)