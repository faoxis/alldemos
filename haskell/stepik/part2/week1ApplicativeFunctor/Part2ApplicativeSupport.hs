module Part2ApplicativeSupport where

import Control.Applicative
import Data.Functor

infixl 4 <*?>
(<*?>) :: Applicative f => f a -> f (a -> b) -> f b
(<*?>) = flip (<*>)

