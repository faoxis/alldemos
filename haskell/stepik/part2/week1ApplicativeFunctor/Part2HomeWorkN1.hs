module Part2HomeWorkN1 where

-- Сделайте типы данных Arr2 e1 e2 и Arr3 e1 e2 e3 представителями класса типов Applicative
--
-- newtype Arr2 e1 e2 a = Arr2 { getArr2 :: e1 -> e2 -> a }
--
-- newtype Arr3 e1 e2 e3 a = Arr3 { getArr3 :: e1 -> e2 -> e3 -> a }
-- с естественной семантикой двух и трех окружений:
--
-- GHCi> getArr2 (Arr2 (\x y z -> x+y-z) <*> Arr2 (*)) 2 3
-- -1
-- GHCi> getArr3 (Arr3 (\x y z w -> x+y+z-w) <*> Arr3 (\x y z -> x*y*z)) 2 3 4
-- -15


newtype Arr2 e1 e2 a = Arr2 { getArr2 :: e1 -> e2 -> a }
newtype Arr3 e1 e2 e3 a = Arr3 { getArr3 :: e1 -> e2 -> e3 -> a }

instance Functor (Arr2 e1 e2) where
  g `fmap` (Arr2 f) = Arr2 $ \e1 e2 -> g $ f e1 e2

instance Functor (Arr3 e1 e2 e3) where
  g `fmap` (Arr3 f) = Arr3 $ \e1 e2 e3 -> g $ f e1 e2 e3

instance Applicative (Arr2 e1 e2) where
  pure x                = Arr2 $ \e1 e2 -> x
  (Arr2 g) <*> (Arr2 f) = Arr2 $ \e1 e2 -> g e1 e2 (f e1 e2)

instance Applicative (Arr3 e1 e2 e3) where
  pure x                = Arr3 $ \e1 e2 e3 -> x
  (Arr3 g) <*> (Arr3 f) = Arr3 $ \e1 e2 e3 -> g e1 e2 e3 (f e1 e2 e3)



-- Задача 2
-- Двойственный оператор аппликации (<**>) из модуля Control.Applicative изменяет направление вычислений,
-- не меняя порядок эффектов:
--
-- infixl 4 <**>
-- (<**>) :: Applicative f => f a -> f (a -> b) -> f b
-- (<**>) = liftA2 (flip ($))
-- Определим оператор (<*?>) с той же сигнатурой, что и у (<**>), но другой реализацией:
--
-- infixl 4 <*?>
-- (<*?>) :: Applicative f => f a -> f (a -> b) -> f b
-- (<*?>) = flip (<*>)
-- Для каких стандартных представителей класса типов Applicative можно привести цепочку аппликативных вычислений,
-- дающую разный результат в зависимости от того, какой из этих операторов использовался?
--
-- В следующих шести примерах вашей задачей будет привести такие контрпримеры для стандартных типов данных,
-- для которых они существуют. Следует заменить аппликативное выражение в предложении in на выражение того же типа,
-- однако дающее разные результаты при вызовах с (<??>) = (<**>) и (<??>) = (<*?>).
-- Проверки имеют вид exprXXX (<**>) == exprXXX (<*?>) для различных имеющихся XXX.
-- Если вы считаете, что контрпримера не существует, то менять ничего не надо.



