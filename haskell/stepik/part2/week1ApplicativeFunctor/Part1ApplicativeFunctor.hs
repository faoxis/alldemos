module Part1ApplicativeFunctor where

import Prelude hiding (pure, (<*>))

-- Задача
-- Сделайте типы данных Arr2 e1 e2 и Arr3 e1 e2 e3 представителями класса типов Functor:
--   newtype Arr2 e1 e2 a = Arr2 { getArr2 :: e1 -> e2 -> a }
--   newtype Arr3 e1 e2 e3 a = Arr3 { getArr3 :: e1 -> e2 -> e3 -> a }
--   Эти типы инкапсулируют вычисление с двумя и тремя независимыми окружениями соответственно:
--   GHCi> getArr2 (fmap length (Arr2 take)) 10 "abc"
--   3
--   GHCi> getArr3 (tail <$> tail <$> Arr3 ziWith) (+) [1,2,3,4] [10,20,30,40,50]
--   [33,44]


newtype Arr2 e1 e2 a = Arr2 {getArr2 :: e1 -> e2 -> a}
newtype Arr3 e1 e2 e3 a = Arr3 {getArr3 :: e1 -> e2 -> e3 -> a}

instance Functor (Arr2 e1 e2) where
  fmap f m = Arr2 (\e1 e2 -> f $ getArr2 m e1 e2)

instance Functor (Arr3 e1 e2 e3) where
  fmap f m = Arr3 (\e1 e2 e3 -> f $ getArr3 m e1 e2 e3)


-- Для любого функтора есть только один способ написать правильно fmap
{- Законы fmap (cont - container)
    fmap id cont         == cont                (1)
    fmap f (fmap g cont) == fmap (f . g) cont   (2)
-}

-- Haskell позволяет формально доказать выполнение законов через "рассуждение о программе"
-- Разберем такое доказательство на примере Either
{-
data Either a b = Left a | Right b

instance Functor (Either e) where
    fmap :: (a -> b) -> Either e a -> Either e b
    fmap _ (Left x)  = Left x
    fmap g (Right x) = Right (g y)


Докажем законы:
(1): для этого мы должны отобразить все возможные варианты применения функции id)

fmap id (Left x)  == Left x -- функтор не поменялся т.к. левая часть не трогается
fmap id (Right y) == Right (id y) == Right y  -- функтор не поменялся => ч.т.д.

(2): закон

-- Left x
-- Для начала рассмотрим случай применения композиции функций:
fmap (f . g) (Left x) == Left x
-- Теперь рассмотрим случай последовательного применения функций к функтору:
fmap f (fmap g Left x) == fmap f Left x == Left x
Результаты равны, а значит левая часть монады Left удовлетворяем законам функтора.

-- Right x
-- Композиция функций:
fmap (f . g) (Right x) == Right (f (g x))
-- Последовательное применение:
fmap f (fmap g Right x) == fmap f (Right (g x)) == Right (f (g x))
Результаты равны

Все возможные случаи доказаны, что доказавает и второй закон для монада Either.

-}

-- Для рекурсивных типов данных доказательство производится с помощью структурной индукции
-- По сути это метод математической индукции,
-- если представить рекурсивный тип данных как последовательность вложенных структур
{- Посмотрим выполнение законов для списков
map _ []     = []
map g (x:xs) = gx : map g xs

(1)
(база)
fmap id [] == []

(шаг индукции)
fmap id (x : xs)
== (id x) : fmap id xs
== x : fmap id xs
== x : xs

(2)
(база)
-- композиция
fmap (f . g) [] == []
-- последовательное применение
fmap f (fmap g []) == fmap f [] == []

(шаг индукции)
-- композиция
fmap (f . g) (x:xs)
== f (g x) : fmap (f . g) xs
== f y : fmap f ys
== z : zs

-- последовательное применение
fmap f (fmap g xs)
== fmap f (g x : fmap g xs)
== fmap f (y : fmap ys)
== f y : fmap f ys
== z : zs

Композиция и последоватеьное применение привели на к одинаковому результату,
а значит закон выполняется.
-}
------------------------------------------------------------------------------------

-- Applicative является наследником Functor
-- и со сути является обьединением класс Pointed и Apply

-- Класс Pointed
-- ******************************************************************************* --
class Functor f => Pointed f where
    pure :: a  -> f a -- aka singleton, return, unit, point

-- Пример использования Pointed для знакомых функторов
instance Pointed Maybe where
    pure x = Just x

instance Pointed [] where
    pure x = [x]

instance Pointed (Either e) where
    pure x = Right x

instance Pointed ((->) e) where
--  pure :: a -> ((->) e a) == a -> e -> a
    pure = const

instance Monoid s => Pointed ((,) s) where
--  pure :: a -> (s, a)
    pure x = (mempty, x)

{- Закон для класса типов Pointed:
fmap g (pure x) == pure (g x)
-}


-- Класс Apply
-- ******************************************************************************* --
infixl 4 <*> -- оператор up

-- класс типов? которого нет в стандартной библиотеке, но есть в эксперементальной
class Functor f => Apply f where
    (<*>) :: f (a -> b) -> f a -> f b

-- Пример определения представителей Apply к знакомым функторам
instance Apply [] where
    (f:fs) <*> (x:xs) = f x : (fs <*> xs)
    _      <*> _      = []


-- Посмотрим как мы можем применить <*> для map с несколькими аргументами
fmap2 :: Apply f => (a -> b -> c) -> f a -> f b -> f c -- liftA2
fmap2 fun fa fb = fmap fun fa <*> fb

fmap3 :: Apply f => (a -> b -> c -> d) -> f a -> f b -> f c -> f d -- liftA3
fmap3 fun fa fb fc = fun <$> fa <*> fb <*> fc


-- Соединение Pointed и Apply: функтор Applicative
-- ******************************************************************************* --
{-
class Functor f => Applicative f where
    pure :: a -> f a
    (<*>) :: f (a -> b) -> f a -> f b

instance Applicative Maybe where
    pure = Just
    Noting    <*> _ = Nothing
    (Maybe g) <*> x = fmap g x


-}

-- Закон связи аппликатива и функтора
{-
fmap g container == pure g <*> container

container :: f a
g :: a -> b
-}

-- Законы аппликатива
{-
(1) Identity
pure id <*> v == v

(2) Homomorphism
pure g <*> pure x = pure (g x)

(3) Interchange (коммутативность операций)
container <*> pure x == pure ($ x) <*> container

* На самом деле этот закон показывает разницу аппликативного функтора и монады
- В монадах мы не может таким образом поменять контейнер местами


(4) Composition
pure (.) <*> u <*> v <*> container === u <*> (v <*> container)

-}




