-- Представители класса Applicative

-- Аппликативный список
-- Например, у нас есть такой список
fs = [(*2), (+3), (\x -> 3 - x)]
as = [1, 2]

-- На самом деле нет единой семантики создания представителей аппликативов
-- При этом могут быть несколько эквивалентных семантик
-- Например, для списка мы можно вернуть множество всех вех возможных применений (в примере выше их получится 6)
-- А можно применять функции к элементам по порядку в списке.
-- В стандартной библиотеке реализован первый вариант.
-- fs <*> as === [2,4,4,5,2,1]

-- Реализовать это можно, например, таким способом:
{-
instance Applicative [] where
    pure x    = [x]
    gs <*> xs = [g x | g <- gs, x <- xs]
-}

-- Альтернативный представитель списка
-- Для реализации второго представителя класса необходимо обернуть нашего представителя
newtype ZipList a = ZipList { getZipList:: [a] } deriving Show

instance Functor (ZipList) where
    fmap f (ZipList xs) = ZipList (map f xs)

instance Applicative (ZipList) where
    pure x              = ZipList (repeat x) -- [x] нарушит законы аппликативов с такой реализацией <*>
    ZipList gs <*> ZipList (xs) = ZipList $ zipWith ($) gs xs

-- Задача
-- Реализуйте операторы (>*<) и (>$<), позволяющие спрятать упаковку ZipList и распаковку getZipList:
x1s = [1,2,3]
x2s = [4,5,6]
x3s = [7,8,9]
x4s = [10,11,12]

(>$<) :: (a -> b) -> [a] -> [b]
(>$<) f xs = getZipList $ f <$> (ZipList xs)
infixl 4 >$<

(>*<) :: [a -> b] -> [a] -> [b]
(>*<) fs xs = getZipList $ (ZipList fs) <*> (ZipList xs)
infixl 4 >*<

-- Другие стандартные пресдтавители аппликативного функтора

-- Either
{-
instance Applicative (Either e) where
    pure x          = Right x
    (Left e)  <*> _ = Left e
    (Right g) <*> r = fmap g r -- Здесь можно использовать стандартный fmap Either (т.к. это функтор)
-}

-- (,)
{-
instance Monoid e => Applicative ((,) e) where
    pure              = (,) mempty
    (a, g) <*> r      = fmap g r // todo can we do so ???
    (l, g) <*> (r, x) = (l `mappend` r, g x)
-}

-- (->)
{-
instance Applicative ((->) a) where
    pure x  = \e -> x
    f <*> g :: (e -> (a -> b)) -> (e -> a) -> (e -> b)
    f <*> g = \e -> f e (g e)
-}


-- Задача
-- Функция
--
divideList :: Fractional a => [a] -> a
divideList []     = 1
divideList (x:xs) = (/) x (divideList xs)
-- сворачивает список посредством деления.
-- Модифицируйте ее, реализовав
-- divideList' :: (Show a, Fractional a) => [a] -> (String,a), такую что последовательность вычислений отражается в логе:
--
-- GHCi> divideList [3,4,5]
-- 3.75
-- GHCi> divideList' [3,4,5]
-- ("<-3.0/<-4.0/<-5.0/1.0",3.75)
-- Используйте аппликативный функтор пары, сохраняя близкую к исходной функции структуру реализации
--

divideList' :: (Show a, Fractional a) => [a] -> (String,a)
divideList' []     = ("1.0", 1)
divideList' (x:xs) = (/) <$> ("<-" ++ show x ++ "/", x) <*> divideList' xs

-- Вспомогательные функции
{-
class Functor f => Applicative f where
    pure  :: a -> f a
    (<*>) :: f (a -> b) -> f a -> f b
    (<*)  :: f a -> f b -> f a
    u <* v = const <$> u <*> v
    (*>)  :: f a -> f b -> f b
    u *> v = flip const <$> u <*> v

liftA :: Applicative f => (a -> b) -> f a -> f b
liftA f fa = f <$> f a

liftA2 :: Applicative f => (a -> b -> c) -> f a -> f b -> f c
liftA2 f fa fb = f <$> fa <*> fb

liftA3 :: Applicative f => (a -> b -> c -> d) -> fa -> fb -> fc -> fd
liftA3 f fa fb fc = f <$> fa <*> fb <*> fc

infixl 4 <**>
<**> :: Applicative f => f a -> f (a -> b) -> f b
<**> fa ff = ff <*> fa -- так нельзя т.к. меняется порядок эффектов эффектов
<**> = liftA2 (flip ($)) -- правильная реализация

infixl 4 <*?>
(<*?>) :: Applicative f => f a -> f (a -> b) -> f b
(<*?>) = flip (<*>)
-}


