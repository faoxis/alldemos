{-# LANGUAGE FlexibleContexts #-}
module Part3ParsecDemo where

import Control.Applicative ((*>), (<*))
import Text.Parsec

vowel :: Parsec [Char] u Char
vowel = oneOf "aeiou"


-- Задача
-- Реализуйте парсер getList, который разбирает строки из чисел, разделенных точкой с запятой,
-- и возвращает список строк, представляющих собой эти числа:

getList :: Parsec String u [String]
getList = many1 digit `sepBy1` char ';'

