module Part2 where

-- Типы произведений и сумм произведений

-- Пример типа произведений
data Point = Point Double Double

origin :: Point
origin = Point 0.0 0.0

distanceToOrigin :: Point -> Double
distanceToOrigin (Point x y) = sqrt (x ^ 2 + y ^ 2)

distance :: Point -> Point -> Double
distance (Point x1 y1) (Point x2 y2)  = sqrt ((x2 - x1) ^ 2 + (y2 - y1) ^ 2)


-- В качестве суммы рассматривается операция |
-- Таким образом, произведение - это операция "и", а сумма - это операция или


-- Задача
-- Определим тип фигур Shape:
--
-- data Shape = Circle Double | Rectangle Double Double
-- У него два конструктора:
-- Circle r — окружность радиуса r,
-- и Rectangle a b — прямоугольник с размерами сторон a и b.
-- Реализуйте функцию area, возвращающую площадь фигуры.
-- Константа pi уже определена в стандартной библиотеке.
-- data Shape = Circle Double | Rectangle Double Double
--
-- area :: Shape -> Double
-- area (Circle r)      = pi * (r ^ 2)
-- area (Rectangle x y) = x * y

-- Задача
-- В одном из прошлых заданий мы встречали тип Result и функцию doSomeWork:
-- data Result = Fail | Success
-- doSomeWork :: SomeData -> (Result,Int)
-- Функция doSomeWork возвращала результат своей работы
-- и либо код ошибки в случае неудачи, либо 0 в случае успеха.
-- Такое определение функции не является наилучшим,
-- так как в случае успеха мы вынуждены возвращать некоторое значение,
-- которое не несет никакой смысловой нагрузки.
--
-- Используя функцию doSomeWork, определите функцию doSomeWork' так,
-- чтобы она возвращала код ошибки только в случае неудачи.
-- Для этого необходимо определить тип Result'.
-- Кроме того, определите instance Show для Result' так,
-- чтобы show возвращал "Success" в случае успеха и "Fail: N" в случае неудачи,
-- где N — код ошибки.

-- data Result' = Fail' Int | Success'
--
-- instance Show Result' where
--     show x = case x of
--                 (Fail' n) -> "Fail: " ++ show n
--                 _         -> "Success"
--
--
-- doSomeWork' :: SomeData -> Result'
-- doSomeWork' res = case fst res of
--                     Fail -> Fail'
--                     _ -> Success'
-- не решена



-- Задача
-- Реализуйте функцию isSquare, проверяющую является ли фигура квадратом.
data Shape = Circle Double | Rectangle Double Double

square :: Double -> Shape
square a = Rectangle a a

isSquare :: Shape -> Bool
isSquare (Rectangle x y) = x == y
isSquare _ = False

-- Задача
-- Целое число можно представить как список битов со знаком.
--
-- Реализуйте функции сложения и умножения для таких целых чисел, считая,
-- что младшие биты идут в начале списка, а старшие — в конце.
-- Можно считать, что на вход не будут подаваться числа с ведущими нулями.
-- data Bit = Zero | One
-- data Sign = Minus | Plus
-- data Z = Z Sign [Bit]
--
-- add :: Z -> Z -> Z
-- add (Z sign1 bits1) (Z sign2 bits2) =
--
-- mul :: Z -> Z -> Z
-- mul = undefined
-- не решена






