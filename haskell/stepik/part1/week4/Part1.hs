module Part1 where

-------------------------------------------
--------------- Типы перечислений ---------


-- Пример собственного Bool
data B = T | F deriving (Show, Eq, Enum)

not' :: B -> B
not' T = F
not' F = T


-- Задача
data Color = Red | Green | Blue

instance Show Color where
    show x | x == Red = "Red"

-- Task 2
charToInt :: Char -> Int
charToInt '0' = 0
charToInt '1' = 1
charToInt '2' = 2
charToInt '3' = 3
charToInt '4' = 4
charToInt '5' = 5
charToInt '6' = 6
charToInt '7' = 7
charToInt '8' = 8
charToInt '9' = 9

-- Task 3
data Color = Red | Green | Blue

stringToColor :: String -> Color
stringToColor "Red"   = Red
stringToColor "Green" = Green
stringToColor "Blue"  = Blue


-- Task 4
data LogLevel = Error | Warning | Info

cmp :: LogLevel -> LogLevel -> Ordering
cmp Error Error = EQ
cmp Error _ = GT
cmp Info Info = EQ
cmp Info _ = LT
cmp Warning Warning = EQ
cmp Warning Error = LT
cmp Warning Info = GT


-- Task 4

