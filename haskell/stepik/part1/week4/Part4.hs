module Part4 where

import Data.Char(isDigit)
import Data.Complex
import Data.Ratio

-- Типы с параметрами
-- В haskell есть четкой разделение на конструктор данных и конструктор типов
doubleIt :: a -> (,) a a -- Здесь (,) - конктруктор типов
doubleIt x = (,) x x -- Здесь (,) - конструктор данных

-- Часто конструкторы типов определяются автоматически.
-- Например, такая конструкция Just (1 :: Int) построит тип Maybe Int (Maybe - конктруктор типов).

--  Часто типы строятся "на ходу".
--  Например, запись Left (4::Int) даст тип Either Int b, где b - еще неопределенный тип.

-- Задача
-- Реализуйте функцию, которая ищет в строке первое вхождение символа,
-- который является цифрой, и возвращает Nothing, если в строке нет цифр.
-- import Data.Char(isDigit)

findDigit :: [Char] -> Maybe Char
findDigit []     = Nothing
findDigit (x:xs) = if isDigit x then Just x else findDigit xs

-- Задача
-- Реализуйте функцию findDigitOrX, использующую функцию findDigit (последнюю реализовывать не нужно).
-- findDigitOrX должна находить цифру в строке, а если в строке цифр нет, то она должна возвращать символ 'X'.
-- Используйте конструкцию case.
-- import Data.Char(isDigit)

findDigitOrX :: [Char] -> Char
findDigitOrX xs = case findDigit xs of
                (Just x) ->  x
                Nothing  -> 'X'

-- Задача
-- Maybe можно рассматривать как простой контейнер, например, как список длины 0 или 1.
-- Реализовать функции maybeToList и listToMaybe, преобразующие Maybe a в [a]
-- и наоборот (вторая функция отбрасывает все элементы списка, кроме первого).
maybeToList :: Maybe a -> [a]
maybeToList (Just x) = [x]
maybeToList Nothing  = []

listToMaybe :: [a] -> Maybe a
listToMaybe []     =  Nothing
listToMaybe (x:xs) =  Just x


-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- Типы служат ограничениями над данными. Но есть ли окраничение над типами? Да, это виды (kind).
-- Виды помогают определенить арность типов (количество аргументов).
-- Например, тип Either имеет такой вид: Either :: * -> * -> *


-- При вычислении и паттерн мачинге, конечное значение переменных форсируется (не вычисляются на ходу).
-- Такой подход не всегда эффективен.
-- Для настройки необходимого эффекта в линивости типов используются флаги строгости типов.

-- Пример
-- import Data.Complex
-- import Data.Ratio

data CoordLazy a = CoordLazy a a -- без флагов
        deriving Show
data CoordStrict a = CoordStrict !a !a -- c флагами
        deriving Show

getXLazy :: CoordLazy a -> a
getXLazy (CoordLazy x _) = x

getXStrict :: CoordStrict a -> a
getXStrict (CoordStrict x _) = x

-- Вызов getXLazy (CoordLazy 3 undefined) отлично отработает, т.к. обращения ко второму полю не было
-- При вызове getXStrict (CoordStrict 3 undefined) получим ошибку, т.к. второе поле вычислялось не по ленивой стратегии



-- Задача
-- Реализуйте функции distance, считающую расстояние между двумя точками с вещественными координатами, и manhDistance,
-- считающую манхэттенское расстояние между двумя точками с целочисленными координатами.
data Coord a = Coord a a

distance :: Coord Double -> Coord Double -> Double
distance (Coord x1 y1) (Coord x2 y2) = sqrt ( (x2 - x1) ^ 2 + (y2 - y1) ^ 2)

manhDistance :: Coord Int -> Coord Int -> Int
manhDistance (Coord x1 y1) (Coord x2 y2) = abs (x2 - x1) + abs (y2 - y1)

