module Part5 where


-- Рекурсивные типы данных
-- Такие данные типы данных задаются через собственное определение
data List a = Nil | Cons a (List a)
                deriving Show


-- Здача
-- Тип List, определенный ниже, эквивалентен определению списков из стандартной библиотеки в том смысле,
-- что существуют взаимно обратные функции, преобразующие List a в [a] и обратно. Реализуйте эти функции.
-- data List a = Nil | Cons a (List a)
fromList :: List a -> [a]
fromList Nil           = []
fromList (Cons x rest) = x : fromList rest

toList :: [a] -> List a
toList []      = Nil
toList (x:xs)  = Cons x (toList xs)




-- Построим удобную систему данных для работы с арифметическими выражениями
-- Для этого создадим рекурсивные тип данных Expr
infixl 6 :+:
infixl 7 :*:

data Expr = Val Int | Expr :+: Expr | Expr :*: Expr
                deriving (Show, Eq)

expr1 = Val 2 :+: Val 3 :*: Val 4
expr2 = (Val 2 :+: Val 3) :*: Val 4

eval :: Expr -> Int
eval (Val x)           = x
eval (expr1 :+: expr2) = eval expr1 + eval expr2
eval (expr1 :*: expr2) = eval expr1 * eval expr2

expand :: Expr -> Expr
expand ((e1 :+: e2) :*: e) = expand e1 :*: expand e :+: expand e2 :*: expand e
expand (e :*: (e1 :+: e2)) = expand e :*: expand e1 :+: expand e :*: expand e2
expand (e1 :+: e2) = expand e1 :+: expand e2
expand (e1 :*: e2) = expand e1 :*: expand e2
expand e = e

-- Задача
-- Рассмотрим еще один пример рекурсивного типа данных:
--
-- data Nat = Zero | Suc Nat
-- Элементы этого типа имеют следующий вид: Zero, Suc Zero, Suc (Suc Zero), Suc (Suc (Suc Zero)), и так далее.
-- Таким образом мы можем считать, что элементы этого типа - это натуральные числа в унарной системе счисления.
--
-- Мы можем написать функцию, которая преобразует Nat в Integer следующим образом:
--
-- fromNat :: Nat -> Integer
-- fromNat Zero = 0
-- fromNat (Suc n) = fromNat n + 1
-- Реализуйте функции сложения и умножения этих чисел, а также функцию, вычисляющую факториал.


data Nat = Zero | Suc Nat deriving Show

fromNat :: Nat -> Integer
fromNat Zero = 0
fromNat (Suc n) = fromNat n + 1

add :: Nat -> Nat -> Nat
add (Zero)  others = others
add (Suc n) others = add n (Suc others)

mul :: Nat -> Nat -> Nat
mul (Zero)  others = Zero
mul firtNat others = helper (fromNat firtNat) others   where
                                    helper :: Integer -> Nat -> Nat
                                    helper 1 nat = nat
                                    helper n nat = add nat (helper (n - 1) nat)

fac :: Nat -> Nat
fac nat = helper (Suc Zero) nat where
                        helper :: Nat -> Nat -> Nat
                        helper res Zero         = res
                        helper res rest@(Suc n) = helper (mul res rest) n


-- Задача
-- Тип бинарных деревьев можно описать следующим образом:
--
data Tree a = Leaf a | Node (Tree a) (Tree a)
--
-- Реализуйте функцию height, возвращающую высоту дерева, и функцию size,
-- возвращающую количество узлов в дереве (и внутренних, и листьев).
-- Считается, что дерево, состоящее из одного листа, имеет высоту 0.

height :: Tree a -> Int
height (Leaf a)           = 0
height (Node tree1 tree2) = 1 + (max (height tree1) (height tree2))

size :: Tree a -> Int
size (Leaf a)           = 1
size (Node tree1 tree2) = 1 + (size tree1) + (size tree2)

foo = Node (Node (Leaf 1) (Leaf 2)) (Node (Leaf 1) (Node (Leaf 1) (Leaf 2)))


-- Задача
-- Теперь нам нужно написать функцию avg, которая считает среднее арифметическое всех значений в дереве.
-- И мы хотим, чтобы эта функция осуществляла только один проход по дереву.
-- Это можно сделать при помощи вспомогательной функции, возвращающей количество листьев и сумму значений в них.
-- Реализуйте эту функцию.
avg :: Tree Int -> Int
avg t =
    let (c,s) = go t
    in s `div` c
  where
    go :: Tree Int -> (Int,Int)
    go (Leaf a) = (1, a)
    go (Node tree1 tree2) = (fst x + fst y, snd x + snd y)
                                                    where
                                                        x = go tree1
                                                        y = go tree2


-- size (Node tree1 tree2) = 1 + (size tree1) + (size tree2)
