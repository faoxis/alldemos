module Part3 where

-- Синтаксис записей

-- Возможная реализация сущности Person(firstName, lastName, age) как произведение типов
data Person' = Person' String String Int

firstName' :: Person' -> String
firstName' (Person' x _ _) = x

lastName' :: Person' -> String
lastName' (Person' _ y _) = y

age' :: Person' -> Int
age' (Person' _ _ z) = z

-- Можно повторить приведенный выше функционал с помощью меткой полей
data Person = Person { firstName :: String, lastName :: String, age :: Int}
                deriving (Show, Eq)


