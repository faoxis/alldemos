module Part6 where

import Prelude hiding (lookup)
import qualified Data.List as L

-- Синонимы и обертки типов
-- Для упрощения работы с типами часто удобно задавать некий синоним.
-- Делается это с помощью ключевого слова type.
-- type String = [Char]

-- Пример использования (сумма квадратов)
type IntegerList = [Integer]

sumSquares :: IntegerList -> Integer
sumSquares = foldr1 (+) . map (^2)


-- Синонимы могут принимать аргументы

-- Пример ассоциативного массива
type AssocList k v = [(k, v)]

lookup' :: Eq k => k -> AssocList k v -> Maybe v
lookup' _ [] = Nothing
lookup' key ((x, y):xys) = if x == key
                                then Just y
                                else lookup' key xys

-- Синонимы можно применять частично

-- Пример частичного применения на Either
type IntableEither = Either Int
-- :k Intarable :       IntableEither :: * -> *


-- !!!
-- Над существующим типом можно создать обертку с единственным контруктором.
-- Делается это с помощью ключевого слова newtype.
newtype IntList = IList [Int] deriving Show
example = IList [1, 2]

-- Мы могли бы сделать идентичное поведение через data
data IntList' = IList' [Int] deriving Show
example' = IList' [1, 2]

-- Между newtype и data есть два отличая:

--  1) newtype может иметь только один конструктор. Из-за этого мы не перебираем его типы в pattern matching,
--    но работа с таким объектом более эффективна. Дело в том, что компилятор отбрасывает наш класс и в процесе
--    исполнения работа будет производиться только с сами значением.

--  2) Тип созданные через newtype более ленив. Пример:
ignore' :: IntList' -> String
ignore' (IList' _) = "Hello"
-- ignore' undefined возбудит ошибку *** Exception: Prelude.undefined, т.к. в рантейме будет значение ignore' (IList' x),
-- где x - undefined

ignore :: IntList -> String
ignore (IList _) = "Hello"
-- ignore undefined отработает без ошибок, т.к. на самом деле сопоставление с образцом не происходит,
-- т.к. в рантайме у нас будет сопоставление (ignore _), что соответствует любым входным данным


-- По всем остальным пунктам newtype схож с data
-- Например, мы так же можем использовать метки полей (но в случае newtype только одну)
newtype User = User {firstName :: String}
                deriving (Show, Eq)
data User' = User' { firstName' :: String, lastName' :: String, age' :: Int}
                deriving (Show, Eq)

newtype Identity a = Identity {runIdentity :: a}
                deriving (Show, Eq, Ord)
-- Типы User и Identity существуют только в момент разработки, в процессе исполнения их нет



-- В haskell часто используется newtype для реализации представителей класс Monoid
-- Моноид - это множество с заданной на нем ассоциативной бинарной опирацией и нейтральным элементом.
class Monoid' a where
    mempty' :: a -- нейтральный элемент
    mappend' :: a -> a -> a -- операция

    mconcat' :: [a] -> a -- свертка
    mconcat' = foldr mappend' mempty'

{- Законы
mempty `mappend` x === x -- левый нейтральный элемент
x `mappend` mempty === x -- правый нейтральный элемент
(x `mappend` y) `mappend` z === x `mappend` (y `mappend` z) -- Ассоциативность операции
-}

-- Одним из самых простых представителей класса моноид является список
instance Monoid' [a] where
    mempty' = []
    mappend' = (++)

-- Числа являются дважды моноидами (для сложения и умножения) и поэтому необходимо задать нужное нам действие

-- Для сложения
newtype Sum a = Sum { getSum :: a}
    deriving (Eq, Ord, Read, Show, Bounded)

instance Num a => Monoid' (Sum a) where
    mempty'                  = Sum 0
    mappend' (Sum x) (Sum y) = Sum (x + y)

-- Для умножения
newtype Product a = Product {getProduct :: a}
    deriving (Eq, Ord, Read, Show, Bounded)

instance Num a => Monoid' (Product a) where
    mempty'                          = Product 1
    mappend' (Product x) (Product y) = Product (x * y)

-- Задача
-- Написать моноид для операции xor
---------------------------------


-- Многие другие типы являются моноидами
-- Например, пары и кортежи являются моноидами, если их элементы моноиды.

-- Пара моноидов легко преобразуется в моноид пары
instance (Monoid' a, Monoid' b) => Monoid' (a,b) where
    mempty'                    = (mempty', mempty')
    mappend' (x1, x2) (y1, y2) = (mappend' x1 y1, mappend' x2 y2)

-- Maybe можно представить как моноид, если элемент внутри него также является моноидом
instance (Monoid' a) => Monoid' (Maybe a) where
   mempty'                      = Nothing
   mappend' m Nothing           = m -- Т.к. Nothing - нейтральный элемент
   mappend' Nothing m           = m
   mappend' (Just m1) (Just m2) = Just (mappend' m1 m2)

-- Одним из полезных представителем моноида для класса Maybe может выступать First
newtype First a = First { getFirst :: a}
    deriving (Eq, Ord, Read, Show)


-- Задача
-- Реализуйте представителя класса типов Monoid для типа Xor, в котором mappend выполняет операцию xor.
newtype Xor = Xor { getXor :: Bool }
    deriving (Eq,Show)

instance Monoid Xor where
    mempty             = Xor False
    mappend (Xor True) (Xor False) = Xor True
    mappend (Xor False) (Xor True) = Xor True
    mappend _     _    = Xor False

--  не работает
-- instance (Maybe a) => Monoid' (First a) where
--     mempty'                    = First Nothing
--     mappend' (First Nothing) r = r
--     l `mappend'` _             = l


-- С функциями в Haskell можно работать как с объектами.
-- И, например, не только передавать в качестве аргумента, но и оборачивать некоторую упоковку.
-- :t [(*2), (+3), (^2)] даст результат: [(*2), (+3), (^2)] :: Num a => [a -> a]

-- Функции, которые возвращают результят того же типа, что и принимают называются эндоморфизмами.
-- Такие функции можно описать с помощью специальной упоковки.
-- newtype

-- Задача
-- Реализуйте представителя класса типов Monoid для Maybe' a так, чтобы mempty не был равен Maybe' Nothing.
-- Нельзя накладывать никаких дополнительных ограничений на тип a, кроме указанных в условии.
newtype Maybe' a = Maybe' { getMaybe :: Maybe a }
    deriving (Eq,Show)

instance Monoid a => Monoid (Maybe' a) where
    mempty                                       = Maybe' (Just mempty) -- Maybe может быть моноидом только, если содержит моноид
    mappend (Maybe' Nothing)    _                = Maybe' Nothing
    mappend _                   (Maybe' Nothing) = Maybe' Nothing
    mappend (Maybe' a)          (Maybe' b)       = Maybe' (mappend a b)

-- Задача
-- Ниже приведено определение класса MapLike типов, похожих на тип Map.
-- Определите представителя MapLike для типа ListMap, определенного ниже как список пар ключ-значение.
-- Для каждого ключа должно храниться не больше одного значения. Функция insert заменяет старое значение новым,
-- если ключ уже содержался в структуре.
class MapLike m where
    empty :: m k v
    lookup :: Ord k => k -> m k v -> Maybe v
    insert :: Ord k => k -> v -> m k v -> m k v
    delete :: Ord k => k -> m k v -> m k v
    fromList :: Ord k => [(k,v)] -> m k v
    fromList [] = empty
    fromList ((k,v):xs) = insert k v (fromList xs)

newtype ListMap k v = ListMap { getListMap :: [(k,v)] }
    deriving (Eq,Show)

instance MapLike ListMap where
    -------
    empty                                 = ListMap []
    -------
    lookup expectedK (ListMap [])         = Nothing
    lookup expectedK (ListMap ((k,v):[])) = if (expectedK == k) then Just v else Nothing
    lookup expectedK (ListMap ((k,v):xs)) = if (expectedK == k) then Just v else lookup expectedK (ListMap xs)
    -------
    delete key (ListMap [])         = empty
    delete key (ListMap list) = ListMap (helper list) where
                                        helper [] = []
                                        helper ((a, b):xs) = if (a == key) then xs else (a, b) : (helper xs)

    -------
    insert k v listMap = insertAfterDelete (delete k listMap) where
                                        insertAfterDelete (ListMap xs) = ListMap ((k,v):xs)
    -------
    fromList list = ListMap list


-- insert 3 "three" (insert 2 "two" (insert 1 "one" (empty :: ListMap Int String)))
