module Other where

import Prelude hiding (scanl)

scanl :: (b -> a -> b) -> b -> [a] -> [b]
scanl f ini []     = [ini]
scanl f ini (x:xs) = ini : scanl f (f ini x) xs

facs :: (Num a, Enum a) => [a]
facs = scanl (*) 1 [1..]


---------------------------------------------------------------------------
-- функция unfold формирует список из одного значния
unfold' :: (b -> (a, b)) -> b -> [a]
unfold' f ini = let (x, ini') = f ini in x : unfold' f ini'

-- на основе функции unfold можно построить интератор
iterate' :: (a -> a) -> a -> [a]
iterate' f = unfold' (\x -> (x, f x))
---------------------------------------------------------------------------



---------------------------------------------------------------------------

unfoldr' :: (b -> Maybe (a, b)) -> b -> [a]
unfoldr' f ini = helper (f ini) where
            helper (Just (x, ini')) = x : unfoldr' f ini'
            helper Nothing = []


---------------------------------------------------------------------------


---------------------------------------------------------------------------
-- Task
revRange :: (Char,Char) -> [Char]
revRange (a, b) = unfoldr' g a
  where g x | x <= b =
                        if x >= a && x <= b
                        then Just(succ x, succ x)
                        else Just (x, succ x)
            | otherwise = Nothing


