module FoldLeft where

-- import Prelude hiding (foldl)
import Prelude hiding (foldl1)

-- левая свертка по определению
-- foldl :: (b -> a -> b) -> b -> [a] -> b
-- foldl f ini [] = ini
-- foldl f ini (x:xs) = foldl f (f ini x) xs


-- улучшеная версия
-- foldl :: (b -> a -> b) -> b -> [a] -> b
-- foldl f ini []      = ini
-- foldl f init (x:xs) = foldl f ini' xs
--             where ini' = f ini xs


-- строгая версия левой свертки (оптимизация thank'ов)
foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' f ini [] = ini
foldl' f ini (x:xs) = ini' `seq` foldl' f ini' xs
                    where ini' = f ini x


meanList :: [Double] -> Double
meanList xs = (fst res) / (snd res)
                where res = foldr (\x (s,c) -> (x+s,c+1)) (0.0, 0) xs


evenOnly :: [a] -> [a]
evenOnly xs = (fst res)
           where res = foldl'
                        (\(s,c) x ->
                            if even c
                                then (s ++ [x], c+1)
                                else (s, c+1))
                        ([], 1)
                        xs


foldl1 :: (a -> a -> a) -> [a] -> a
foldl1 f [] = error "foldl1 can't get emply list"
foldl1 f (x:xs) = foldl' f x xs

lastElem :: [a] -> a
lastElem = foldl1 seq


