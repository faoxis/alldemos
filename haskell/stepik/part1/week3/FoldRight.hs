module FoldRight where

sumList :: [Integer] -> Integer
sumList = foldr' (+) 0

productList :: [Integer] -> Integer
productList = foldr' (*) 1

concatList :: [[a]] -> [a]
concatList = foldr' (++) []

foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f init [] = init
foldr' f init (x:xs) = f x (foldr' f init xs)

