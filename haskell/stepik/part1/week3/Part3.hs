module Part3 where

coins :: Num a => [a]
coins = [2, 3, 7]

change :: (Ord a, Num a) => a -> [[a]]
change 0 = [[]]
change n = [coin:rest | coin <- coins, coin <= n, rest <- (change $ n - coin)]



