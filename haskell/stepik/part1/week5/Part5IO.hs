module Part5 where

import Control.Monad

import Data.List
import System.Directory

import Data.Typeable

-- В Haskell проблема эффектов ввода/вывода решается с помощью монады IO
main = do
    putStrLn "What is your name?"
    name <- getLine
    putStrLn $ "Nice to meet you, " ++ name ++ "!"

-- main :: IO ()
-- putStrLn :: String -> IO ()     putStrLn - это стрелка Клейсли (из String в контейнер IO)
-- getLine :: IO String




--- Задача
-- Напишите программу, которая будет спрашивать имя пользователя, а затем приветствовать его по имени. Причем, если пользователь не ввёл имя, программа должна спросить его повторно, и продолжать спрашивать, до тех пор, пока пользователь не представится.
   --
   -- Итак, первым делом, программа спрашивает имя:
   --
   -- What is your name?
   -- Name:
   --
   -- Пользователь вводит имя и программа приветствует его:
   --
   -- What is your name?
   -- Name: Valera
   -- Hi, Valera!
   --
   --
   -- Если же пользователь не ввёл имя, необходимо отобразить точно такое же приглашение ещё раз:
   -- What is your name?
   -- Name:
   -- What is your name?
   -- Name:
   -- What is your name?
   -- Name: Valera
   -- Hi, Valera!
   --
   --
   -- Пожалуйста, строго соблюдайте приведенный в примере формат вывода.
   -- Особое внимание уделите пробелам и переводам строк!
   -- Не забудьте про пробел после Name:,
   -- а также про перевод строки в самом конце
   -- (ожидается, что вы будете использовать putStrLn для вывода приветствия пользователя).

-- getThisFuckingName :: IO String -> String
-- getThisFuckingName io = io >>= (\s -> s)


main' :: IO ()
main' = do
            putStrLn "What is your name?"
            putStr "Name: "
            name <- getLine
            if (null name) then main' else putStrLn $ "Hi, " ++ name ++ "!"



--- Примерное описание монады IO
{-
type IO a = RealWorld -> (RealWorld, a)

return :: a -> IO a
или
return :: a -> RealWorld -> (RealWorld, a)

(>>=) :: IO a -> (a -> IO b) -> IO b
или
(>>=) :: RealWorld -> (RealWorld, a)
      -> (a -> RealWorld -> (RealWorld, b))
      -> RealWorld -> (RealWorld, b)

instance Monad IO where
    return a = \w -> (w, a)   -- 1. Возвращается функция, в которую будет "подставленно" значение w (RealWorld).
                              -- 2. Значение w не меняется. Какой RealWorld пришел, такой и будет подставлен.

    (>>=) m k = \w -> case m w of (w', a) -> k a w'   -- m :: RealWorld -> (RealWorld, a)
                                                      -- k :: (a -> RealWorld -> (RealWorld, b))
-}


--- Построим функцию getLine через примитив getChar
getLine' :: IO String
getLine' = do
            c <- getChar
            if c == '\n' then
                return []
            else do
                cs <- getLine'
                return (c:cs)

--- Построим функцию putStr на основе примитива putChar
putStr' :: String -> IO ()
putStr' []     = return ()
putStr' (x:xs) = putChar x >> putStr' xs



--- В модуле Control.Monad имеется множество полезных функций для работы с монадами

-- Функция sequence_
{- Функция sequence_ берет список монад, выполняет все монадические вычисления и возвращает "не особо интересный" результат.
sequence_ :: Monad m => [m a] -> m ()
sequence_ = foldr (>>) (return ())
Таким образом мы выполняем действие и игнорируем значение -}

-- Функция sequence_ может продемонстрировать только эффект вычисления, но не его значение
-- sequence_ [Just 1, Just 2]                          === Just ()
-- sequence_ [Just 1, Just 2, Just 3, Nothing, Just 4] === Nothing
-- sequence_ [[1, 2], [3, 4, 5, 6]]                    === [(),(),(),(),(),(),(),()]

-- Основываясь на этом, можно переписать функцию putStr' в следующий вид:
putStr'' :: String -> IO ()
putStr'' = sequence_ . map putChar

-- На самом деле связка sequence_ . map встречается довольно часто и поэтому в библиотеке имеется такая функция:
{-
mapM_ :: Monad m => (a -> m b) -> [a] -> m ()
mapM_ f = sequence_ . map f
-}
-- mapM_ (\x -> [x, x]) [1,2,3] === [(),(),(),(),(),(),(),()]


-- Функции без подчеркивания говорят о том, что нам важен возвращаемый тип
{-
sequence :: Monad m => [m a] -> m [a]
sequence ms = foldr k (return []) ms
    where
        k :: Monad m => m a -> m [a] -> m [a]
        k m m' = do
                 x  <- m
                 xs <- m'
                 return (x:xs)

mapM :: Monad m => (a -> m b) -> [a] -> m [b]
mapM f sequence. map f
-}


--- Задача
   -- В этом задании ваша программа должна попросить пользователя ввести любую строку,
   -- а затем удалить все файлы в текущей директории,
   -- в именах которых содержится эта строка, выдавая при этом соответствующие сообщения.
   --
   -- Substring:
   --
   -- Пользователь вводит любую строку:
   --
   -- Substring: hell
   --
   --
   -- Затем программа удаляет из текущей директории файлы с введенной подстрокой в названии.
   -- К примеру, если в текущей директории находились файлы thesis.txt, kitten.jpg, hello.world, linux_in_nutshell.pdf, то вывод будет таким:
   --
   -- Substring: hell
   -- Removing file: hello.world
   -- Removing file: linux_in_nutshell.pdf
   --
   --
   -- Если же пользователь ничего не ввёл (просто нажал Enter), следует ничего не удалять и сообщить об этом:
   --
   -- Substring:
   -- Canceled
   --
   --
   -- Для получения списка файлов в текущей директории используйте функцию getDirectoryContents,
   -- передавая ей в качестве аргумента строку, состоящую из одной точки  ("."), что означает «текущая директория».
   -- Для удаления файлов используйте функцию removeFile
   -- (считайте, что в текущей директории нет поддиректорий — только простые файлы).
   -- В выводимых сообщениях удаленные файлы должны быть перечислены в том же порядке,
   -- в котором их возвращает функция getDirectoryContents.
   --
   -- Пожалуйста, строго соблюдайте приведенный в примере формат вывода.
   -- Особое внимание уделите пробелам и переводам строк! Не забудьте про пробел после Substring:,
   -- а также про перевод строки в конце (ожидается, что вы будете использовать putStrLn для вывода сообщений об удалении).


deleteFile :: String -> IO ()
deleteFile name = do
                   putStrLn $ "Removing file: " ++ name
                   removeFile name

main'' :: IO ()
main'' = do
            putStr "Substring: "
            substring <- getLine

            if (null substring)
                    then putStrLn "Canceled"
                    else do
                         files <- getDirectoryContents "."
                         let filtered = filter (isInfixOf substring) files
                         mapM_ (deleteFile) filtered
--             files <- filesInCurDir
--
--             let filtered = filter (isInfixOf substring) files
--             print filtered
--
--             putStrLn ("type of filtered is: " ++ (show (typeOf filtered)))
--             mapM_ (deleteFile) filtered
--             do
--                 s <- filtered
--                 putStrLn s
--             putStrLn ("type of x is: " ++ (show (typeOf x)))


