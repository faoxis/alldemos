module MyTests where

class Functor' f where
    fmap' :: (a -> b) -> f a -> f b

instance Functor' [] where
    fmap' f (x:xs) = map

instance Functor' (Either a) where
    fmap' f (Right x) = Right (f x)
    fmap' f (Left x)  = Left x

instance Functor' ((,) a) where
    fmap' f (x, y) = (x, f y)

instance Functor' ((->) a) where
    fmap' = (.)

