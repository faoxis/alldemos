module Part6Reader where

import Control.Monad (ap, liftM)

-- МОНАДА Reader
--
-- Монада Reader нужна для вычислений в некотором константном окружении (иногда такую монаду называют Environment монадой)
--
-- Монадическое представление (->) является простейшим видом монады Reader
-- Для начала вспомним как "стрелка" реализует представителя класса Functor
{-
instance Functor ((->) e) where
    fmap g h = g . h

fmap :: (a -> b) -> f a -> f b
     :: (a -> b) -> (e -> a) -> (e -> b)
-}
-- вызов fmap (^2) length [1,2,3]  даст 3, здесь [1, 2, 3] является неким окружением


-- Сделаем специальную обертку Reader для более удобной работы
newtype MyReader a b = MyReader { runReader :: (a -> b) }


instance Functor (MyReader r) where
  fmap = liftM

instance Applicative (MyReader r) where
  pure = return
  (<*>) = ap

instance Monad (MyReader a) where
    return x = MyReader $ \e -> x
    m >>= f  = MyReader $ \e ->
                let v = runReader m e
                in runReader (f v) e

-- Функция ask позволяет возвращаться окружение (финальное значение)
ask :: MyReader r r
ask = MyReader id -- на самом деле это просто вызов функции id

-- Пример использования функции ask
type User = String
type Password = String
type UsersTable = [(User, Password)]

pwds :: UsersTable
pwds = [("Bill", "123"), ("Ann", "qwerty"), ("John", "2sRq8P")]

firstUser :: MyReader UsersTable User
firstUser = do
            e <- ask
            return $ fst (head e)
-- runReader firstUser pwds


-- Удобно определить функцию для вызова конструктора Reader'a
asks :: (r -> a) -> MyReader r a
asks = MyReader

firstUserPwd' :: MyReader UsersTable Password
firstUserPwd' = do
            pwd <- asks (snd . head)
            return pwd

-- Можно одному из монадических законов можно сделать следующее
firstUserPwd :: MyReader UsersTable Password
firstUserPwd = asks (snd . head)


-- Так же, весьма полезной функций может стать local
local :: (r -> r) -> MyReader r a -> MyReader r a -- Функция модифицирует окружение
local f m = MyReader $ \e -> runReader m (f e)

-- Пример использования
usersCount :: MyReader UsersTable Int
usersCount = asks length

localTest :: MyReader UsersTable (Int, Int)
localTest = do
        count1 <- usersCount
        count2 <- local (("Mike", "1"):) usersCount -- Добавляем нового пользователя
        return (count1, count2) -- Возращем количество начальных и модифицированных элементов

-- В стандартной библиотеки весьма популярна функция reader как инкапсуляция конструктора Reader
reader :: (r -> a) -> MyReader r a
reader f = do
        r <- ask
        return (f r)

-- Задача
-- Реализуйте функцию local' из прошлого задания.
--
-- Считайте, что монада Reader определена так, как на видео:
--data Reader' r a = Reader' { runReader :: (r -> a) }
--
--instance Functor (Reader' r) where
--  fmap = liftM
--
--instance Applicative (Reader' r) where
--  pure = return
--  (<*>) = ap
--
--instance Monad (Reader' r) where
--  return x = Reader' $ \_ -> x
--  m >>= k  = Reader' $ \r -> runReader (k (runReader m r)) r
--
--
--local' :: (r -> r') -> Reader' r' a -> Reader' r a
--local' f m = Reader' (\e -> runReader m (f e))

-- Задача
-- Вспомним пример с базой пользователей и паролей:
--
-- type User = String
-- type Password = String
-- type UsersTable = [(User, Password)]
-- Реализуйте функцию, принимающую в качестве окружения UsersTable и возвращающую список пользователей, использующих пароль "123456" (в том же порядке, в котором они перечислены в базе).
--
-- GHCi> runReader usersWithBadPasswords [("user", "123456"), ("x", "hi"), ("root", "123456")]
-- ["user","root"]


usersWithBadPasswords :: MyReader UsersTable [User]
usersWithBadPasswords = asks $ (fmap fst) . (filter (\x -> snd x == "123456"))



