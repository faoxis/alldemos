module Part7Writer where

import Control.Monad (ap, liftM)
import Data.Monoid


-- Монада Writer позволяет в контексте вычисления хранить лог в виде моноида
newtype MyWriter w a = MyWriter { runWriter :: (a, w) } deriving Show

-- runWriter :: MyWriter w a -> (a, w)

-- Конструктор
writer :: (a, w) -> MyWriter w a
writer = MyWriter

-- Чтение логов
execWriter :: MyWriter w a -> w
execWriter m = snd (runWriter m)

-- Объявляем монадой
instance (Monoid w) => Functor (MyWriter w) where
  fmap = liftM

instance (Monoid w) => Applicative (MyWriter w) where
  pure = return
  (<*>) = ap

instance (Monoid w) => Monad (MyWriter w) where
    return x = MyWriter (x, mempty)
    m >>= k =
        let (x, u) = runWriter m
            (y, v) = runWriter $ k x
        in MyWriter (y, u `mappend` v)


-- Задача
-- Функция execWriter запускает вычисление, содержащееся в монаде Writer,
-- и возвращает получившийся лог, игнорируя сам результат вычисления.
-- Реализуйте функцию evalWriter, которая, наоборот, игнорирует накопленный лог и возвращает только результат вычисления.
evalWriter :: MyWriter w a -> a
evalWriter m = fst (runWriter m)


-- Вспомогательные методы
tell :: Monoid w => w -> MyWriter w ()
tell w = writer ((), w)


-- Задача
-- Давайте разработаем программное обеспечение для кассовых аппаратов одного исландского магазина.
-- Заказчик собирается описывать товары, купленные покупателем, с помощью типа Shopping следующим образом:
--
type Shopping = MyWriter (Sum Integer) ()
--

purchase :: String -> Integer -> Shopping
purchase item cost = writer $ ((), (Sum cost))

--
shopping1 :: Shopping
shopping1 = do
  purchase "Jeans"   19200
  purchase "Water"     180
  purchase "Lettuce"   328
--

-- Последовательность приобретенных товаров записывается с помощью do-нотации.
-- Для этого используется функция purchase, которую вам предстоит реализовать.
-- Эта функция принимает наименование товара,
-- а также его стоимость в исландских кронах
-- (исландскую крону не принято делить на меньшие единицы, потому используется целочисленный тип Integer).
-- Кроме того, вы должны реализовать функцию total:
total :: Shopping -> Integer
total = getSum . execWriter



-- Задача
-- Измените определение типа Shopping и доработайте функцию purchase из предыдущего задания таким образом,
-- чтобы можно было реализовать функцию items, возвращающую список купленных товаров
-- (в том же порядке, в котором они были перечислены при покупке):
--
-- shopping1 :: Shopping
-- shopping1 = do
--   purchase "Jeans"   19200
--   purchase "Water"     180
--   purchase "Lettuce"   328
--
-- GHCi> total shopping1
-- 19708
-- GHCi> items shopping1
-- ["Jeans","Water","Lettuce"]
-- Реализуйте функцию items и исправьте функцию total, чтобы она работала как и прежде.
type ItemSums = (Sum Integer)
type ItemNames = [String]

type Shopping' = MyWriter (ItemSums, ItemNames) ()


purchase' :: String -> Integer -> Shopping'
purchase' item cost = writer $ ((), ((Sum cost), [item]))


shopping1' :: Shopping'
shopping1' = do
  purchase' "Jeans"   19200
  purchase' "Water"     180
  purchase' "Lettuce"   328

total' :: Shopping' -> Integer
total' = getSum . fst . execWriter

items' :: Shopping' -> [String]
items' = snd . execWriter
