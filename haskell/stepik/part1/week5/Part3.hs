module Part3 where

import Data.Char (isDigit)

--- util
-- Обратная операция $
infixl 1 &
(&) :: a -> (a -> b) -> b
x & f = f x
---------------------------------

------------------------ Монада Identity ----------------------------------------------------------------------

{- Напоминание монады
class Monad m where
    return :: a -> m a
    (>>=) :: m a -> (a -> m b) -> m b
-}

-- Монада Identity
newtype Identity a = Identity { runIdentity :: a }
    deriving (Eq, Show)

instance Monad Identity where
    return x          = Identity x
    Identity x >>= k = k x

--- Класс типов Monad теперь расширяет классы типов Functor и Applicative,
-- поэтому нужно добавить представителей для них
instance Functor Identity where
  fmap  f (Identity x) = Identity (f x)

instance Applicative Identity where
  pure x = Identity x
  Identity f <*> Identity v = Identity (f v)
----------------------------------------------------------------------------------------------------------------

-- Определеним вспомогательную функцию, которая инкрементирует значение и упаковывает его
wrap'n'succ :: Integer -> Identity Integer
wrap'n'succ x = Identity (succ x)
-- GHCi> runIdentity $ wrap'n'succ 3 >>= wrap'n'succ >>= wrap'n'succ       === 7

--- Задача
-- Если некоторый тип является представителем класса
   -- Monad
   -- , то его можно сделать представителем класса
   -- Functor
   -- , используя функцию
   -- return
   -- и оператор
   -- >>=
   -- . Причём, это можно сделать даже не зная, как данный тип устроен.
   --
   -- Пусть вам дан тип
   -- data SomeType a = ...
   -- и он является представителем класса Monad. Сделайте его представителем класса Functor.
-- instance Functor SomeType where
--     fmap f x = x >>= (return . f)


---------------------------------------------------------------------
---               Законы монад
{- Первый закон монад
return a >>= k    ===   k a

Пример применения:
return 3 >>= (\x -> Log ["adding one"] (x + 1)) === (\x -> Log ["adding one"] (x + 1)) 3
-}

{- Второй закон монад
m >>= return      ===   m

Пример применения:
let mymonad = return 3 :: Log Int
mymonad >>= return                              === mymonad
-}

{- Третий закон: ассоциативность
(m >>= k) >>= k'  ===       m >>= (\x -> k x >>= k')

Пример применения:
let addingOne = \x -> Log ["adding one"] (x + 1)
let multipleOne = \x -> Log ["multipling one"] (x + 1)

return 3 >>= addingOne >>= multipleOne         ===  return 3 >>= (\x -> addingOne x >>= multipleOne)
-}

--- Пример работы монадой Maybe с помощью do нотации
type Name = String
type Database = [(Name, Name)]

fathers = [("Bill", "John"), ("Ann", "John"), ("John", "Piter")]
mothers = [("Bill", "Jane"), ("Ann", "Jane"), ("Jane", "Alice"), ("John", "Mary")]

getF :: Name -> Maybe Name
getF name = lookup name fathers

getM :: Name -> Maybe Name
getM name = lookup name mothers

-- Ищем бабушек
granmas :: Name -> Maybe (Name, Name)
granmas person = do
    m   <- getM person
    gmm <- getM m -- по линии матери
    f   <- getF person
    gmf <- getM f -- по линии отца
    return (gmm, gmf)

-- Эквивалент с опереторатором >>=
granmas' :: Name -> Maybe (Name, Name)
granmas' person = getM person >>= (\m -> getM m >>= (\gmm -> getF person >>= (\f -> getM f >>= (\gmf -> return (gmm, gmf)))))



--- Задача
-- Рассмотрим язык арифметических выражений, которые состоят из чисел, скобок, операций сложения и вычитания.
-- Конструкции данного языка можно представить следующим типом данных:
   --
data Token = Number Int | Plus | Minus | LeftBrace | RightBrace
   deriving (Eq, Show)
   -- Реализуйте лексер арифметических выражений. Для начала реализуйте следующую функцию:
   --
   -- asToken :: String -> Maybe Token
   --
   -- Она проверяет, является ли переданная строка числом (используйте функцию
   -- isDigit
   -- из модуля
   -- Data.Char
   -- ), знаком
   -- "+"
   -- или
   -- "-"
   -- , открывающейся или закрывающейся скобкой. Если является, то она возвращает нужное значение обёрнутое в
   -- Just
   -- , в противном случае -
   -- Nothing
   -- :
   --
   -- GHCi> asToken "123"
   -- Just (Number 123)
   --
   -- GHCi> asToken "abc"
   -- Nothing
   --
   -- Далее, реализуйте функцию tokenize:
   --
   -- tokenize :: String -> Maybe [Token]
   -- Функция принимает на вход строку и если каждое слово является корректным токеном, то она возвращает список этих токенов, завёрнутый в Just. В противном случае возвращается Nothing.
   --
   -- Функция должна разбивать входную строку на отдельные слова по пробелам (используйте библиотечную функцию words). Далее, полученный список строк должен быть свёрнут с использованием функции asToken и свойств монады Maybe:
   --
   -- GHCi> tokenize "1 + 2"
   -- Just [Number 1,Plus,Number 2]
   --
   -- GHCi> tokenize "1 + ( 7 - 2 )"
   -- Just [Number 1,Plus,LeftBrace,Number 7,Minus,Number 2,RightBrace]
   --
   -- GHCi> tokenize "1 + abc"
   -- Nothing
   -- Обратите внимание, что скобки отделяются пробелами от остальных выражений!

asToken :: String -> Maybe Token
asToken str
             | all isDigit str = Just (Number (read str :: Int))
             | otherwise  = case str of "+" -> Just Plus
                                        "-" -> Just Minus
                                        "(" -> Just LeftBrace
                                        ")" -> Just RightBrace
                                        otherwise -> Nothing

tokenize' :: [String] -> Maybe [Token]
tokenize' []     = return []
-- tokenize' (x:xs) = (tokenize' xs) >>= (\ys -> (token >>= (\y -> Just (y:ys)))) where token = asToken x
tokenize' (x:xs) = do
                   ys <- tokenize' xs
                   y <- asToken x
                   return (y:ys)


tokenize :: String -> Maybe [Token]
tokenize str = tokenize' (words str)



--- Монада список

-- Примеры
l1 = [1, 2, 3]
l2 = [1, 2, 3]

-- обработка с помощью list comprehension
touples1 = [(x, y) | x <- l1, y <- l2]

-- Обработка через do нотацию
touples2 = do
    x <- l1
    y <- l2
    return (x, y)

-- Оба варианта преобразуются в такой синтаксис
touples3 = l1 >>= (\x -> (l2 >>= (\y -> return (x, y))))

--- Задача
--
-- Пусть имеется тип данных, который описывает конфигурацию шахматной доски:
-- data Board = ...
-- Кроме того, пусть задана функция
-- nextPositions :: Board -> [Board]
-- которая получает на вход некоторую конфигурацию доски и возвращает все возможные конфигурации,
-- которые могут получиться, если какая-либо фигура сделает один ход. Напишите функцию:
-- nextPositionsN :: Board -> Int -> (Board -> Bool) -> [Board]
-- которая принимает конфигурацию доски, число ходов n, предикат и возвращает все возможные конфигурации досок,
-- которые могут получиться, если фигуры сделают n ходов и которые удовлетворяют заданному предикату.
-- При n < 0 функция возвращает пустой список.
--
data Board = Board Int deriving Show
nextPositions :: Board -> [Board]
nextPositions (Board x) = map Board [x-1, x+1]

nextPositionsN :: Board -> Int -> (Board -> Bool) -> [Board]
nextPositionsN b n pred
                        | n < 0     = []
                        | n == 0    = if (pred b) then [b] else []
                        | otherwise = do
                                        p <- filter pred $ nextPositions b
                                        return p ++ nextPositionsN p (n - 1) pred

stupidPredicate :: Board -> Bool
stupidPredicate (Board x) = odd x


--- Обработка ветвлений в монадических вычислениях

-- Для начала посмотрим как условия из list comprehension
-- возращаем пары с неравными элементами
lst1 = [(x, y) | x <- l1, y <- l2, x /= y] -- list comprehension
lst2 = do                                  -- do нотация
        x <- l1
        y <- l2
        True <- return (x /= y)
        return (x,y)
lst3 = l1 >>= (\x ->                       -- В такой вид транслируются оба выражения на низком уровне
       l2 >>= (\y ->
       return (x /= y) >>= (\b ->
       case b of True -> return (x, y)
                 _    -> fail "...")))


-- На самом деле не столь важно булево значение определяет наличие или нет
lst4 = do
        x <- l1                        -- цикл 1: пробегаемся по всем элементам l1
        y <- l2                        -- цикл 2: пробегаемся по всем элементам l2
        if (x /= y) then "X" else []   -- цикл 3: пробегаемся по циклу правильных решений (тут его размер мб от 0 до 1)
        return (x, y)

--- Задача
-- Используя монаду списка и do-нотацию, реализуйте функцию
   --
   -- pythagoreanTriple :: Int -> [(Int, Int, Int)]
   --
   -- которая принимает на вход некоторое число x и возвращает список троек (a,b,c), таких что
   --
   -- a2+b2=c2,a>0,b>0,c>0,c≤x,a<b
   --
   -- Число x может быть ≤0 , на таком входе должен возвращаться пустой список.
   --
   -- GHCi> pythagoreanTriple 5
   -- [(3,4,5)]
   --
   -- GHCi> pythagoreanTriple 0
   -- []
   --
   -- GHCi> pythagoreanTriple 10
   -- [(3,4,5),(6,8,10)]
pythagoreanTriple :: Int -> [(Int, Int, Int)]
pythagoreanTriple n
                    | n <= 0 = []
                    | otherwise = do
                        c <- [1..n]
                        b <- [1..c]
                        a <- [1..b]
                        True <- return (a * a + b * b == c * c)
                        return (a, b, c)

-- [1..n] >>= (\c -> ([1..c] >>= (\b -> ([1..b] >>= (\a -> if (((a ** 2) + (b **2)) == ((c ** 2))) then return (a,b,c) else []      )))))


-- Поиск палиндромов
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome xs = xs == reverse xs

findPalindroms :: Int -> Int
findPalindroms n
                    | n <= 0     = 0
                    | otherwise = res + findPalindroms (n - 1) where res = if (isPalindrome . show $ n) then 1 else 0

-- Поиск повторяющихся символов
findReplacedSymbols :: (Eq a) => [a] -> [a]
findReplacedSymbols xs = helper xs [] where helper = (sort xs)



