module Part2 where

import Control.Monad (ap, liftM)

--- Задача
-- Введём следующий тип:
   --
data Log a = Log [String] a deriving Show
   -- Реализуйте вычисление с логированием, используя Log. Для начала определите функцию toLogger
   --
toLogger :: (a -> b) -> String -> (a -> Log b)
toLogger f msg = Log [msg] . f
   -- которая превращает обычную функцию, в функцию с логированием:
   --
-- let add1Log = toLogger (+1) "added one"
   -- GHCi> add1Log 3
   -- Log ["added one"] 4
   --
-- let mult2Log = toLogger (* 2) "multiplied by 2"
   -- GHCi> mult2Log 3
   -- Log ["multiplied by 2"] 6
   -- Далее, определите функцию execLoggers
   --
execLoggers :: a -> (a -> Log b) -> (b -> Log c) -> Log c
-- execLoggers a' (a -> Log (x:xs) b') (b -> Log (y:ys) c) =
execLoggers a f g =  Log (xs ++ ys) c where
                            (Log ys c) = g b
                            (Log xs b) = f a
   --
   -- Которая принимает некоторый элемент и две функции с логированием.
   -- execLoggers
   -- возвращает результат последовательного применения функций к элементу и список сообщений, которые были выданы при применении каждой из функций:
-- execLoggers 3 add1Log mult2Log
   -- Log ["added one","multiplied by 2"] 8



-- Монада - представляет собой обобщенный интерфейс контекста вычисления
-- Делается это за счет стрелки Клейсли (a -> m b)

-- Возможное определение монады
{--
class Monad m where
    return :: a -> m a
    (>>=)  :: m a -> (a -> m b) -> m b -- оператор bind

    -- Дополнительные функции c реализацией по умолчанию

    -- "облегченное" связывание
    (>>)   :: m a -> m b -> b
    x >> y = x >>= \_ -> y

    -- "ошибочное" вычисление
    fail :: String -> m a
    fail s = error s

infixl 1 >>=
--}


-- Удобные функции для работы с монадами

--  Можно написать собственное преобразование морфизма в стрелку Клейсли
toKleisli :: Monad m => (a -> b) -> (a -> m b)
toKleisli f = \x -> return (f x)
-- еще один валидный вариант
-- toKleisli f x = return (f x)

-- Функция для разворота конвеера вычислений
(=<<) :: Monad m => (a -> m b) -> m a -> m b
(=<<) = flip (>>=)

-- Оператор рыбки - оператор композиции монадических вычислений (композиция стрелок Клейсли)
(<=<) :: Monad m => (b -> m c) -> (a -> m b) -> (a -> m c)
f <=< g = \x -> g x >>=  f


-- Обратная операция $
infixl 1 &
(&) :: a -> (a -> b) -> b
x & f = f x

--- Задача
-- Функции с логированием из предыдущего задания возвращают в качестве результата значение с некоторой дополнительной информацией в виде списка сообщений. Этот список является контекстом. Реализуйте функцию returnLog
   --
   -- returnLog :: a -> Log a
   --
   -- которая является аналогом функции
   -- return
   -- для контекста
   -- Log
   -- . Данная функция должна возвращать переданное ей значение с пустым контекстом.
returnLog :: a -> Log a
returnLog = Log []

--- Задача
-- Реализуйте фукцию bindLog
   --
   -- bindLog :: Log a -> (a -> Log b) -> Log b
   -- которая работает подобно оператору >>= для контекста Log.
   --
   -- GHCi> Log ["nothing done yet"] 0 `bindLog` add1Log
   -- Log ["nothing done yet","added one"] 1
   --
   -- GHCi> Log ["nothing done yet"] 3 `bindLog` add1Log `bindLog` mult2Log
   -- Log ["nothing done yet","added one","multiplied by 2"] 8
bindLog :: Log a -> (a -> Log b) -> Log b
bindLog (Log logsA a) f = Log (logsA ++ logsB) b where (Log logsB b) = f a

--- Задача
-- Реализованные ранее returnLog и bindLog позволяют объявить тип Log представителем класса Monad:
   --

instance Functor Log where
  fmap = liftM

instance Applicative Log where
  pure = return
  (<*>) = ap

instance Monad Log where
   return = returnLog
   (>>=) = bindLog
   -- Используя return и >>=, определите функцию execLoggersList
   --
   -- execLoggersList :: a -> [a -> Log a] -> Log a
   -- которая принимает некоторый элемент,
   -- список функций с логированием
   -- и возвращает результат последовательного применения всех функций в списке к переданному элементу
   -- вместе со списком сообщений, которые возвращались данными функциями:
   --
   -- GHCi> execLoggersList 3 [add1Log, mult2Log, \x -> Log ["multiplied by 100"] (x * 100)]
   -- Log ["added one","multiplied by 2","multiplied by 100"] 800
execLoggersList :: a -> [a -> Log a] -> Log a
execLoggersList a fs = foldl (>>=) (return a) fs

-- Мой второй вариант на "поиграться с рыбкой"
composeKleislies :: Monad m => [a -> m a] -> a -> m a
composeKleislies (f:[]) = f
composeKleislies (f:fs) = f <=< composeKleislies fs

execLoggersList' :: a -> [a -> Log a] -> Log a
execLoggersList' a fs = return a >>= (composeKleislies fs)

