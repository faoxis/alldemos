module Part1 where

import Data.Char

------------------------------------------------------
------------------------------------------------------
------------- Functor --------------------------------
class Functor' f where
    fmap' :: (a -> b) -> f a -> f b

-- Представитель функтора для списка
instance Functor' [] where
    fmap' = map

-- Представитель функтор для Maybe
instance Functor' Maybe where
    fmap' _ Nothing  = Nothing
    fmap' f (Just a) = Just (f a)

-- Представитель функтора для бинарного дерева
data Tree a = Leaf a | Branch (Tree a) a (Tree a)
                deriving Show


instance Functor' Tree where
    fmap' f (Leaf a)                  = Leaf (f a)
    fmap' f (Branch left value right) = Branch (fmap' f left) (f value) (fmap' f right)

testTree = Branch (Branch (Leaf 2) 3 (Leaf 4)) 5 (Leaf 6)
-- Функтор из пакета Data.Functor можно вызывать как (*2) <$> testTree
-- Таким образом можно описать последовательность дейтвий над контейнером:
-- (+5) <$> (*2) <$> [1, 2, 3]
-- Оператор <$ позволяет поменять значение контейнера 1 <$ [3, 2, 1]


-- !!!!
-- Функтор с несколькими типовыми параметрами делается путем сокращения типовых параметров
-- Например, мы можем задачать для Either некоторый тип левого параметра Either Int
-- и тогда это уже становится тип с одним параметром
instance Functor' (Either a) where -- на самом деле мы тут говорим, что первый параметр Either не важен
    fmap' _ (Left x)  = Left x
    fmap' f (Right x) = Right $ f x

instance Functor' ((,) s) where
    fmap' g (x, y) = (x, g y)

-- Функторы применимы даже к морфизмам
instance Functor' ((->) e) where
    fmap' = (.)
-- Композиция является представителем функтора для морфизмов

{- Законы функторов
(1) fmap' id x      = id x
(2) fmap' (f . g) x = (fmap f . fmap g) x
-}


--- Задача
-- Определите представителя класса Functor для следующего типа данных, представляющего точку в трёхмерном пространстве:
--
data Point3D a = Point3D a a a deriving Show
--
-- GHCi> fmap (+ 1) (Point3D 5 6 7)
-- Point3D 6 7 8
instance Functor' Point3D where
    fmap' f (Point3D x y z) = (Point3D (f x) (f y) (f z))

--- Задача
-- Определите представителя класса Functor для типа данных GeomPrimitive, который определён следующим образом:
   --
data GeomPrimitive a = Point (Point3D a) | LineSegment (Point3D a) (Point3D a) deriving Show
   -- При определении, воспользуйтесь тем, что Point3D уже является представителем класса Functor.
   --
   -- GHCi> fmap (+ 1) $ Point (Point3D 0 0 0)
   -- Point (Point3D 1 1 1)
   --
   -- GHCi> fmap (+ 1) $ LineSegment (Point3D 0 0 0) (Point3D 1 1 1)
   -- LineSegment (Point3D 1 1 1) (Point3D 2 2 2)
instance Functor' GeomPrimitive where
    fmap' f (Point x)         = Point (fmap' f x)
    fmap' f (LineSegment x y) = LineSegment (fmap' f x) (fmap' f y)


--- Задача
-- Определите представителя класса Functor для бинарного дерева, в каждом узле которого хранятся элементы типа Maybe:
   --
data Tree' a = Leaf' (Maybe a) | Branch' (Tree' a) (Maybe a) (Tree' a) deriving Show
   --
   -- GHCi> words <$> Leaf Nothing
   -- Leaf Nothing
   --
   -- GHCi> words <$> Leaf (Just "a b")
   -- Leaf (Just ["a","b"])
instance Functor' Tree' where
    fmap' f (Leaf' x)       = Leaf' (fmap' f x)
    fmap' f (Branch' x y z) = Branch' (fmap' f x) (fmap' f y) (fmap' f z)

--- Задача
-- Определите представителя класса Functor для типов данных Entry и Map. Тип Map представляет словарь, ключами которого являются пары:
   --
data Entry k1 k2 v = Entry (k1, k2) v  deriving Show
data Map k1 k2 v = Map [Entry k1 k2 v]  deriving Show
   --
   -- В результате должно обеспечиваться следующее поведение: fmap применяет функцию к значениям в словаре, не изменяя при этом ключи.
   --
   -- GHCi> fmap (map toUpper) $ Map []
   -- Map []
   --
   -- GHCi> fmap (map toUpper) $ Map [Entry (0, 0) "origin", Entry (800, 0) "right corner"]
   -- Map [Entry (0,0) "ORIGIN",Entry (800,0) "RIGHT CORNER"]
instance Functor' (Entry a b) where
    fmap' f (Entry (k1, k2) v) = Entry (k1, k2) (f v)
instance Functor' (Map a b) where
    fmap' f (Map x) = Map (fmap' (fmap' f) x)


