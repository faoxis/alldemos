module Part8State where

import Control.Monad (replicateM, ap, liftM)
import Part6Reader
import Part7Writer

-- Монада State

newtype MyState s a = MyState { runState :: s -> (a, s) }

instance Functor (MyState w) where
  fmap = liftM

instance Applicative (MyState w) where
  pure = return
  (<*>) = ap


instance Monad (MyState s) where
    return a = MyState $ \s -> (a, s)
    m >>= k = MyState $ \s ->
                let (a, s') = runState m s
                    m'      = k a
                in runState m' s'

-- Наиболее полезные методы

-- Запустить вычисление и посмотреть состояние
execState :: MyState s a -> s -> s
execState m = snd . (runState m)

-- Запустить вычисление и посмотреть результат
evalState :: MyState s a -> s -> a
evalState m s = fst (runState m s)


-- Получение текущего состояния
get :: MyState s s
get = MyState $ \st -> (st, st)

-- Запись текущего состояния
put :: s -> MyState s ()
put st = MyState $ \_ -> ((), st)

-- Пример использования
tick :: MyState Int Int
tick = do
        n <- get
        put (n + 1)
        return n

-- Изменение текущего состояние без изменения значения
modify :: (s -> s) -> MyState s ()
-- modify f = State $ \s -> ((), f s)
modify f = do
            s <- get
            put (f s)

-- Задача
-- Давайте убедимся, что с помощью монады State можно эмулировать монаду Reader.
--
-- Напишите функцию readerToState, «поднимающую» вычисление из монады Reader в монаду State:
--
-- GHCi> evalState (readerToState $ asks (+2)) 4
-- 6
-- GHCi> runState (readerToState $ asks (+2)) 4
-- (6,4)

readerToState :: MyReader r a -> MyState r a
readerToState m = MyState $ \s -> ((runReader m s), s)



-- Задача
-- Теперь убедимся, что с помощью монады State можно эмулировать монаду Writer.
--
-- Напишите функцию writerToState, «поднимающую» вычисление из монады Writer в монаду State:
--
-- GHCi> runState (writerToState $ tell "world") "hello,"
-- ((),"hello,world")
-- GHCi> runState (writerToState $ tell "world") mempty
-- ((),"world")
--
-- Обратите внимание на то, что при работе с монадой Writer предполагается, что изначально лог пуст
-- (точнее, что в нём лежит нейтральный элемент моноида),
-- поскольку интерфейс монады просто не позволяет задать стартовое значение.
-- Монада State же начальное состояние (оно же стартовое значение в логе) задать позволяет.
writerToState :: Monoid w => MyWriter w a -> MyState w a
writerToState m = MyState $  \s -> (value, s `mappend` log)  where (value,  log) = runWriter m



-- Посмотрим примеры использования монады State
succ' :: Int -> Int
succ' n = execState tick n

plus :: Int -> Int -> Int
plus n x = execState (sequence $ replicate n tick) x
-- replicate повторяет действие n раз
-- sequence заменяет список монад на монаду списка

{- Связка sequence и reqplicate встречается довольно часто и поэтому выделена в отдельный метод
replicateM :: (Monad m) => Int -> m a -> m [a]
replicateM n = sequence . replicate n
-}
plus' :: Int -> Int -> Int
plus' n x = execState (replicateM n tick) x
