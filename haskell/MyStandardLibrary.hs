module MyStandardLibrary where

-- foldr' (-) 0 [1,2,3] == (1 - (2 - (3 - 0)))
foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f startValue [] = startValue
foldr' f startValue (x:xs) = x `f` foldr' f startValue xs

-- foldl' (-) 0 [1,2,3] == 0 - 1 - 2 - 3
foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' f startValue [] = startValue
foldl' f startValue (x:xs) = foldl f (startValue `f` x) xs

