module Domain.Auth where

import ClassyPrelude

newtype Email = Email { emailRaw :: Text } deriving (Eq, Show)

rawEmail :: Email -> Text
rawEmail = emailRaw

data EmailValidationError = EmailValidationErrorInvalidEmail

mkEmail :: Text -> Either [EmailValidationError] Email
mkEmail = undefined

newtype Password = Password { passwordRaw :: Text } deriving (Eq, Show)

rawPassword :: Password -> Text
rawPassword = passwordRaw


data PasswordValidationError = PasswordValidationErrorLength Int
        | PasswordValidationErrorMustContainUpperCase
        | PasswordValidationErrorMustContainLowerCase
        | PasswordValidationErrorMustContainNumber

mkPassword :: Text -> Either [PasswordValidationError] Password
mkPassword = undefined

data Auth = Auth { authEmail :: Email, authPassword :: Password } deriving (Show, Eq)






