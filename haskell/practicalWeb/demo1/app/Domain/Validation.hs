module Domain.Validation where


import ClassyPrelude
import Text.Regex.PCRE.Heavy

type Validation e a = a -> Maybe e

