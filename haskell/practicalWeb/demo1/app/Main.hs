module Main where

import ClassyPrelude
import Lib
import Data.Aeson
import Data.Aeson.TH

main :: IO ()
main = someFunc

data User = User { userId :: Int, userName :: Text, userHobbies :: [Text] } deriving (Show)
$(deriveJSON defaultOptions ''User)
