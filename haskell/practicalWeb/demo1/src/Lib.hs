module Lib
    ( someFunc
--     , jsonExample
    ) where

import ClassyPrelude

-- jsonExample = Text
-- jsonExample = "{"
--     \"id\": 123,
--     \"name\": \"Ecky\",
--     \"hobbies\": [\"Running\", \"Programming\"],
--     \"country\": null
-- }
-- "

someFunc :: IO ()
someFunc = putStrLn "someFunc"
