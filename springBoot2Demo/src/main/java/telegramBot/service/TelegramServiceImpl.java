package telegramBot.service;

import org.springframework.stereotype.Service;
import telegramBot.client.TelegramClient;
import telegramBot.domain.AnswerResult;

import java.util.TreeSet;

@Service
public class TelegramServiceImpl implements TelegramService {

    private TelegramClient telegramClient;

    public TelegramServiceImpl(TelegramClient telegramClient) {
        this.telegramClient = telegramClient;
    }

    private TreeSet<AnswerResult> messages = new TreeSet();

    @Override
    public void makeEcho() {
        telegramClient
                .getMessages()
                .subscribe(s -> s
                        .stream()
                        .filter(message -> !messages.contains(message))
                        .forEach(message -> {
                            messages.add(message);
                            long chatId = message.getMessage().getChat().getId();
                            String msg = message.getMessage().getText();
                            telegramClient.sendMessage(chatId, msg);
                            System.out.println("done");
                        })
                );
    }

}
