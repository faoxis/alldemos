package telegramBot.client;

import reactor.core.publisher.Mono;
import telegramBot.domain.AnswerResult;

import java.util.Set;

public interface TelegramClient {
    Mono<Set<AnswerResult>> getMessages();
    void sendMessage(long chatId, String message);
}
