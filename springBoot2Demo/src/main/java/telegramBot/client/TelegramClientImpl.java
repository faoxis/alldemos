package telegramBot.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import telegramBot.domain.Answer;
import telegramBot.domain.AnswerResult;
import telegramBot.domain.Request;

import javax.annotation.PostConstruct;
import java.util.Set;

@Repository
public class TelegramClientImpl implements TelegramClient {

    @Value("${bot.token}")
    private String token;

    private WebClient webClient;

    @PostConstruct
    private void initWebClient() {
        webClient = WebClient
                .create("http://165.22.73.204/telegram/bot" + token + "/getUpdates");
    }

    @Override
    public Mono<Set<AnswerResult>> getMessages() {
        Mono<Answer> res = WebClient.create("https://api.telegram.org/bot" + token + "/getUpdates")
                .get()
                .retrieve()
                .bodyToMono(Answer.class);

        return res
                .map(Answer::getResult)
                .doOnError(e -> System.out.println(e.getMessage()));
    }

    @Override
    public void sendMessage(long chatId, String message) {
        WebClient.create("https://api.telegram.org/bot" + token + "/sendMessage")
                .post()
                .body(BodyInserters.fromObject(new Request(chatId, message)))
                .retrieve()
                .bodyToMono(String.class)
                .doOnError(System.out::println)
                .subscribe(System.out::println);
    }



}
