package telegramBot;


import org.reactivestreams.Subscriber;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import telegramBot.client.TelegramClient;
import telegramBot.service.TelegramService;

import javax.annotation.PostConstruct;


@Component
@SpringBootApplication
public class TelegramBotApplication {

    public static void main(String[] args) throws Exception {
        ApplicationContext context = SpringApplication.run(TelegramBotApplication.class, args);
        TelegramService service = context.getBean(TelegramService.class);
        TelegramClient client = context.getBean(TelegramClient.class);
//        client.getMessages().subscribe(System.out::println);
        while (true) {
            service.makeEcho();
            Thread.sleep(1_000);
        }
//        Subscriber
//        client.sendMessage(126927462, "Hello from reactive java");
//        while (true) {
//            service.makeEcho();
//            Thread.sleep(10_000);
//        }

//        TelegramClient telegramClient = context.getBean(TelegramClient.class);
//        telegramClient.getMessages();
    }


    @PostConstruct
    public void getSomeBean() {
//        String token = "749701273:AAFtq7c2NabGWvHdpsAUXdksIoiHcFy97e8";
//        String url = "https://api.telegram.org/bot" + token + "/";
//        System.out.println("================================= heeey! ========================================================");
//        WebClient webClient = WebClient.create("http://vbdsme.ru/swagger-ui.html");
//        Mono<String> mono = webClient.get().retrieve().bodyToMono(String.class);
//        mono.subscribe(System.out::println);

    }

}
