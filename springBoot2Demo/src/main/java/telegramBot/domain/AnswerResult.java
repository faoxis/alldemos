package telegramBot.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AnswerResult {

    @JsonProperty("update_id")
    private long updateId;

    private Message message;
}
