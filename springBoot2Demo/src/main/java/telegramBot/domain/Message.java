package telegramBot.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class Message {

    @JsonProperty("message_id")
    private String messageId;

    private From from;

    private Chat chat;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    private long date;

    private String text;

    @JsonProperty("new_chat_participant")
    private NewChatParticipant newChatParticipant;

    @JsonProperty("new_chat_member")
    private NewChatParticipant newChatMember;

    @JsonProperty("new_chat_members")
    private List<NewChatParticipant> newChatMembers;


}
