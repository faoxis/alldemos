package telegramBot.domain;

import lombok.Data;

import java.util.Set;

@Data
public class Answer {

    private Boolean ok;
    private Set<AnswerResult> result;
}
