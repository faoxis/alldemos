package telegramBot.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Chat {
    private long id;
    private String title;
    private String group;

    @JsonProperty("all_members_are_administrators")
    private boolean allMembersAreAdministrators;
}
