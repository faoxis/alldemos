package algorithms.rodStivens.structures

fun makeBinaryHeap(mass: Array<Int>) {
    for (i in 0 until mass.size) {
        var index = i
        while (index != 0) {
            val parent = (index - 1) / 2
            if (mass[index] <= mass[parent]) break

            val temp = mass[index]
            mass[index] = mass[parent]
            mass[parent] = temp

            index = parent
        }
    }
}

fun getTopElement(mass: Array<Int>, size: Int): Int {
    val result = mass[0]
    mass[0] = mass[size - 1]

    var index = 0
    while (true) {
        var child1 = index * 2 + 1
        var child2 = index * 2 + 2

        if (child1 >= size) child1 = index
        if (child2 >= size) child2 = index

        if (mass[child1] <= mass[index] && mass[child2] <= mass[index]) {
            break
        }

        val indexForSwap = if (mass[child1] >= mass[child2]) child1
                           else child2

        val tmp = mass[indexForSwap]
        mass[indexForSwap] = mass[index]
        mass[index] = tmp

        index = indexForSwap
    }

    return result
}



