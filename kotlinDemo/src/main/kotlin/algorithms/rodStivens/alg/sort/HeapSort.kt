package algorithms.rodStivens.alg.sort

import algorithms.rodStivens.structures.getTopElement
import algorithms.rodStivens.structures.makeBinaryHeap

fun heapSort(mass: Array<Int>) {
    makeBinaryHeap(mass)
    for (size in mass.size downTo 1) {
        val res = getTopElement(mass, size)
        mass[size - 1] = res
    }
}