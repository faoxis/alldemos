package talktonobody.rabbit

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.rabbitmq.client.CancelCallback
import com.rabbitmq.client.Channel
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.DeliverCallback
import org.slf4j.LoggerFactory
import reactor.core.publisher.Flux
import java.nio.charset.Charset

interface Connector {
    fun send(message: Any)
    fun <T> doOnReceive(clazz: Class<T>, action: (T) -> Unit)
}

class RabbitConnector: Connector {
    override fun <T> doOnReceive(clazz: Class<T>, action: (T) -> Unit) {
        val deliverCallback = DeliverCallback { _, delivery ->
            val message = String(delivery.body, Charset.forName("UTF-8"))
            val readyToHandleMessage = objectMapper.readValue(message, clazz)
            action(readyToHandleMessage)
        }
        channel.basicConsume("telegram-message", true, deliverCallback, CancelCallback { })
    }

    private val channel: Channel
    private val objectMapper: ObjectMapper
    private val queueName: String
    private val logger = LoggerFactory.getLogger(RabbitConnector::class.java)

    constructor(host: String, queueName: String) {
        this.queueName = queueName

        val facotory = ConnectionFactory()
        facotory.host = host


        val connection = facotory.newConnection()
        val channel = connection.createChannel()
        channel.queueDeclare(queueName, false, false, false, null)
        this.channel = channel

        Runtime.getRuntime().addShutdownHook(Thread {
            connection?.close()
        })

        objectMapper = ObjectMapper().registerKotlinModule()
    }

    override fun send(message: Any) {
        val preparedMessage = objectMapper.writeValueAsBytes(message)
        channel.basicPublish("", queueName, null, preparedMessage)
    }



}
