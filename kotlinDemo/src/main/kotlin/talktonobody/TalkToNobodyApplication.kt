package talktonobody

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.TopicExchange
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import talktonobody.flow.*
import talktonobody.telegram.AnswerResult
import talktonobody.telegram.MessageWrapper
import talktonobody.telegram.TelegramBotRepository
import java.time.LocalDateTime

@SpringBootApplication
class TalkToNobodyApplication

internal val topicExchangeName = "question-exchange"
internal val queueName = "question"

@Bean
internal fun queue(): Queue {
    return Queue(queueName, false)
}

@Bean
internal fun exchange(): TopicExchange {
    return TopicExchange(topicExchangeName)
}

//@Bean
//internal fun binding(queue: Queue, exchange: TopicExchange): Binding {
//    return BindingBuilder.bind(queue).to(exchange).with("foo.bar.#")
//}

@Bean
internal fun container(connectionFactory: ConnectionFactory,
                       listenerAdapter: MessageListenerAdapter): SimpleMessageListenerContainer {
    val container = SimpleMessageListenerContainer()
    container.connectionFactory = connectionFactory
    container.setQueueNames(queueName)
    container.setMessageListener(listenerAdapter)
    return container
}

@Bean
internal fun listenerAdapter(receiver: Receiver): MessageListenerAdapter {
    return MessageListenerAdapter(receiver, "receiveMessage")
}

@Component
class Receiver {

    fun receiveMessage(message: String) {
        println("Received <$message>")
    }

    fun receiveMessage(bytes: ByteArray) {
        receiveMessage(String(bytes))
    }
}


fun main() {
    runApplication<TalkToNobodyApplication>()

//    val botRepository = context.getBean(TelegramBotRepository::class.java)
//    botRepository.getLastMessages().subscribe {
//        println(it)
//    }



//    val url: String = "http://165.22.73.204/telegram/bot857934255:AAG2ybajD8MjMd6MX2gN9LzunckZs3V3Ovc"
//    val client: WebClient = WebClient.builder().exchangeStrategies(getExchangeStrategies()).baseUrl(url).build()

//    val userRepository = context.getBean(UserRepository::class.java)
//    userRepository.save(User(1L, UserState.QUESTION_WAITER, LocalDateTime.now(), LocalDateTime.now())).block()
//    userRepository.findAllByState(UserState.QUESTION_WAITER).subscribe { println(it) }

//    val questionRepository = context.getBean(QuestionRepository::class.java)
//    questionRepository.save(Question(
//            id = null,
//            authorChatId = 1L,
//            text =" 12424",
//            done = false,
//            answer = null,
//            answerChatId = null,
//            state = QuestionState.CREATED,
//            updatedAt = LocalDateTime.now(),
//            createdAt = LocalDateTime.now()
//    )).block()
//    questionRepository.findAll().subscribe(::println)
//    val repository = context.getBean(TelegramBotRepository::class.java)
//    val cache = context.getBean(RecordsRepository::class.java)
//    repository.getLastMessages().collectList().block()?.forEach(::println)
//    println(cache.saveFoo("one", "foo"))
//    println(cache.saveFoo("one", "foo"))



}
