package talktonobody.telegram

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Chat (
        val id: Long,
        val title: String? = null,
        val group: String? = null,

        @get:JsonProperty("all_members_are_administrators")
        val allMembersAreAdministrators: Boolean = false
)

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
data class From (

        val id: Long,

        @get:JsonProperty("is_bot")
        var isBot: Boolean = false,

        @get:JsonProperty("first_name")
        var firstName: String? = null,

        @get:JsonProperty("last_name")
        val lastName: String? = null,

        val username: String? = null,

        @get:JsonProperty("language_code")
        val languageCode: String? = null
)

data class NewChatParticipant (

        val id: Long = 0,

        @get:JsonProperty("is_bot")
        val isBot: Boolean = false,

        @get:JsonProperty("first_name")
        val firstName: String? = null,
        val username: String? = null
)

data class Message (

        @get:JsonProperty("message_id")
        val messageId: String? = null,

        val from: From,

        val chat: Chat,

        @get:JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
        val date: Long = 0,

        val text: String? = null,

        @get:JsonProperty("new_chat_participant")
        val newChatParticipant: NewChatParticipant? = null,

        @get:JsonProperty("new_chat_member")
        val newChatMember: NewChatParticipant? = null,

        @get:JsonProperty("new_chat_members")
        val newChatMembers: List<NewChatParticipant>? = null

)

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
data class AnswerResult (

        @get:JsonProperty("update_id")
        val updateId: Long = 0,
        val message: Message?,
        @get:JsonProperty("edited_message")
        val editedMessage: Message?
)

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
data class MessageWrapper(val ok: Boolean, val result: List<AnswerResult>? = listOf())

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
data class MessageToChat(
        @JsonProperty("chat_id")
        val chatId: String,
        val text: String
)
