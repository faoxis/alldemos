package talktonobody.telegram

import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.core.scheduler.Schedulers
import talktonobody.rabbit.RabbitConnector
import java.lang.Thread.sleep
import java.time.Duration
import javax.annotation.PostConstruct
import kotlin.concurrent.thread


@Component
class TelegramHandler(
        private val botRepository: TelegramBotRepository,
        private val repository: TelegramRepository) {

    private val rabbitSender = RabbitConnector("localhost", "telegram-message")

    @PostConstruct
    fun run() {
        repository
                .getLastUpdateId()
                .map { it + 1 }
                .defaultIfEmpty(0L)
                .publishOn(Schedulers.elastic())
                .repeatWhen { Flux.interval(Duration.ofSeconds(5)) }
                .flatMap(botRepository::getLastMessages)
                .publishOn(Schedulers.elastic())
                .filter { repository.saveAnswer(it).block() != 0L }
                .onErrorResume { err -> err.printStackTrace(); Flux.empty<AnswerResult>() }
                .subscribe {
                    repository.setLastUpdateId(it.updateId).subscribe {}
                    rabbitSender.send(it)
                }

    }


}