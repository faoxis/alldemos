package talktonobody.telegram

import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.http.codec.LoggingCodecSupport
import org.springframework.web.reactive.function.client.*
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient
import org.springframework.web.reactive.socket.client.WebSocketClient
import reactor.core.publisher.Flux
import reactor.core.scheduler.Schedulers
import java.net.URI
import java.time.Duration


interface TelegramBotRepository {
    fun getLastMessages(offset: Long = 0L): Flux<AnswerResult>
    fun sendMessage(messageToChat: MessageToChat): Mono<ClientResponse>
}

@Component
class TelegramBotRepositoryImpl : TelegramBotRepository {

    val logger = LoggerFactory.getLogger(TelegramBotRepositoryImpl::class.java)

    override fun sendMessage(messageToChat: MessageToChat): Mono<ClientResponse> =
            client
                    .post()
                    .uri(SEND_COMMAND)
                    .body(Mono.just(messageToChat))
                    .exchange()


    override fun getLastMessages(offset: Long): Flux<AnswerResult>  {
        return client
                .get()
                .uri("$UPDATE_COMMAND?offset=$offset")
                .exchange()
                .filter { !it.headers().contentType().filter { it.includes(MediaType.TEXT_HTML) }.isPresent }
                .flatMap { it.bodyToMono(MessageWrapper::class.java) }
                .map {it.result}
                .flatMapMany { if (it != null && !it.isEmpty()) Flux.fromIterable(it) else Flux.empty() }
    }



    private val url: String = "http://165.22.73.204/telegram/bot857934255:AAG2ybajD8MjMd6MX2gN9LzunckZs3V3Ovc"
    private val client: WebClient = WebClient.builder().baseUrl(url).build()
    private val UPDATE_COMMAND: String = "/getUpdates"
    private val SEND_COMMAND: String = "/sendMessage"

    private fun getExchangeStrategies(): ExchangeStrategies {
        val exchangeStrategies = ExchangeStrategies.withDefaults()
        exchangeStrategies
                .messageWriters().stream()
                .filter {it is LoggingCodecSupport }
                .forEach { writer -> (writer as LoggingCodecSupport).isEnableLoggingRequestDetails = true }
        return exchangeStrategies
    }
}