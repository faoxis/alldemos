package talktonobody.telegram

import org.springframework.data.redis.core.ReactiveRedisTemplate
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono

interface TelegramRepository {
    fun saveAnswer(answerResult: AnswerResult): Mono<Long?>
    fun setLastUpdateId(updateId: Long): Mono<Unit>
    fun getLastUpdateId(): Mono<Long>
}

@Repository
class TelegramRepositoryImpl(private val template: ReactiveRedisTemplate<Any, Any>) : TelegramRepository {

    override fun getLastUpdateId(): Mono<Long> {
        return template.opsForValue().get("lastUpdateId").map { (it as Int).toLong() }
    }


    override fun setLastUpdateId(updateId: Long): Mono<Unit> {
        return template.opsForValue().set("lastUpdateId", updateId).map {  }
    }

    override fun saveAnswer(answerResult: AnswerResult): Mono<Long?> =
            template.opsForSet().add("telegramAnswerSet", answerResult)


}
