package talktonobody.flow

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.rabbitmq.client.Address
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Delivery
import org.slf4j.LoggerFactory
import org.springframework.data.redis.core.ReactiveRedisTemplate
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.scheduler.Schedulers
import talktonobody.rabbit.RabbitConnector
import talktonobody.telegram.AnswerResult
import talktonobody.telegram.MessageToChat
import talktonobody.telegram.TelegramBotRepository
import java.time.LocalDateTime
import javax.annotation.PostConstruct
import reactor.rabbitmq.ReceiverOptions
import reactor.rabbitmq.SenderOptions
import reactor.rabbitmq.RabbitFlux
import java.nio.charset.Charset


@Service
class LogicFlow(private val template: ReactiveRedisTemplate<Any, Any>,
                private val userRepository: UserRepository,
                private val questionRepository: QuestionRepository,
                private val telegramBotRepository: TelegramBotRepository) {

    val welcomeQuestionMessage = """
        Добро пожаловать!
        Этот канал посвящен общению со случайными собеседниками.
        Вы задаете вопрос/утверждение и через какое-то временя получаете ответ.
        Далее вам задают вопрос/утверждение, вы отвечате и так по кругу.
        Давайте начнем. Спросите вселенную о чем-нибудь (например, про день недели):
    """.trimIndent()

    val welcomAnswerMessage = """
        Добро пожаловать!
        Этот канал посвящен общению со случайными собеседниками.
        Вы задаете вопрос/утверждение и через какое-то временя получаете ответ.
        Далее вам задают вопрос/утверждение, вы отвечате и так по кругу.
        Давайте начнем. Просто напишите ответ на вопрос, который к вам скоро поступит:
    """.trimIndent()

    private val connector = RabbitConnector("localhost", "telegram-message")
    private val logger = LoggerFactory.getLogger(LogicFlow::class.java)

    private fun getReceiver(): Flux<AnswerResult> {
        val connectionFactory = ConnectionFactory()
        connectionFactory.useNio()

        val receiverOptions = ReceiverOptions()
                .connectionFactory(connectionFactory)
                .connectionSubscriptionScheduler(Schedulers.elastic())

        val senderOptions = SenderOptions()
                .connectionFactory(connectionFactory)
                .connectionSupplier { cf ->
                    cf.newConnection(
                            arrayOf<Address>(Address("127.0.0.1")))
                }
                .resourceManagementScheduler(Schedulers.elastic())
        val objectMapper = ObjectMapper().registerKotlinModule()
        return RabbitFlux.createReceiver(receiverOptions)
                .consumeNoAck("telegram-message")
                .map { it.body }
                .map { String(it, Charset.forName("UTF-8")) }
                .map { objectMapper.readValue(it, AnswerResult::class.java) }
    }

    @PostConstruct
    fun handleAnswer() {
//        connector.doOnReceive(AnswerResult::class.java) { result ->
        getReceiver().subscribe { result ->
            val message = result.message
             message?.from.let { user ->
                if (user != null && !user.isBot) {
                    // смотрим ид, если у нас такого нет, то заводим
                    // смотрим на статус пользователя и понимаем, что он прислал
                    val userId = user.id
                    val user = userRepository
                            .findById(userId)
                            .switchIfEmpty(userRepository.save(User(
                                    id = userId,
                                    charId = message!!.chat.id,
                                    state = UserState.NEWER,
                                    updatedAt = LocalDateTime.now(),
                                    createdAt = LocalDateTime.now()
                            )))

                    user.subscribe { user ->
                        println(user)
                        when (user.state) {
                            UserState.NEWER -> {
                                    val userToSave = if (result.message.from.id % 2 == 0L) {
                                        user
                                                .copy(
                                                        state = UserState.QUESTION_MAKER,
                                                        updatedAt = LocalDateTime.now()
                                                )
                                    } else {
                                        user
                                                .copy(
                                                        state = UserState.QUESTION_WAITER,
                                                        updatedAt = LocalDateTime.now()
                                                )
                                    }
                                    userRepository
                                            .save(userToSave)
                                            .subscribe { _ ->
                                                telegramBotRepository.sendMessage(
                                                        if (result.message.from.id % 2 == 0L) {
                                                            MessageToChat(result.message.chat.id.toString(), welcomeQuestionMessage)
                                                        } else {
                                                            MessageToChat(result.message.chat.id.toString(), welcomAnswerMessage)
                                                        }
                                                ).subscribe {
                                                    logger.info("User {} got state {}", user.id, userToSave.state)
                                                }
                                            }
                            }
                            UserState.ANSWER_MAKER -> {
                                val chatId = result.message.chat.id
                                questionRepository.findTopByAnswerChatId(chatId).flatMap { question ->
                                    userRepository.findById(question.authorChatId).map { author ->
                                        val answer = (result.message.text ?: "") + "<<<---- Ответ на ваш вопрос. Теперь ожидайте вопрос от другого человека."
                                        val messageToChat = MessageToChat(author.charId.toString(), answer)
                                        telegramBotRepository.sendMessage(messageToChat).subscribe {
                                            questionRepository.save(question.copy(
                                                    answer = answer,
                                                    state = QuestionState.FINISHED,
                                                    updatedAt = LocalDateTime.now())).subscribe {
                                                userRepository.save(author.copy(
                                                        state = UserState.QUESTION_WAITER,
                                                        updatedAt = LocalDateTime.now())).subscribe {
                                                    logger.info("User (author) {} became {}", author.id, UserState.QUESTION_WAITER)
                                                    userRepository.save(user.copy(
                                                                    state = UserState.QUESTION_MAKER,
                                                                    updatedAt = LocalDateTime.now())).subscribe {
                                                        logger.info("User (who answered) {} became {}", user.id, UserState.ANSWER_MAKER)
                                                        // Отправить сообщение тому кто ответил на вопрос
                                                        val messageToChat = MessageToChat(user.id.toString(), "Спасибо за ответ на вопрос! Теперь можете задать свой вопрос:")
                                                        telegramBotRepository.sendMessage(messageToChat).subscribe {
                                                            logger.info("It's alive!")
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }.subscribe {
                                    logger.info("answer was delivered!")
                                }
                            }
                            UserState.QUESTION_MAKER -> {
                                val question = result.convertToNewQuestion()
                                template.opsForList().leftPush("question", question).subscribe { _ ->
                                    questionRepository
                                            .save(question)
                                            .subscribe {
                                                logger.info("Question {} was saved", it.id)
                                            }
                                    userRepository
                                            .save(user.copy(state = UserState.ANSWER_WAITER, updatedAt = LocalDateTime.now()))
                                            .subscribe { _ ->
                                                logger.info("User {} made question and became {}", user.id, UserState.ANSWER_WAITER)
                                            }
                                }
                            }
                        }

                    }
                }
            }
        }
    }


}


fun main() {
}