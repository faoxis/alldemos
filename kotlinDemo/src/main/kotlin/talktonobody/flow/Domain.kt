package talktonobody.flow

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

enum class UserState {
    NEWER,
    QUESTION_WAITER,
    ANSWER_MAKER,
    QUESTION_MAKER,
    ANSWER_WAITER
}

@Document
data class User (
        @Id
        val id: Long,
        val charId: Long,
        val state: UserState,
        val updatedAt: LocalDateTime = LocalDateTime.now(),
        val createdAt: LocalDateTime = LocalDateTime.now()
)

enum class QuestionState {
    CREATED, // Только получили
    IN_PROGRESS, // Кому-то был задан
    FINISHED   // Завершен (получен и отправлен ответ)
}

@Document
data class Question (
        @Id
        val id: String?,
        val authorChatId: Long,
        val text: String,
        val answer: String?,
        val answerChatId: Long?,
        val state: QuestionState,
        val updatedAt: LocalDateTime,
        val createdAt: LocalDateTime
)

