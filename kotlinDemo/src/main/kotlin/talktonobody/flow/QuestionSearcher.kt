package talktonobody.flow

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.ClientResponse
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import talktonobody.telegram.MessageToChat
import talktonobody.telegram.TelegramBotRepository
import java.time.Duration
import javax.annotation.PostConstruct

@Component
class QuestionSearcher(val userRepository: UserRepository,
                       val questionRepository: QuestionRepository,
                       val telegramBotRepository: TelegramBotRepository) {
    private val logger = LoggerFactory.getLogger(QuestionSearcher::class.java)

    @PostConstruct
    fun findQuestionToWaiterUser() {
        userRepository
                .findAllByState(UserState.QUESTION_WAITER)
                .repeatWhen { Flux.interval(Duration.ofSeconds(2)) }
                .subscribe { user ->
                    questionRepository.findByState(QuestionState.CREATED).subscribe { question ->
                        val messageToChat = MessageToChat(user.charId.toString(), question.text + "\n<------ Вопрос. Ответить можно сообщением ниже:")
                        telegramBotRepository.sendMessage(messageToChat).onErrorResume { e -> e.printStackTrace(); Mono.empty() }.subscribe { _ ->
                            questionRepository.save(question.copy(
                                    answerChatId = user.charId
                            )).subscribe {
                                userRepository.save(user.copy(state = UserState.ANSWER_MAKER)).subscribe {
                                    logger.info("User {} got question {}", user.id, question.id)
                                }
                            }
                        }
                    }
                }
    }

}