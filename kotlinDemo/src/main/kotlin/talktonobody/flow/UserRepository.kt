package talktonobody.flow

import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Flux

interface UserRepository: ReactiveMongoRepository<User, Long> {
    fun findAllByState(state: UserState): Flux<User>
}