package talktonobody.flow

import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface QuestionRepository : ReactiveMongoRepository<Question, String> {
    fun findByState(state: QuestionState): Mono<Question>
    fun findTopByAnswerChatId(chatId: Long): Mono<Question>
}
