package talktonobody.flow

import talktonobody.telegram.AnswerResult
import java.time.LocalDateTime


fun AnswerResult.convertToNewQuestion(): Question {
    return Question(
            id = null,
            authorChatId = this.message!!.chat.id,
            text = this.message.text ?: "empty message",
            answer = null,
            answerChatId = null,
            state = QuestionState.CREATED,
            updatedAt = LocalDateTime.now(),
            createdAt = LocalDateTime.now()
    )
}