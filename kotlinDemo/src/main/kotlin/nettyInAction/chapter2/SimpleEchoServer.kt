package nettyInAction.chapter2

import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled
import io.netty.channel.ChannelFutureListener
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelInboundHandlerAdapter
import io.netty.util.CharsetUtil

class SimpleEchoServerHandler: ChannelInboundHandlerAdapter() {

    override fun channelRead(ctx: ChannelHandlerContext?, msg: Any?) {
        val input: ByteBuf = msg as ByteBuf
        println("Server received: ${input.toString(CharsetUtil.UTF_8)}")
        ctx?.write(input)
    }

    override fun channelReadComplete(ctx: ChannelHandlerContext?) {
        ctx?.writeAndFlush(Unpooled.EMPTY_BUFFER)
                ?.addListener(ChannelFutureListener.CLOSE)
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {
        cause.printStackTrace()
        ctx.close()
    }

}

class SimpleEchoServer (private val port: Int) {

    fun start() {
        val echoServerHandler = SimpleEchoServerHandler()


    }

}