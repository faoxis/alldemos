package kafka.streamCourse

import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.common.utils.Bytes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.kstream.*
import org.apache.kafka.streams.state.KeyValueStore
import org.slf4j.LoggerFactory
import java.util.*


fun main() {
    val logger = LoggerFactory.getLogger("asd")

    val props = Properties()
    props[StreamsConfig.APPLICATION_ID_CONFIG] = "first-stream-app"
    props[StreamsConfig.BOOTSTRAP_SERVERS_CONFIG] = "127.0.0.1:9092"
    props[StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG] = Serdes.String().javaClass
    props[StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG] = Serdes.String().javaClass

    val builder = StreamsBuilder()
    val textLines: KStream<String, String> = builder
                    .stream<String, String>("streams-demo", Consumed.with(Serdes.String(), Serdes.String()))

    fun getWords(line: String): List<String> =
            line.toLowerCase().split("\\W+".toRegex()).dropLastWhile { it.isEmpty() }
    val wordCounts: KTable<String, Long> = textLines
            .flatMapValues { textLine -> getWords(textLine) }
            .selectKey { _, value -> value }
            .groupByKey()
            .count(Materialized.`as`<String, Long, KeyValueStore<Bytes, ByteArray>>("counts-store"))
    wordCounts.toStream().to("streams-demo-out", Produced.with(Serdes.String(), Serdes.Long()))

    val streams = KafkaStreams(builder.build(), props)
    streams.start()

}
