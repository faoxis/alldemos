package kafka.beginerCourse.tutorial1

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import org.slf4j.LoggerFactory
import java.lang.Thread.sleep
import java.util.*


fun main() {

    val logger = LoggerFactory.getLogger("ProducerWithCallback")

    // create Producer properties
    val properties = Properties()
    properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java.name)
    properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java.name)

    // create the producer
    val producer: KafkaProducer<String, String> = KafkaProducer(properties)

    for (i in 0..10) {
        //create a producer record
        val record: ProducerRecord<String, String> = ProducerRecord("first_topic", "from Java! Message number $i")

        // send data
        producer.send(record) { meta, exception: Exception? ->
            exception?.printStackTrace() ?: run {
                logger.info("""
                Received new data.
                 Partition: ${meta.partition()}
                 Offset: ${meta.offset()}
                 Timestamp: ${meta.timestamp()}
            """.trimIndent())
            }
        }
    }


    producer.flush()
    producer.close()
    sleep(10000)
}
