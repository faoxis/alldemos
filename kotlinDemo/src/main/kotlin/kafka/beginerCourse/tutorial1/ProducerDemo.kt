package kafka.beginerCourse.tutorial1

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import java.util.*


fun main() {

    // create Producer properties
    val properties = Properties()
    properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java.name)
    properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java.name)

    // create the producer
    val producer: KafkaProducer<String, String> = KafkaProducer(properties)

    //create a producer record
    val record: ProducerRecord<String, String> = ProducerRecord("first_topic", "hello from Java!")

    // send data
    producer.send(record)


    producer.flush()
    producer.close()
}
