package kafka.beginerCourse.tutorial1

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.serialization.StringDeserializer
import org.slf4j.LoggerFactory
import java.time.Duration
import java.util.*

fun main() {
    val logger = LoggerFactory.getLogger("ConsumerDemoGroups")

    val bootstrapServer = "127.0.0.1:9092"
    val groupId = "my-fourth-application"
    val topic = "first_topic"

    // create consumer config
    val properties = Properties()
    properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer)
    properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer::class.java.name)
    properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer::class.java.name)
    properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId)
    properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")

    // create consumer
    val consumer: KafkaConsumer<String, String> = KafkaConsumer(properties)

    // subscribe consumer to our topic
    consumer.subscribe(Collections.singleton(topic))

    // pol for new data
    while (true) {
        val records: ConsumerRecords<String, String> =
                consumer.poll(Duration.ofMillis(100))

        for (record in records) {
            logger.info("Key: {}  Value: {}", record.key(), record.value())
            logger.info("Partition: {}   Offset: {}", record.partition(), record.offset())
        }
    }

}

