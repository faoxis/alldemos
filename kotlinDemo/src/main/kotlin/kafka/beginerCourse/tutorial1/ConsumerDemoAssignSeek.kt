package kafka.beginerCourse.tutorial1

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.common.serialization.StringDeserializer
import org.slf4j.LoggerFactory
import java.time.Duration
import java.util.*

fun main() {
    val logger = LoggerFactory.getLogger("ConsumerDemo")

    val bootstrapServer = "127.0.0.1:9092"
    val topic = "first_topic"

    // create consumer config
    val properties = Properties()
    properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer)
    properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer::class.java.name)
    properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer::class.java.name)
    properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")

    // create consumer
    val consumer: KafkaConsumer<String, String> = KafkaConsumer(properties)

    // assign and seek are mostly used to replay data or fetch a specific message

    // assign
    val partitionToReadFrom = TopicPartition(topic, 0)
    consumer.assign(Arrays.asList(partitionToReadFrom))

    // seek
    val offsetToReadFrom = 15L
    consumer.seek(partitionToReadFrom, offsetToReadFrom)


    val numberMessageToRead = 5
    var keepOnReading = true
    var numberOfMessagesReadSoFar = 0

    // pol for new data
    while (true) {
        val records: ConsumerRecords<String, String> =
                consumer.poll(Duration.ofMillis(100))

        for (record in records) {
            numberOfMessagesReadSoFar += 1
            logger.info("Key: {}  Value: {}", record.key(), record.value())
            logger.info("Partition: {}   Offset: {}", record.partition(), record.offset())
            if (numberMessageToRead >= numberOfMessagesReadSoFar) {
                keepOnReading = false // to exit the while loop
                break
            }
        }
    }

}

