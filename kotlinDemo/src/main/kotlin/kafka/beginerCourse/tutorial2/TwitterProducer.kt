package kafka.beginerCourse.tutorial2

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.google.common.collect.Lists
import com.twitter.hbc.ClientBuilder
import com.twitter.hbc.core.Client
import com.twitter.hbc.core.Constants
import java.util.concurrent.LinkedBlockingQueue
import com.twitter.hbc.httpclient.auth.OAuth1
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint
import com.twitter.hbc.core.HttpHosts
import com.twitter.hbc.core.processor.StringDelimitedProcessor
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread


data class TwitterAuth(
        val consumerKey: String,
        val consumerSecret: String,
        val token: String,
        val secret: String
)

data class Tweet (
        val text: String
)

class TwitterProducer(private val twitterAuth: TwitterAuth) {
    private val objectMapper = ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .registerKotlinModule()
    private val logger = LoggerFactory.getLogger(TwitterProducer::class.java)

    fun run() {

        // create a twitter client
        val msgQueue = LinkedBlockingQueue<String>(100000)
        val client = createTwitterClient(msgQueue)
        client.connect()

        // create the Producer
        val producer = createKafkaProducer()

        // loop to send tweets to kafka
        repeat(5) {
            thread {
                while (!client.isDone) {
                    try {
                        val msg = msgQueue.poll(5, TimeUnit.SECONDS)
                        producer.send(ProducerRecord("twitter_tweets", msg))
                    } catch (e: InterruptedException) {
                        logger.error("Cannot poll twitter message: {}", e)
                    }
                }
            }
        }

    }

    private fun createKafkaProducer(): KafkaProducer<String, String> {
        val properties = Properties()
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:8092")
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java.name)
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java.name)

        // create safe Producer
        properties.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true")
        properties.setProperty(ProducerConfig.ACKS_CONFIG, "all")
        properties.setProperty(ProducerConfig.RETRIES_CONFIG, Int.MAX_VALUE.toString())
        properties.setProperty(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5")

        properties.setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy")
        properties.setProperty(ProducerConfig.LINGER_MS_CONFIG, "20")
        properties.setProperty(ProducerConfig.BATCH_SIZE_CONFIG, (32 * 1024).toString())


        // create the producer
        return KafkaProducer(properties)
    }

    private fun createTwitterClient(msgQueue: BlockingQueue<String>): Client {


        /** Declare the host you want to connect to, the endpoint, and authentication (basic auth or oauth) */
        val hosebirdHosts = HttpHosts(Constants.STREAM_HOST)
        val hosebirdEndpoint = StatusesFilterEndpoint()

        val terms = Lists.newArrayList("bitcoin")
        hosebirdEndpoint.trackTerms(terms)

        // These secrets should be read from a config file
        val hosebirdAuth = OAuth1(
                twitterAuth.consumerKey,
                twitterAuth.consumerSecret,
                twitterAuth.token,
                twitterAuth.secret)

        val builder = ClientBuilder()
                .name("Hosebird-Client-01")                              // optional: mainly for the logs
                .hosts(hosebirdHosts)
                .authentication(hosebirdAuth)
                .endpoint(hosebirdEndpoint)
                .processor(StringDelimitedProcessor(msgQueue))

        return builder.build()
        // Attempts to establish a connection.
//        hosebirdClient.connect()
    }
}

fun main() {
    TwitterProducer(
            TwitterAuth(
                    consumerKey = "sXomR4KX0W9Sm1lXuxbaGv89Q",
                    consumerSecret = "EIy5sJGmMdGl32s97mVlA4S7h3zpNrrpa8hpozVeLbODDHuROI",
                    token = "1166668023505313793-Vpp4kOBUTxvm1vWGofW38POwn5ggLy",
                    secret = "1RlBedqcfOBlYkchXeDAsazPGgWXjTnlIvFGApRAI6zTb"
            )
    ).run()
}