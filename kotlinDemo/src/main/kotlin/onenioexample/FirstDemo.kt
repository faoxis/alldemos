package onenioexample

import one.nio.http.HttpClient;
import one.nio.http.HttpException;
import one.nio.http.Response;
import one.nio.net.ConnectionString;
import one.nio.pool.PoolException;



fun main() {
    val client = HttpClient(ConnectionString("http://yamakarov.ru"))
//    val response = client.get("/about/")
    val response = client.get("/about/")

    println(response.bodyUtf8)

}
