package basic

// in, out
class StupidClass<in A, out B> {

    // norm
    fun doWithArg(a: A): B? = null

    // compile error
//    fun doWithArg2(b: B): A? = null
}




fun main() {

}
