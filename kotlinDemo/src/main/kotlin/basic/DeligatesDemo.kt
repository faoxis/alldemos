package basic

import java.lang.Thread.sleep
import kotlin.properties.Delegates
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

interface Repository {
    fun getById(id: Long): String
}

interface Logger {
    fun log(message: String)
}

class Controller(repository: Repository, logger: Logger): Repository by repository, Logger by logger {

    var delegateProperty: String by ClassToDelegate

    fun index(): String {
        val res = getById(1)
        log(res)
        return res
    }

}

object ClassToDelegate : ReadWriteProperty<Controller, String> {
    override fun getValue(thisRef: Controller, property: KProperty<*>): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setValue(thisRef: Controller, property: KProperty<*>, value: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

fun main() {
    val lazayVal: String by lazy {
        sleep(50)
        "Then ${System.currentTimeMillis()}"
    }
    println("Now ${System.currentTimeMillis()}. $lazayVal")
    println("Now ${System.currentTimeMillis()}. $lazayVal")

    var observableValue: String by Delegates.observable("init value") { prop, old, new ->
        println("I see! $old -> $new")
    }
    println(observableValue)
    observableValue = "new value"

}
