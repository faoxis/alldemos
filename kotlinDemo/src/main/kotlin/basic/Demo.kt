package basic

fun fiveWithOperationTwo(operation: Int.(Int) -> Int): Int = 5.operation(2)


val threadLocal = ThreadLocal<Map<String, Int>>()

class Bar {
    fun printFromBar() {
        println("from bar")
    }
}

class Foo {
    fun printSomething() {
        println("Something")
    }

    fun superThing(barF: Bar.() -> Unit): Bar {
        val bar = Bar()
        bar.barF()
        return bar
    }
}

fun doInFooContext(f: Foo.() -> Unit): Foo {
    val foo = Foo()
    foo.f()
    return foo
}

fun main() {

    doInFooContext {
        printSomething()
        superThing {
            printFromBar()
        }
    }

//    threadLocal.set(mapOf("foo" to 1))
//    threadLocal.set(threadLocal.get().plus("bar" to 2))
//    thread {
//        threadLocal.get().plus("baz" to 3)
//    }
//    sleep(1000)
//
//    println(threadLocal.get())
}

