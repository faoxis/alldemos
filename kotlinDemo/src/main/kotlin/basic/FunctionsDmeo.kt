package basic

// random number of parameters
fun varArgsDemo(vararg names: String) {
    names.forEach(::println)
}


fun main() {
    varArgsDemo("Hello", "World")
}
