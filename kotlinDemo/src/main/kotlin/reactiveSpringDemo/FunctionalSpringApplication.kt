package reactiveSpringDemo

import org.springframework.boot.ApplicationRunner
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.support.beans
import org.springframework.data.mongodb.core.MongoTemplate

class FunctionalSpringApplication

class Bar
class Foo(val bar: Bar)

fun main(args: Array<String>) {
    SpringApplicationBuilder()
            .sources(FunctionalSpringApplication::class.java)
            .initializers(beans {
                bean { Bar() }
                bean { Foo(ref()) }
                bean { ApplicationRunner {
                    println("Hello, World!")
                }}
                bean { ApplicationRunner {
                    println("Hey hey hey")
                }}
            })
            .run(*args)

//    template.save(BasicDBObjectBuilder.start().add("one", 1).get())
//    template.findAll(DBObject::class.java).forEach { println(it) }
}