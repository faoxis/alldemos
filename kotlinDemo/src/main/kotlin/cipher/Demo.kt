package cipher

import java.io.*
import javax.crypto.*
import javax.crypto.spec.IvParameterSpec
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.SecretKeyFactory







class MyCipher(transformation: String) {

    private fun getSecretKey(): SecretKey {
        val stringBytes = "90 -125 13 113 -29 15 -78 45 30 -31 -24 -73 62 -5 -81 50"
        val secretBytes = stringBytes.split(" ").map { it.toByte() }.toByteArray()
        return SecretKeySpec(secretBytes, "AES")
    }


    private val secretKey = getSecretKey()
    private val cipher = Cipher.getInstance(transformation)

    fun encrypt(content: String): ByteArray {
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        val iv: ByteArray = cipher.iv

        val byteArray =  ByteArrayOutputStream()
        byteArray.use { outputStream ->
            CipherOutputStream(outputStream, cipher).use { cipherStream ->
                byteArray.write(iv)
                cipherStream.write(content.toByteArray())
            }
        }
        return byteArray.toByteArray()
    }

    fun decrypt(bytes: ByteArray): String =

        ByteArrayInputStream(bytes).use { fileIn ->
            val fileIv = ByteArray(16)
            fileIn.read(fileIv)
            cipher.init(Cipher.DECRYPT_MODE, secretKey, IvParameterSpec(fileIv))

            CipherInputStream(fileIn, cipher).use { cipherIn ->
                InputStreamReader(cipherIn).use { inputReader ->
                    BufferedReader(inputReader).readText()
                }
            }

        }
}

class Aes256Class {
    private var secretKey: SecretKey? = null

    init {
        try {
            val keyGenerator = KeyGenerator.getInstance("AES")
            keyGenerator.init(256)

            val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
            val spec = PBEKeySpec("YouWillNeverGuess".toCharArray(), "somesalt".toByteArray(), 65536, 256)
            this.secretKey = factory.generateSecret(spec)
//            val secret = SecretKeySpec(secretKey.getEncoded(), "AES")
//            this.secretKey = keyGenerator.generateKey()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

    }

    fun encrypt(message: ByteArray): ByteArray {
        return makeAes(message, Cipher.ENCRYPT_MODE)
    }

    fun decrypt(message: ByteArray): ByteArray {
        return makeAes(message, Cipher.DECRYPT_MODE)
    }

    private fun makeAes(rawMessage: ByteArray, cipherMode: Int): ByteArray {
        val cipher = Cipher.getInstance("AES")
        cipher.init(cipherMode, this.secretKey)
        return cipher.doFinal(rawMessage)
    }

}

fun main(args: Array<String>) {
    val content = "Amazing content"
//    val secret: SecretKey = KeyGenerator.getInstance("AES").generateKey()
//    println(secret.encoded.joinToString(" "))

    val cipher = MyCipher("AES/CBC/PKCS5Padding")
//    val encrypted: ByteArray = cipher.encrypt(content)



//    val aes = Aes256Class()
//
//    val encrypted = cipher.encrypt(content)
//    FileOutputStream("cipherdemo.txt").write(encrypted)
    FileInputStream("cipherdemo.txt").use { stream ->
        val decrypted = cipher.decrypt(stream.readBytes())
        println(decrypted)
    }

}