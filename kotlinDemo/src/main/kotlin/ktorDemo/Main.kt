package ktorDemo

import io.ktor.application.*
import io.ktor.features.ContentNegotiation
import io.ktor.http.*
import io.ktor.jackson.jackson
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import java.io.FileInputStream

fun main(args: Array<String>) {
    embeddedServer(Netty,port=8099) {


    }.apply { start(wait = true)}
}

fun Application.her() {


    install(ContentNegotiation) {
        jackson {
        }
    }
    routing {
        get("/her") {
            call.respondText("her sobachij")
        }
        get("/") {
            call.respondText("Hello, World!", contentType = ContentType.Text.Plain)
        }
        get("/json") {
            call.respond(mapOf("message" to "Hello, World"))
        }
    }
}
