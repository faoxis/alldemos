package functionalKotlin

import arrow.effects.IO

fun main(args: Array<String>) {

    fun putStrLn(line: String): IO<Unit> = IO { println(line) }
    fun getStrLn(): IO<String?> = IO { readLine() }

    getStrLn()
            .map { it?.toInt() }
            .map { n -> n?.let { (n*n).toString() }}
//            .flatMap { n -> n?.let { putStrLn(it) }}


}
