package functionalKotlin.safariCourse

import arrow.core.*
import arrow.data.Ior
import arrow.effects.IO
import java.lang.RuntimeException

fun main(args: Array<String>) {

    // Id
    Id.just(1)
            .map { it * 2 }
            .flatMap { Id.just( it * 2) }
            .value()

    // Option
    Option.just(1 )
    Option.empty<String >()

    // Either
    Either.left("foo")
    Either.right("bar")

    // Ior represents left value, right value or both
    Ior.bothNel(12, 13)

    // Try
    Try { 1.0 / 0.0 }.toEither()

    // Eval - lazy calculation, will be done after value calling
    Eval.later { "String" + 42 }.memoize() // memoize is for remembering

    // IO
    IO.just(42)
    IO.raiseError<Int>(RuntimeException())

    class Database {
        fun readUserName() = "John"
        fun readAnotherUserName() = "Howard"
    }
    val database = Database()

    IO { database.readUserName() }

//    IO.async<Int> { callback ->
//        callback(1.right())
//    }


//    IO.async { result: (Either<Throwable, String>) -> Unit ->
//        try {
//            result(database.readUserName().right())
//        } catch (e: Throwable) {
//            result(e.left())
//        }
//    }


 }


