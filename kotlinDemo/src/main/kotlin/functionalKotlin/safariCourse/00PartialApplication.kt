package functionalKotlin.safariCourse

fun price(cost: Double, pDiscount: Double, fDiscount: Double): Double =
        cost * (1 - pDiscount) - fDiscount

// method extension for object like price function
fun <T1, T2, T3, R> ((T1, T2, T3) -> R).partial1(t1: T1) : (T2, T3) -> R =
        { t2, t3 -> this(t1, t2, t3) }



fun main(args: Array<String>) {
    val p100 = ::price.partial1(100.0) // ::price - function reference
    println(p100(0.5, 10.0)) // 40 as expected

    // There is something like our partial1 in arrow library
}
