package functionalKotlin.safariCourse

import kotlinx.coroutines.delay
import kotlinx.coroutines.*


fun main(args: Array<String>) {
    val hello = GlobalScope.async {
        delay(1000)
        "Hello, "
    }

    val world = GlobalScope.async {
        delay(1000)
        "World!"
    }



}

suspend fun doWorld() {
    delay(1000L)
    println("World!")
}

