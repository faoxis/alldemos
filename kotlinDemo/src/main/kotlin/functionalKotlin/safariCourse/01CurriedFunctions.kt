package functionalKotlin.safariCourse

// extension for carring functions
fun <T1, T2, R> ((T1, T2) -> R).curried(): (T1) -> (T2) -> R =
     { t1 -> { t2 -> this(t1, t2)} }


fun main(args: Array<String>) {
    val sum2: (Int, Int) -> Int = { a, b -> a + b }
    val carriedSum2 = sum2.curried()
    println(carriedSum2(1)(2))

    val sumWith2 = carriedSum2(2)
    println(sumWith2(2))
}

