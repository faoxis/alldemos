package algorithms.RodStivens.structures

import org.junit.Assert.*
import org.junit.Test
import java.util.*
import kotlin.random.Random

class BinaryHeapKtTest {
    @Test
    fun makeBinaryHeapTest() {
        val arrayToTest = arrayOf(1, 2, 3,  4)
        makeBinaryHeap(arrayToTest)

        assertEquals(4, arrayToTest[0])
        assertEquals(1, arrayToTest[arrayToTest.size - 1])
        arrayToTest.forEach(::println)
    }

    @Test
    fun getTopHeapTest() {
        val arrayToTest = arrayOf(4, 3, 2, 1)

        for (i in 4 downTo 1) {
            assertEquals(i, getTopElement(arrayToTest, i))
        }
    }

    @Test
    fun heapSortTest() {
        val arrayToTest = Array(10_000) {
            Random.nextInt(0, 10_000)
        }

        val sortedArray = arrayToTest.copyOf(arrayToTest.size)
        Arrays.sort(sortedArray)

        assertNotEquals(sortedArray, arrayToTest)

        heapSort(arrayToTest)
        assertArrayEquals(sortedArray, arrayToTest)
    }
}
