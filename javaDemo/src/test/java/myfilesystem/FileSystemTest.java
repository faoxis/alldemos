package myfilesystem;

import org.junit.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;

import static org.junit.Assert.*;

public class FileSystemTest {

    public final String pathToTest = "src/test/java/myfilesystem";

    @Test
    public void testGetListOfFile() {
        FileSystem fileSystem =
                new FileSystem(pathToTest);
        assertTrue(fileSystem.getListOfFiles().contains(getClass().getSimpleName() + ".java"));
    }

//    @Test
    public void testDelete() throws InterruptedException {
        FileSystem fileSystem =
                new FileSystem(pathToTest);
        fileSystem.delete("test.txt");
//        String fileName = "test.txt";
//        System.out.println(Paths.get(pathToTest, fileName));
//        System.out.println(Files.exists(Paths.get(pathToTest, fileName)));
        Thread.sleep(100);
    }

}