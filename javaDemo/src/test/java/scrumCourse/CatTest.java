package scrumCourse;

import org.junit.Test;

import static org.junit.Assert.*;

public class CatTest {


    @Test
    public void play() {
        Clew clew = new Clew(10);

        Cat boris = new Cat("Boris", 1);
        assertEquals(10, boris.play(clew));

        Cat semen = new Cat("Semen", 5);
        assertEquals(2, semen.play(clew));
    }
}