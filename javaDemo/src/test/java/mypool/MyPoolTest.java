package mypool;

import org.junit.Test;

public class MyPoolTest {


    @Test
    public void testRunningTaskCompletion() throws InterruptedException {
        int size = 10;

        MyPool pool = new MyPool(size);
        pool.execute(() -> System.out.println("heeeey!"));
        pool.execute(() -> System.out.println("heeeey!"));
        pool.execute(() -> System.out.println("heeeey!"));
        pool.execute(() -> System.out.println("heeeey!"));

        pool.execute(() -> {throw new RuntimeException();});
        System.out.println(pool.getCurrentPoolSize());
        Thread.sleep(1_000);
        System.out.println(pool.getCurrentPoolSize());

        pool.shutdown();

        pool.execute(() -> System.out.println("12412124"));
        pool.execute(() -> System.out.println("12412124"));
        pool.execute(() -> System.out.println("12412124"));

    }

}