package algorithms;

import org.junit.Test;

import static org.junit.Assert.*;

public class BinarySearcherTest {

    @Test
    public void testFindIndexHappyPass() {

        int[] mass = new int[] { 5, 6, 8, 9, 10, 23, 42, 68, 98 };
        assertEquals(6, BinarySearcher.findIndex(mass, 42));
        assertEquals(0, BinarySearcher.findIndex(mass, 5));
        assertEquals(mass.length - 1, BinarySearcher.findIndex(mass, 98));
    }

    @Test(expected = IllegalArgumentException.class)
    public void tesetFindIndexWithNotExistSmallValue() {
        int[] mass = new int[] { 5, 6, 8, 9, 10, 23, 42, 68, 98 };
        BinarySearcher.findIndex(mass, 4);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tesetFindIndexWithNotExistLargeValue() {
        int[] mass = new int[] { 5, 6, 8, 9, 10, 23, 42, 68, 98 };
        BinarySearcher.findIndex(mass, 99);
    }

}