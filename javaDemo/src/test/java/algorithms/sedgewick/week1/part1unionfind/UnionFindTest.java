package algorithms.sedgewick.week1.part1unionfind;

import org.junit.Test;

import static org.junit.Assert.*;

public class UnionFindTest {

    private void testWork(UnionFind unionFind) {
        assertEquals(10, unionFind.count());
        assertFalse(unionFind.connected(0, 3));
        assertFalse(unionFind.connected(3, 5));

        unionFind.union(0, 3);
        assertTrue(unionFind.connected(0, 3));
        assertEquals(9, unionFind.count());

        unionFind.union(3, 5);
        assertEquals(8, unionFind.count());
        assertTrue(unionFind.connected(0, 5));

        unionFind.union(3, 9);
        assertTrue(unionFind.connected(0, 9));
        assertEquals(7, unionFind.count());

    }

    @Test
    public void testWorkNaiveImplTest() {
        testWork(new UnionFindNaiveImpl(10));
    }

    @Test
    public void testWorkQuickUnionImplTest() {
        testWork(new UnionFindQuickUnionImpl(10));
    }

    @Test
    public void testWeightedQuickUnionImplTest() {
        testWork(new UnionFindWeightedQuickUnionImpl(10));
    }



    private void testTimeOfWorking(UnionFind unionFind) {
        for (int i = 2; i < 10_000; i++) {
            unionFind.union(1, i);
        }

        unionFind.union(0, 9_999);
    }

    @Test(timeout = 4_000)
    public void testTimeOfWorkingNaive() {
        testTimeOfWorking(new UnionFindNaiveImpl(1_000_000));
    }

    @Test(timeout = 150)
    public void testTimeOfWorkingQuickUnion() {
        testTimeOfWorking(new UnionFindQuickUnionImpl(1_000_000));
    }

    @Test(timeout = 20)
    public void testTimeOfWorkingWeightedQuickUnion() {
        testTimeOfWorking(new UnionFindWeightedQuickUnionImpl(1_000_000));
    }

}