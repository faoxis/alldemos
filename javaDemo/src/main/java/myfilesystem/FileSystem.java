package myfilesystem;

import myfilesystem.domain.ActionFile;
import myfilesystem.domain.FileState;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

public class FileSystem {

    private ExecutorService pool = ForkJoinPool.commonPool();
    private String root;
    private Map<String, ActionFile> files =
            new ConcurrentHashMap<>();

    public FileSystem(String root) {
        this.root = root;
        for (File file : Objects.requireNonNull(new File(root).listFiles())) {
            if (!file.isDirectory()) {
                files.put(file.getName(), new ActionFile());
            }
        }
    }

    private boolean prepareForDelete(String filename) {
        ActionFile actionFile = files.get(filename);
        FileState fileState = actionFile.getCurrentFileState();

        boolean result = false;
        switch (fileState) {
            case IDLE: {
                if (actionFile.tryToSet(fileState, FileState.DELETING)) {
                    result = true;
                } else {
                    prepareForDelete(filename);
                }
                break;
            }
            default: {
                break;
            }
        }

        return result;
    }

    public void delete(String fileName) {
        if (files.get(fileName) == null) throw new IllegalArgumentException();
        pool.execute(() -> {
            if (prepareForDelete(fileName)) {
                try {
                    Files.delete(Paths.get(root, fileName));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                files.remove(fileName);
            }
        });
    }

    private boolean acquireForCopy(String filename) {
        ActionFile actionFile = files.get(filename);
        FileState fileState = actionFile.getCurrentFileState();

        boolean result = false;
        switch (fileState) {
            case IDLE: {
                if (actionFile.tryToSet(fileState, FileState.COPYING)) result = true;
                break;
            }
            default: {
                break;
            }

        }
        return result;
    }
    
    public void copy(String filename) {
        if (files.get(filename) == null) throw new IllegalArgumentException();
        pool.execute(() -> {
            if (acquireForCopy(filename)) {

            }
        });
    }



    public void create(String fileName) {
        if (files.get(fileName) != null) {
            throw new IllegalArgumentException();
        }

        pool.execute(() -> {
            synchronized (files) {
                files.put(fileName, new ActionFile(FileState.CREATING));
            }

            File file = new File(fileName);
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                files.put(fileName, new ActionFile());
            }


        });
    }


    public Set<String> getListOfFiles() {
        return files.keySet();
    }
}
