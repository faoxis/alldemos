package myfilesystem.exception;

public class AllSituationException extends RuntimeException {

    public AllSituationException(String msg) {
        super(msg);
    }

}
