package myfilesystem.domain;

public enum FileState {
    IDLE,
    COPYING,
    DELETING,
    CREATING
}
