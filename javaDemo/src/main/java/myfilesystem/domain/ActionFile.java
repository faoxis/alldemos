package myfilesystem.domain;

import java.util.concurrent.atomic.AtomicReference;

public class ActionFile {
    private final AtomicReference<FileState> state;

    public ActionFile() {
        state = new AtomicReference<>(FileState.IDLE);
    }

    public ActionFile(FileState fileState) {
        state = new AtomicReference<>(fileState);
    }

    public FileState getCurrentFileState() {
        return state.get();
    }

    public boolean tryToSet(FileState expectedState, FileState newState) {
        return state.compareAndSet(expectedState, newState);
    }

}
