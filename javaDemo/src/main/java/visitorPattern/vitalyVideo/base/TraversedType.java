package visitorPattern.vitalyVideo.base;

public interface TraversedType {
    void accept(Service service);
}
