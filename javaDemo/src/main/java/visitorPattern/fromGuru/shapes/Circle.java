package visitorPattern.fromGuru.shapes;

import visitorPattern.fromGuru.visitor.Visitor;

public class Circle extends Dot {
    private int radius;

    public Circle(int x, int y, int z, int radius) {
        super(x, y, z);
        this.radius = radius;
    }

    @Override
    public String accept(Visitor visitor) {
//        return visitor.visitCircle(this);
        return "";
    }

    public int getRadius() {
        return radius;
    }
}
