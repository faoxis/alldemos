package visitorPattern.fromGuru.shapes;

import visitorPattern.fromGuru.visitor.Visitor;

public interface Shape {
    void move(int x, int y);
    void draw();
    String accept(Visitor visitor);
}

