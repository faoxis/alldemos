package visitorPattern.fromGuru.shapes;

import visitorPattern.fromGuru.visitor.Visitor;

public class Dot implements Shape {

    private int x;
    private int y;
    private int z;

    public Dot() {}

    public Dot(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void move(int x, int y) {

    }

    @Override
    public void draw() {

    }

    @Override
    public String accept(Visitor visitor) {
        return null;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }
}
