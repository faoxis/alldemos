package visitorPattern.fromGuru.visitor;

import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import visitorPattern.fromGuru.shapes.CompoundShape;
import visitorPattern.fromGuru.shapes.Dot;

public interface Visitor {
    String visitDot(Dot dot);
    String visitCircle(Circle circle);
    String visitRectangle(Rectangle rectangle);
    String visitCompoundGraphic(CompoundShape cg);
}
