public class FirstDemo {

    static {
        System.loadLibrary("hello");
    }

    native void print();

    public static void main(String[] args) {
        FirstDemo demo = new FirstDemo();
        demo.print();
    }

}
