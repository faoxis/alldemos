package complatableFutureDemo;

import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

public class FunctionalCompletableFuture {
    public static void main(String[] args) throws InterruptedException {
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(5_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "heey";
        });
        IntStream.range(0, 10).forEach(x -> future.handleAsync((s, e) -> s + "*"));
        future.thenAccept(System.out::println);




        Thread.sleep(10_000);
    }
}
