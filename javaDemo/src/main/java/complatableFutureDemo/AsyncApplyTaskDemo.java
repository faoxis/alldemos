package complatableFutureDemo;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class AsyncApplyTaskDemo {

    public static Future<String> getCalculatedHelloWorld() {
        CompletableFuture<String> completableFuture =
                CompletableFuture.completedFuture("H");

        return completableFuture
                .thenApply(s -> s + "e")
                .thenApply(s -> s + "l")
                .thenApply(s -> s + "l")
                .thenApply(s -> s + "o")
                .thenApply(s -> s + ",")
                .thenApply(s -> s + " ")
                .thenApply(s -> s + "W")
                .thenApply(s -> s + "o")
                .thenApply(s -> s + "r")
                .thenApply(s -> s + "l")
                .thenApply(s -> s + "d")
                .thenApply(s -> s + "!");
    }

}
