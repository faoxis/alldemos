package complatableFutureDemo;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class WithPoolsDemo {

    public static Future<String> calculateAsync() throws InterruptedException {

        CompletableFuture<String> completableFuture
                = new CompletableFuture<>();

        Executors.newCachedThreadPool().submit(() -> {
            try {
                Thread.sleep(500);
                completableFuture.complete("Hello");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        return completableFuture;
    }


}
