package sheduledTasks;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.*;

public class TimerCaller {

    public static final int THREAD_POOL_SIZE = Runtime.getRuntime().availableProcessors() * 2;

    private static final ScheduledExecutorService pool = Executors
            .newScheduledThreadPool(THREAD_POOL_SIZE);

    public static <T> Future<T> doIt(LocalDateTime time, Callable<T> callable) {
        long seconds = LocalDateTime.now().until(time, ChronoUnit.SECONDS);
        return pool.schedule(callable, seconds, TimeUnit.SECONDS);
    }
}
