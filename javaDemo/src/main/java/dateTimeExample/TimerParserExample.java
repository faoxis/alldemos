package dateTimeExample;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimerParserExample {
    static String s = "2018-02-18 10:52:12.125691";
    public static void main(String[] args) {
        LocalDateTime time = LocalDateTime
                .parse(s, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS"));
        System.out.println(time);
    }
}

