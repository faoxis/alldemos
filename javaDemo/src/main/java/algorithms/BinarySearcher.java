package algorithms;

public class BinarySearcher {


    public static int findIndex(int[] mass, int value) {
        int fromIndex = 0;
        int toIndex = mass.length + 1;
        int middle;

        while (true) {
            middle = (fromIndex + toIndex) / 2;

            if (mass[middle] == value) return middle;

            if (value > mass[middle]) fromIndex = middle;
            else toIndex = middle;

            if (middle == 0 || middle == mass.length - 1) {
                throw new IllegalArgumentException();
            }
        }

    }

}

