package algorithms.sedgewick.week1.part1unionfind;

public class UnionFindNaiveImpl implements UnionFind {

    private int[] elements;

    /**
     * initialize union-find data structure with N object (0 to N - 1)
     */
    public UnionFindNaiveImpl(int n) {
        elements = new int[n];
        for (int i = 0; i < n; i++) elements[i] = i;

    }

    @Override
    public void union(int p, int q) {
        int groupOfP = find(p);
        int groupOfQ = find(q);

        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == groupOfQ) {
                elements[i] = groupOfP;
            }
        }

    }

    @Override
    public boolean connected(int p, int q) {
        return elements[p] == elements[q];

    }

    @Override
    public int find(int p) {
        return elements[p];
    }

    @Override
    public int count() {
        int count = 0;
        for (int i = 0; i < elements.length; i++) {
            if (i == elements[i]) count++;
        }

        return count;
    }


}
