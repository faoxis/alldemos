package algorithms.sedgewick.week1.part1unionfind;

public interface UnionFind {

    /**
    add connection between p and q
     */
    void union(int p, int q);

    /**
     * are p and q in the same component?
     */
    boolean connected(int p, int q);

    /**
     * Component identifier
     */
    int find(int p);

    /**
     * number of components
     */
    int count();

}
