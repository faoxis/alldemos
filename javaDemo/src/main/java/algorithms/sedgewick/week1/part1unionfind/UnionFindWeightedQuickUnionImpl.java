package algorithms.sedgewick.week1.part1unionfind;

public class UnionFindWeightedQuickUnionImpl implements UnionFind {

    private int[] elements;
    private int[] groupSizes;
    private int count;

    public UnionFindWeightedQuickUnionImpl(int n) {
        elements = new int[n];
        groupSizes = new int[n];
        for (int i = 0; i < elements.length; i++) {
            elements[i] = i;
            groupSizes[i] = 1;
        }
        count = n;
    }

    @Override
    public void union(int p, int q) {
        int groupOfP = find(p);
        int groupOfQ = find(q);
        if (groupOfP == groupOfQ) return;

        if (groupSizes[groupOfP] < groupSizes[groupOfQ]) {
            elements[groupOfP] = elements[groupOfQ];
            groupSizes[groupOfQ] += groupSizes[groupOfP];
        } else {
            elements[groupOfQ] = elements[groupOfP];
            groupSizes[groupOfP] += groupSizes[groupOfQ];
        }
        count--;
    }

    @Override
    public boolean connected(int p, int q) {
        return find(p) == find(q);
    }

    @Override
    public int find(int p) {
        int root = p;
        while (root != elements[root]) {
            elements[root] = elements[elements[root]];
            root = elements[root];
        }

        return root;
    }

    @Override
    public int count() {
        return count;
    }
}
