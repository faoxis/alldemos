package algorithms.sedgewick.week1.part1unionfind;

public class UnionFindQuickUnionImpl implements UnionFind {

    private int[] elements;

    public UnionFindQuickUnionImpl(int n) {
        elements = new int[n];
        for (int i = 0; i < elements.length; i++)
            elements[i] = i;
    }

    @Override
    public void union(int p, int q) {
        int rootOfP = getRoot(p);
        int rootOfQ = getRoot(q);
        elements[rootOfP] = rootOfQ;
    }

    private int getRoot(int e) {
        int root = e;
        while (root != elements[root]) {
            root = elements[root];
        }

        return root;
    }

    @Override
    public boolean connected(int p, int q) {
        return getRoot(p) == getRoot(q);
    }

    @Override
    public int find(int p) {
        return getRoot(p);
    }

    @Override
    public int count() {
        int count = 0;
        for (int i = 0; i < elements.length; i++) {
            if (i == elements[i]) count++;
        }

        return count;
    }
}
