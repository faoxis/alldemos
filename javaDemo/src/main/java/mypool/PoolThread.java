package mypool;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;

public class PoolThread extends Thread {

    private BlockingQueue<Callable> queue;

    public PoolThread(BlockingQueue<Callable> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {

        try {
            while (true) {
                Callable callable = queue.take();
                Object result = callable.call();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
