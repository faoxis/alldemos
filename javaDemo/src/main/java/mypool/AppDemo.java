package mypool;

import java.util.concurrent.*;

public class AppDemo {
    public static void main(String[] args) throws InterruptedException {
        ThreadPoolExecutor pool = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        pool.execute(() -> System.out.println("Heeey!"));
        pool.execute(() -> System.out.println("Heeey!"));
        pool.execute(() -> System.out.println("Heeey!"));
        pool.shutdown();
        System.out.println(pool.getCompletedTaskCount());
    }
}
