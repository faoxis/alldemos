package mypool;

import java.util.*;
import java.util.concurrent.*;

public class MyPool {

    private final int size;
    private boolean isFinished = false;
    private LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
    private Set<Thread> threads = new HashSet<>();

    public MyPool(int size) {
        Collections.synchronizedSet(threads);
        this.size = size;
        checkThreads();
        for (int i = 0; i < size; i++) {
            Thread thread = new Thread(() -> {
                while (!isFinished) {
                    try {
                        Thread.sleep(1_000);
                        checkThreads();
                    } catch (InterruptedException e) {
                        isFinished = true;
                    }
                }
            });
            thread.setDaemon(true);
            thread.start();
        }
    }

    private void addThread() {
        Thread thread = new Thread(() -> {
            while (!isFinished) {
                try {
                    Runnable runnable = queue.take();
                    runnable.run();
                } catch (InterruptedException e) { }
            }
        });
        thread.start();
        threads.add(thread);
    }

    private void checkThreads() {
        Set<Thread> threadsToRemove = new HashSet<>();

        for (Thread thread: threads) {
            if (!thread.isAlive()) threadsToRemove.add(thread);
        }

        for (Thread thread: threadsToRemove) {
            threads.remove(thread);
        }

        while (threads.size() < size) {
            addThread();
        }
    }

    public void execute(Runnable runnable) throws InterruptedException {
        if (!isFinished) queue.put(runnable);
    }

    public void shutdown() {
        isFinished = true;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public int getCurrentPoolSize() {
        return threads.size();
    }

}
