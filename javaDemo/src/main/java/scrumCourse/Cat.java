package scrumCourse;

public class Cat {

    final private String name;
    final private int age;

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int play(Clew clew) {
        return clew.getSize() / age;
    }


}