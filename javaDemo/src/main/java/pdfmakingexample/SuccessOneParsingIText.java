package pdfmakingexample;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;

import java.io.File;
import java.util.Objects;

public class SuccessOneParsingIText {
    public static void main(String[] args) throws Exception {
        String pathToHere = "javaDemo/src/main/java/pdfmakingexample/";
        String src = pathToHere + "AnketaCoborover.pdf";
        String dest = pathToHere + "ITextPDF.pdf";


            try (PDDocument pdfDocument = PDDocument.load(new File(src))) {
                PDAcroForm form = pdfDocument.getDocumentCatalog().getAcroForm();

                PDFont font = PDType0Font.load(pdfDocument, new File(pathToHere + "Fonts/Helv Normal.ttf"));
                PDResources res = new PDResources();
                res.put(COSName.getPDFName("Helv"), font);
                res.put(COSName.getPDFName("Arial,Bold"), font);
                form.setDefaultResources(res);

                form.getField("ФИО")
                        .getWidgets()
                        .get(0)
                        .setHidden(false);
                form.getField("ФИО")
                        .setValue("сомесинasd");

//                form.setNeedAppearances(true);
                form.refreshAppearances();
//                form.flatten();
                pdfDocument.save(new File(dest));

            }
    }
}

