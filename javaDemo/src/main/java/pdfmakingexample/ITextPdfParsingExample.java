package pdfmakingexample;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.*;

import java.io.FileOutputStream;
import java.io.IOException;

public class ITextPdfParsingExample {

    public static void main(String[] args) throws IOException, DocumentException {

        String src = "AnketaCoborover.pdf";
        String dest = "AnketaCoborover1.pdf";


        PdfReader reader = new PdfReader(src);
        System.out.println("Pages: " + reader.getNumberOfPages());
        reader.getAcroFields().getFields().forEach((k, v) -> {
            System.out.println(k + " " + v);
        });

        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));

        // ФИО полностью
        stamper.getAcroFields().setField("fill_1", "213");

        // Паспортные данные (серия)
        stamper.getAcroFields().setField("comb_1", "0000");

        // Паспортные данные (номер)
        stamper.getAcroFields().setField("Ž", "нннннн");

        // Созаемщик
        stamper.getAcroFields().setField("fill_2", "Кто-то");

//        stamper.getAcroFields().setField("fill_1", "213");

        stamper.close();
        reader.close();

    }

}
