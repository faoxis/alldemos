package pdfmakingexample;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.*;

import java.io.FileOutputStream;
import java.io.IOException;

public class SuccessParsingIText {
    public static void main(String[] args) throws IOException, DocumentException {
        String pathToHere = "javaDemo/src/main/java/pdfmakingexample/";
        String src = pathToHere + "AnketaCoborover.pdf";
        String dest = pathToHere + "ITextPDF.pdf";

        PdfReader reader = new PdfReader(src);
        PRAcroForm form = reader.getAcroForm();

        PRAcroForm.FieldInformation field = form.getField("Group25");
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));

        AcroFields fields = stamper.getAcroFields();
        System.out.println(reader.getAcroFields().getField("Group25"));
        fields.setField("Group25", "2");
        fields.setField("ФИО", "ааааЭ");

        stamper.flush();
        stamper.close();
        reader.close();


        PdfReader reader1 = new PdfReader(src);
        AcroFields fields1 = reader.getAcroFields();

        System.out.println(fields1.getField("ФИО"));
        reader.close();
    }
}
