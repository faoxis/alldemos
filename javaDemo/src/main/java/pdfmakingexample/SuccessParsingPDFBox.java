package pdfmakingexample;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class SuccessParsingPDFBox {
    public static void main(String[] args) throws IOException {

        String pathToHere = "javaDemo/src/main/java/pdfmakingexample/";
        String src = pathToHere + "AnketaCoborover.pdf";
        String dest = pathToHere + "ITextPDF.pdf";

        File filesFolder = new File(pathToHere + "Fonts");
        for (File fileFont: Objects.requireNonNull(filesFolder.listFiles())) {

                try (PDDocument pdfDocument = PDDocument.load(new File(src))) {
                    PDAcroForm form = pdfDocument.getDocumentCatalog().getAcroForm();

                    PDFont font = PDType0Font.load(pdfDocument, fileFont);
                    PDResources res = new PDResources();
                    res.put(COSName.getPDFName("Helv"), font);
//                    COSName fontName = res.add(font);
                    form.setDefaultResources(res);
//
//                    String da = "/" + fontName.getName() + " 12 Tf 0 g";
//                    form.setDefaultAppearance(da);
//
//                    PDResources resources = new PDResources();
//                    resources.put(COSName.getPDFName("Helv"), PDType1Font.HELVETICA);
//                    form.setDefaultResources(resources);

                    form.getField("ФИО")
                        .setValue("сомесин");

                    pdfDocument.save(new File(dest));

                    System.out.println("--------------=+++++++++++++++++ ASF AG ASHG AHAHE HH E HSH E" + fileFont.getName());
                } catch (Exception e) {
                    System.out.println(fileFont.getName() + ": is not correct " + e.getMessage());
                }

        }

    }
}

