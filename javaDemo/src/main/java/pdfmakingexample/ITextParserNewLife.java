package pdfmakingexample;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

public class ITextParserNewLife {
    public static void main(String[] args) throws IOException, DocumentException {
        String src = "AnketaCoborover.pdf";
        String dest = "NewPdfHaHaHa.pdf";


        PdfReader reader = new PdfReader(dest);

        for (Map.Entry<String, AcroFields.Item> entry : reader.getAcroFields().getFields().entrySet()) {
            String k = entry.getKey();
            AcroFields.Item v = entry.getValue();
            System.out.println(k + " " + v);
        }

//        reader.getAcroFields().setField("Group25", "Выбор2");
//        reader.getAcroFields().getField("Group25");
        System.out.println(reader.getAcroFields().getField("Group25"));
//        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
//         ФИО полностью
//        stamper.getAcroFields().setField("fill_1", "213");
//
//        stamper.getAcroFields().setField("Group25", "Выбор2");
//
//
//
//        stamper.close();
        reader.close();
    }
}
