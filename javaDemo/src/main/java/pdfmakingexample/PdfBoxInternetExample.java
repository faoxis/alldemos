package pdfmakingexample;

import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class PdfBoxInternetExample {
    private static String src = "AnketaCoborover.pdf";
    private static String dest = "PDFBoxAnketaCoborover2.pdf";

    private final static String fieldName = "fill_1";

    public static void main(String[] args) throws Exception {
//        testFillLikeRichardBrown();
//        testFillLikeStDdt();
//        testFillLikeBarbara();
        testFillLikeJuvi();
    }

    public static void testFillLikeJuvi() throws IOException {
            PDDocument document = PDDocument.load(new File(src));
            PDDocumentCatalog docCatalog = document.getDocumentCatalog();
            PDAcroForm acroForm = docCatalog.getAcroForm();

            PDTextField field = (PDTextField) acroForm.getField(fieldName);
//            field.getWidgets().forEach(x -> x.setHidden(false));
            field.setValue("54321");

            document.save(new File("testFillLikeJuvi.pdf"));
            document.close();
    }

    public static void testFillLikeBarbara() throws IOException
    {
        PDDocument pdfDocument = PDDocument.load(new File(src));
        PDAcroForm acroForm = pdfDocument.getDocumentCatalog().getAcroForm();

        if (acroForm != null)
        {
            PDTextField pdfField = (PDTextField) acroForm.getField(fieldName);
            pdfField.getWidgets().get(0).setHidden(false);// <===
            pdfField.setValue("xxxxxx");
        }

        pdfDocument.setAllSecurityToBeRemoved(true);
        COSDictionary dictionary = pdfDocument.getDocumentCatalog().getCOSObject();
        dictionary.removeItem(COSName.PERMS);

        pdfDocument.save(new File("testFillLikeBarbara.pdf"));
        pdfDocument.close();
    }

    public static void testFillLikeStDdt() throws IOException
    {
            PDDocument pdfDocument = PDDocument.load(new File(src));
            PDAcroForm acroForm = pdfDocument.getDocumentCatalog().getAcroForm();

            if (acroForm != null)
            {
                List<PDField> fields = acroForm.getFields();
                for (PDField field : fields) {
                    switch (field.getPartialName()) {
                        case fieldName /*"devices"*/:
                            field.setReadOnly(false);
                            field.setValue("Gert");
                            field.setReadOnly(true);
                            break;
                    }
                }
//                acroForm.flatten(fields, true);
            }

            pdfDocument.save(new File("testFillLikeStDdt.pdf"));
            pdfDocument.close();
    }

    public static void testFillLikeRichardBrown() throws IOException {

//        try (   InputStream originalStream = PdfBoxInternetExample.class.getResourceAsStream(src))
//        {
            PDDocument pdfDocument = PDDocument.load(new File(src));
            PDAcroForm acroForm = pdfDocument.getDocumentCatalog().getAcroForm();

            if (acroForm != null)
            {
                System.out.println(acroForm);
                PDTextField pdfField = (PDTextField) acroForm.getField("fill_1");
                pdfField.getWidgets().get(0).setHidden(false);// <===
                pdfField.setValue("xxxxxx");
            }

            pdfDocument.setAllSecurityToBeRemoved(true);
            COSDictionary dictionary = pdfDocument.getDocumentCatalog().getCOSObject();
            dictionary.removeItem(COSName.PERMS);

            pdfDocument.save(new File(dest));
            pdfDocument.close();
//        }
    }
}
