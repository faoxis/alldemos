package pdfmakingexample;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDRadioButton;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

import java.io.File;
import java.io.IOException;

public class PdfBoxParserNewLife {
    public static void main(String[] args) throws IOException {
        String src = "AnketaCoborover.pdf";
        String dest = "NewPdfHaHaHa.pdf";

        PDDocument pdfDocument = PDDocument.load(new File(src));
        PDAcroForm form = pdfDocument.getDocumentCatalog().getAcroForm();

        PDTextField fio = (PDTextField) form.getField("fill_1");
        fio.setValue("Somebody body body");

        // Покупка недвижимости на вторичном рынке жилье
        PDRadioButton realtyBuying = (PDRadioButton) form.getField("Group2");
        realtyBuying.setValue("Выбор3");

        // Страхование
        PDRadioButton btn = (PDRadioButton) form.getField("Group25");
//        btn.setValue(btn.getValue());
//        btn.setValue(btn.getValue());

        System.out.println(btn.getOnValues());

        form.getFields().forEach(field -> {
            if (field instanceof PDRadioButton) {
                PDRadioButton radio = (PDRadioButton) field;
                try {
//                    radio.getWidgets().get(0).setHidden(false);
//                    radio.setValue("Выбор2");

//                    System.out.println(radio.getFullyQualifiedName() + ": " + radio.getOnValues());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        pdfDocument.save(new File(dest));
    }
}
