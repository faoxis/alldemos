package jpoint.luxoft;

public class Third {
    public static void main(String[] args) {
        System.out.println(A.i);
        System.out.println(B.i);
    }

    static class A {
        static {
            i = 2;
        }
        static int i = 1;
    }

    static class B {
        static int i = 1;
        static {
            i = 2;
        }
    }
}
