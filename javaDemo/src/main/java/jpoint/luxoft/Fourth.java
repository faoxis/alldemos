package jpoint.luxoft;

import java.util.Arrays;
import java.util.List;

public class Fourth {

    static class Pair<T> {
        private final T first;
        private final T second;

        public Pair(T first, T second) {
            this.first = first;
            this.second = second;
        }

        public List<String> stringList() {
            return Arrays.asList(String.valueOf(first), String.valueOf(second));
        }
    }

    public static void main(String[] args) {
        Pair p = new Pair<Object>(23, "32");
        System.out.println(p.first + " " + p.second);
//        for (String s: p.stringList())
//            System.out.println(s + " ");
    }

}
