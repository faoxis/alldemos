#!/usr/bin/python
# -*- coding: utf-8 -*-
import SimpleHTTPServer
import SocketServer
import sys

if (not sys.argv[1]):
    print("There is no port")

port = int(sys.argv[1])

Handler = SimpleHTTPServer.SimpleHTTPRequestHandler

httpd = SocketServer.TCPServer(("", port), Handler)

print "serving at port", port
httpd.serve_forever()