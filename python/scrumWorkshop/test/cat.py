import unittest

from src.cat import Cat


class TestCat(unittest.TestCase):

    def test_name(self):
        cat = Cat("Boris")
        self.assertEqual("Boris", cat.name)

    def test_make_sound(self):
        cat = Cat("Boris")
        self.assertTrue('meow' in cat.make_sound())


