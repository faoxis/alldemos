package sheduledTasks;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.junit.Assert.*;

public class TimerCallerTest {

    @Test
    public void testCommonFunctional() throws Exception {
//        assertEquals(new Integer(2),
//                TimerCaller.doIt(LocalDateTime.now(),() -> 1 + 1).get());
    }

    @Test
    public void testNegativeTime() throws Exception {
//        assertEquals(new Integer(2),
//                TimerCaller.doIt(
//                        LocalDateTime.now().minus(1, ChronoUnit.MINUTES),
//                        () -> 1 + 1
//                ).get());
    }

    @Test
    public void testWorkingWithoutOverMaxSize() throws Exception {
//        int poolSize = TimerCaller.THREAD_POOL_SIZE;
//
//        List<Future<Integer>> futures = new ArrayList<>();
//        for (int i = 0; i < poolSize; i++) {
//            futures.add(TimerCaller.doIt(LocalDateTime.now().plus(1, ChronoUnit.MINUTES), () -> 1 + 1 + 1));
//        }
//
//        LocalDateTime timeCalling = LocalDateTime.now();
//        Future<Integer> onePlusOne = TimerCaller.doIt(LocalDateTime.now(), () -> 1 + 1);
//        assertEquals(new Integer(2), onePlusOne.get());
//        assertEquals(timeCalling.getMinute(), LocalDateTime.now().getMinute());

    }

    @Test
    public void testWaitingOneMinute() throws Exception {
        LocalDateTime plusOneMinute = LocalDateTime.now()
                .plus(1, ChronoUnit.MINUTES);



//        Future<String> helloMessage = TimerCaller
//                .doIt(
//                        plusOneMinute,
//                        () -> "Hello!"
//                );

//        assertEquals("Hello!", helloMessage.get());
//        assertEquals(LocalDateTime.now().getMinute(), plusOneMinute.getMinute());
    }
}