package implicitClasses

import scala.annotation.tailrec

object Helpers {
  implicit class Repeater(value: Int) {
    def times[A](f: => A): Unit = {
      @tailrec
      def loop(current: Int): Unit = {
        if (current > 0) {
          f
          loop(current - 1)
        }
      }
      loop(value)
    }
  }
}


object DocExample extends App {
  import Helpers._

  5 times println("Heeeey!")
}
