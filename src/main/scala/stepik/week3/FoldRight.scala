package stepik.week3

import scala.annotation.tailrec

object FoldRight extends App {


  def foldr[A, B](f: (A, B) => B)(ini: B)(l: List[A]): B = {
    l match {
      case Nil => ini
      case x::xs => f(x, foldr(f)(ini)(xs))
    }
  }

  def sumFunc = (x: Int, y: Int) => x + y
  println(foldr(sumFunc)(0)(List(1,3,4,5)))

}
