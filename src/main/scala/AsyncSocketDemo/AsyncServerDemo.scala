
package AsyncSocketDemo

import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.{AsynchronousServerSocketChannel, AsynchronousSocketChannel}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object AsyncServerDemo extends App {
  val PORT = 6667

  val server = AsynchronousServerSocketChannel.open()
  server.bind(new InetSocketAddress("127.0.0.1", PORT))

//  while (true) {
//    val futureChannel = server.accept()
//    val worker = Future(futureChannel.get())
//
//    val buffer: ByteBuffer = ByteBuffer.allocate(1025)
//    worker.map { clientChannel =>
//      clientChannel.read(buffer)
//    }.map { readedBytes =>
//      println(readedBytes)
//    }
//  }
//  server.close()


  Thread.sleep(1000)
}
