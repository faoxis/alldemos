package AsyncSocketDemo

import java.net.InetSocketAddress
import java.nio.channels.AsynchronousSocketChannel

object AsyncClientDemo extends App {
  import AsyncServerDemo._

  val client: AsynchronousSocketChannel = AsynchronousSocketChannel.open()
  client.connect(new InetSocketAddress("127.0.0.1", PORT))


}
