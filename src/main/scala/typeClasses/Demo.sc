// type classes example of show trait
trait Show[A] {
  def show(a: A): String
}

// The most simple realization
/*
object Show {
  val intCanShow: Show[Int] =
    new Show[Int] {
      def show(int: Int): String = s"int $int"
    }
}
import Show._
println(intCanShow.show(20))
*/

// Better with implicit
//object Show {
//  def show[A](a: A)(implicit sh: Show[A]) = sh.show(a)
//
//  implicit val intCanShow: Show[Int] =
//    (int: Int) => s"int $int"
//}
//import Show._
//show(14)



trait Twicer[A] {
  def makeTwice(a: A): String
}

object Twicer {
  def makeTwice[A: Twicer](a: A) = implicitly[Twicer[A]].makeTwice(a)

  implicit val intAgain: Twicer[Int] = (value: Int) => (2 * value).toString
  implicit val stringAgain: Twicer[String] = (value: String) => value * 2
}

import Twicer._
makeTwice(5)
makeTwice("5")














