package speedRun.lesson1

trait Eq[T] {
  def ===(x: T, y: T): Boolean
}

object Eq {
  def compareList[T](first: List[T], second: List[T])(eq: Eq[T]): Boolean = {
    first.size == second.size && first.zip(second).forall { case (x, y) => eq.===(x, y) }
  }

}
