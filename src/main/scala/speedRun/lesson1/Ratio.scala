package speedRun.lesson1

final case class Ratio(num: Int, den: Int) {
  def ===(that: Ratio): Boolean = num * that.den == that.num * den
}

object Ration {

  val eq: Eq[Ratio] = (x: Ratio, y: Ratio) => x === y

  def main(args: Array[String]) = {

  }
}

