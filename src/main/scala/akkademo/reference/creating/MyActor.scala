package akkademo.reference.creating

import akka.actor.{Actor, ActorSystem, Props}
import akka.event.Logging

class HelloSayer extends Actor {

  val greetingChooser = context.actorOf(GreetingChooser.props("Peter"), "greeting-chooser")
  val log = Logging(context.system, this)

  override def receive: Receive = {
    case x: String => greetingChooser ! x
    case _ => log.info("I don't know what to do!")
  }
}

object GreetingChooser {
  def props(name: String): Props = Props(new GreetingChooser(name))
}

class GreetingChooser(name: String) extends Actor {
  val log = Logging(context.system, this)

  override def receive: Receive = {
    case "low" => log.info(s"Hey, ${name}!")
    case "medium" => log.info(s"Hello, ${name}!")
    case "high" => log.info(s"My please, ${name}!")
  }
}


class MyActor extends Actor {

  val log = Logging(context.system, this)

  override def receive: Receive = {
    case "test" => log.info("received test")
    case _      => log.info("received unknown message")
  }
}


object MyActorSuperMegaDemo extends App {

  val system: ActorSystem = ActorSystem("super-system")
  val myActor = system.actorOf(Props[MyActor], "my-actor")

  myActor ! "test"
  myActor ! "blablabla"

  val greeter = system.actorOf(Props[HelloSayer], "hello-sayer")
  greeter ! "low"
  greeter ! "high"
  greeter ! "medium"
}
