package akkademo.askFuture

import akka.actor.{Actor, ActorSystem, Props}
import akka.util.Timeout

import scala.concurrent.Future
import scala.concurrent.duration.{Duration, FiniteDuration}

case class User(name: String, age: Int)

class ExampleActor extends Actor {
  import context.dispatcher
  import akka.pattern.pipe

  override def receive: Receive = {
    case "get" => Future {
      // just for example (Future[Option[User]])
      Some(User("Petr", 42))
    } pipeTo sender()
  }

}

object AskFutureDemo extends App {
  val system = ActorSystem("example-system")
  val example = system.actorOf(Props(new ExampleActor()))

  import akka.pattern.ask
  val duration = Duration("1s")
  implicit val timeout: Timeout = FiniteDuration(duration.length, duration.unit)
  println (example.ask("get").mapTo[Option[User]])
}
