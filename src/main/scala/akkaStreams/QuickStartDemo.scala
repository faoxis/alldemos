package akkaStreams

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.util.ByteString

import scala.concurrent._
import java.nio.file.Paths

import akka.stream.{ActorMaterializer, IOResult}
import akka.stream.scaladsl.{FileIO, Flow, Keep, Sink, Source}

object QuickStartDemo extends App {
  implicit val system = ActorSystem("quick-start")
  implicit val executionContext = system.dispatcher

  implicit val materializer = ActorMaterializer()

  val source: Source[Int, NotUsed] = Source(1 to 20)

//  val done: Future[Done] = source.runForeach(i ⇒ println(i))(materializer)
//  done.onComplete(_ => system.terminate())

  val factorials = source.scan(BigInt(1))((acc, next) => acc * next)
  val pathToThisDir = "src/main/scala/akkaStreams"
//  val result: Future[IOResult] = factorials
//        .map(num => ByteString(s"$num\n"))
//        .runWith(FileIO.toPath(Paths.get(s"$pathToThisDir/factorials.txt")))
//  result.onComplete(_ => system.terminate())

  def lineSink(filename: String): Sink[String, Future[IOResult]] =
    Flow[String]
      .map(s => ByteString(s + "\n"))
      .toMat(FileIO.toPath(Paths.get(filename)))(Keep.right)

  val res = factorials
    .map(_.toString).runWith(lineSink(s"$pathToThisDir/factorials2.txt"))
  res.onComplete(_ => system.terminate())

}
