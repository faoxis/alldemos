import cats._
import cats.instances.all._

val len: String => Int = _.length
Functor[List].map(List("scala", "cats")) (len)


val r: Either[String, Int] = Right(1000)
r.map(_ + 1)

import cats.syntax.functor._

// fproduct
val keyValueTuple = List("scala", "cats").fproduct(len)
keyValueTuple.toMap

import java.util.Date
new Date()

