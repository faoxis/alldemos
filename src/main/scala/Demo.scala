import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Demo extends App {

  val l = List(1, 2, 3, 4, 5)

  lazy val a = {
    val t = Future { "" }
  }

  for {
    x <- Future(println("1"))
    y <- Future(println("2"))
  } yield 42

  Thread.sleep(1000)
}
