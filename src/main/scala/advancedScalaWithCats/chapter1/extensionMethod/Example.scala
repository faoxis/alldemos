package advancedScalaWithCats.chapter1.extensionMethod

sealed trait Json
final case class JsObject(get: Map[String, Object]) extends Json
final case class JsString(get: String) extends Json
final case class JsNumber(get: Double) extends Json

trait JsonWriter[A] {
  def write(value: A): Json
}

final case class Person(name: String, email: String)

object JsonWriterInstances {
  implicit val stringJsonWriter: JsonWriter[String] = value => JsString(value)
  implicit val numberJsonWriter: JsonWriter[Double] = value => JsNumber(value)
  implicit val personJsonWriter: JsonWriter[Person] = (value: Person) => JsObject(Map(
    "name" -> JsString(value.name),
    "email" -> JsString(value.email)
  ))
}

object Json {
  def toJson[A](value: A)(implicit writer: JsonWriter[A]): Json = writer.write(value)
}

// Extension method
object JsonSyntax {
  implicit class JsonWriterOps[A](value: A) {
    def toJson(implicit writer: JsonWriter[A]): Json = writer.write(value)
  }
}


object Example extends App {
  import JsonWriterInstances._
  println(Json.toJson(Person("Petr", "afsg@mail.ru")))

  import JsonSyntax._
  println(Person("Peter", "peter@mail.ru").toJson)
  println("Peter".toJson)
  println(1.0.toJson)

}

