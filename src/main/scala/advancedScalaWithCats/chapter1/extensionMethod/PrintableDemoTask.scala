package advancedScalaWithCats.chapter1.extensionMethod

trait Printable[A] {
  def show(value: A): String
}

final case class Cat(
                      name: String,
                      age: Int,
                      color: String)

object PrintableInstances {
  implicit val stringPrintableFormat: Printable[String] = value => value
  implicit val intPrintableFormat: Printable[Int] = value => value.toString
  implicit val catPrintableFormat: Printable[Cat] = value => s"${value.name} is a ${value.age} year-old ${value.color} cat."
}

object Printable {
  import PrintableInstances._
  def format[A](value: A)(implicit printable: Printable[A]): String = printable.show(value)
//  def print[A](value: A) = println(format(value))
}

object PrintableSyntax {
  implicit class makePrintableOps[A](value: A) {
    def format(implicit printable: Printable[A]): String = printable.show(value)
  }
  implicit class printPrintableOps[A](value: A) {
    def print(implicit printable: Printable[A]): Unit = println(value.format)
  }
}


object PrintableDemoTask extends App {
  import PrintableInstances._
  import PrintableSyntax._

  "5".print
  5.print
  Cat("Murzik", 2, "white").print
}

