package advancedScalaWithCats.chapter1.comparing

import java.util.Date

import cats.instances.long._
import cats.kernel.Eq
import cats.syntax.eq._

object UserComparingDemo extends App {

  // (A, A) => Boolean - function for user's types
  implicit val dateEqual = Eq.instance[Date] { (date1, date2) =>
    date1.getTime === date2.getTime
  }

  val x = new Date()

  Thread.sleep(1)
  val y = new Date()

  println(x === x)
  println(x === y)


}
