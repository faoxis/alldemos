package advancedScalaWithCats.chapter1.comparing

import cats.instances.int._
import cats.instances.option._

import cats.syntax.eq._

object DefaultComparingDemo extends App {
  println(123 === 123)
  println(123 === 124)

  // compilation error
  // println(123 === "123")

  // compilation error
  //  println(Some(1) === None)

  //that works
  println((Some(1): Option[Int]) === (None: Option[Int]))

  // works as well
  println(Option(1) === Option.empty[Int])

  // works with cats support
  import cats.syntax.option._
  println(1.some === None)
  println(1.some =!= None)



}
