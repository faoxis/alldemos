package advancedScalaWithCats.chapter1.comparing

import cats.syntax.eq._
import cats.instances.string._
import cats.instances.option._
import cats.instances.int._
import cats.kernel.Eq


final case class Cat(name: String, age: Int, color: String)

object ComparingTask extends App {

  // (A, A) => Boolean
  implicit val catEqual = Eq.instance[Cat] { (cat1, cat2) =>
    cat1.name === cat2.name &&
    cat1.age === cat2.age &&
    cat1.color == cat2.color
  }

  // testing
  val cat1 = Cat("Garfield", 35, "orange and black")
  val cat2 = Cat("Heathcliff", 30, "orange and black")

  val optionCat1 = Option(cat1)
  val optionCat2 = Option.empty[Cat]

  assert (optionCat1 =!= optionCat2)
}

