package advancedScalaWithCats.chapter1.myOwnExamples

trait Awesomable[A] {
  def beAwesome(value: A): String
}

object AwesomableInstances {
  implicit val stringAwesome: Awesomable[String] = value => s"------###|$value|###------"
  implicit val intAwesome: Awesomable[Int] = value => s"------###|$value|###------"
}

object Awesomable {
  def beAwesome[A](value: A)(implicit awesomable: Awesomable[A]): String = awesomable.beAwesome(value)
}

object AwesomableSyntax {
  implicit class AwesomeOps[A](value: A) {
    def beAwesome(implicit awesome: Awesomable[A]): String = awesome.beAwesome(value)
  }
}

object TypeClassesDemo extends App {
  import AwesomableInstances._
  println(Awesomable.beAwesome("Heey!"))
  println(Awesomable.beAwesome(123))

  import AwesomableSyntax._
  println(123 beAwesome)
  println("abc" beAwesome)
}
