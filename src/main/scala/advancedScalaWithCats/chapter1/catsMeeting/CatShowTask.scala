package advancedScalaWithCats.chapter1.catsMeeting


final case class Cat(name: String, age: Int, color: String)

object CatShowTask extends App {
  import cats.Show
  import cats.syntax.show._
  import cats.instances.all._

  implicit val catShow: Show[Cat] =
    Show.show(cat => s"${cat.name} is a ${cat.age} year-old ${cat.color} cat.")

  println(Cat("Murzik", 2, "red").show)

}


