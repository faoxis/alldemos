
import cats.Show
import cats.instances.int._
import cats.instances.string._

val showInt: Show[Int] = Show.apply[Int]
val showString: Show[String] = Show.apply[String]

val intAsString = showInt.show(123)
val stringAsString = showString.show("abc")
println(intAsString)
println(stringAsString)

import cats.syntax.show._
val showIntBySyntax = 123.show
val showStringBySyntax = "abc".show
println(showIntBySyntax)
println(showStringBySyntax)

import java.util.Date

implicit val dataShow: Show[Date] =
  Show.show(date => s"${date.getTime}ms since the epoch.")

(new Date()).show

