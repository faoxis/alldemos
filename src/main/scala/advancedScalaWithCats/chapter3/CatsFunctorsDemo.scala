package advancedScalaWithCats.chapter3

import cats.Functor
import cats.instances.function._
import cats.syntax.functor._

object CatsFunctorsDemo extends App {

  val func1 = (a: Int) => a + 1
  val func2 = (a: Int) => a * 2
//  val func3 = func2.map(func1)

  // own functor for option
  implicit val optionFunctor = new Functor[Option] {
    def map[A, B](value: Option[A])(f: A => B): Option[B] = {
      value.map(f)
    }
  }


}


