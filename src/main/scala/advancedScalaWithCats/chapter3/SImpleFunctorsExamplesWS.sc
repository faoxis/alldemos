import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

import scala.language.higherKinds

// Functor - is a class with map function
trait Functor[F[_]] {
  def map[A, B](fa: F[A])(f: A => B): F[B]
}

// functor's rules
// fa.map(a => a) == fa
// fa.map(g(f(_)) == fa.map(f).map(g)

// List - type constructor
// List[A] - type




// Future is example of functor
val future1 = Future("Hello there!")
val future2 = future1.map(_.length)

Await.result(future1, 1.second)
Await.result(future2, 1.second)

println(future2.value)
