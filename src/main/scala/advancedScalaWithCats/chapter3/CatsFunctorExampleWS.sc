import cats.Functor
import cats.instances.list._
import cats.instances.option._

// map for List
val list1 = List(1, 2, 3)
val list2 = Functor[List].map(list1)(_ * 2)
list2.foreach(println) // 2, 4, 6

// map for option
val option1 = Option(123)
val option2 = Functor[Option]
  .map(option1)(_.toString + " string")
option2.foreach(println) // 123 string

// lift method
val func = (x: Int) => x + 1 //
val lifted = Functor[Option].lift(func)
lifted(Option(1))

// extension method
import cats.instances.function._
import cats.syntax.functor._

val func1 = (a: Int) => a + 1
val func2 = (a: Int) => a * 2
//val func3 = func1.map(func2)









