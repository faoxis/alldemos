package advancedScalaWithCats.chapter3

object TypeContructor extends App {
  import cats.Functor
  import cats.instances.all._
  import scala.language.higherKinds

  // type constructor
  //def myMethod[F[_]] = {
  //  val functor = Functor.apply[F]
  //}

  // cats' functor with map
  val list1 = List(1, 2, 3)
  val list2 = Functor[List].map(list1)(_ * 2)
  list2.foreach(println) // 2 4 6

  // cats' functor with lift
  val func = (x: Int) => x + 1
  val lifted = Functor[Option].lift(func)
  println(lifted(Option(1))) // Some(2)

  import cats.instances.function._
  import cats.syntax.functor._

  val func1 = (a: Int) => a + 1
  val func2 = (a: Int) => a * 2

//  val func3 = func1.map(func2)
}
