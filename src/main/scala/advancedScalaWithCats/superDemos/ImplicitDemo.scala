package advancedScalaWithCats.superDemos

import scala.annotation.tailrec


object ImplicitDemo extends App {

  trait MySuperTrait {
    def sayHello(): String
  }

  implicit val mySuperTrait: MySuperTrait = () => "Heeey!"

  @tailrec
  def theMostSimpleFunction(times: Int) (implicit mySuperTrait: MySuperTrait) {
    if (times > 0) {
      println(mySuperTrait.sayHello())
      theMostSimpleFunction(times - 1)
    }
  }

  implicit class HelloSayer(timesCount: Int) {
    def timesGreeting(): Unit = {
      theMostSimpleFunction(timesCount)
    }
  }

  import ImplicitDemo._
//  theMostSimpleFunction(3)
  3 timesGreeting

}

