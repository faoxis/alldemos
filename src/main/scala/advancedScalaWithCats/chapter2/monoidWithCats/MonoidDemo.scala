package advancedScalaWithCats.chapter2.monoidWithCats

import cats.Monoid
import cats.Semigroup
import cats.instances.string._
import cats.instances.int._
import cats.instances.option._

object MonoidDemo extends App {
  Monoid[String].combine("Hi", "There")
  Monoid[String].empty

  Monoid[Int].combine(123, 123)
  Monoid[Option[Int]].combine(Option(123), Option(132))

  // Semigroup example
  Semigroup[String].combine("Hi", "there")
}
