
class A
class B extends A
class C extends B


sealed class Foo[String]
sealed class Bar[-B]

val foo = new Foo[C]

