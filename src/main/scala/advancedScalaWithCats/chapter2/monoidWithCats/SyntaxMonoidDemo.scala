package advancedScalaWithCats.chapter2.monoidWithCats

import cats.syntax.semigroup._
import cats.kernel.Monoid

import cats.instances.string._
import cats.instances.int._

object SyntaxMonoidDemo extends App {
  val stringResult = "Hi " |+| "there" |+| Monoid[String].empty
  println(stringResult)

  val intResult = 1 |+| 2 |+| + Monoid[Int].empty
  println(intResult)
}

