import cats.kernel.Monoid
import cats.syntax.semigroup._
import cats.instances.all._

implicit val multiplication =
  new Monoid[Int] {
    override def empty = 1
    override def combine(x: Int, y: Int) =
      x * y
  }

//println(2 |+| 3 |+| + Monoid[Int].empty)
