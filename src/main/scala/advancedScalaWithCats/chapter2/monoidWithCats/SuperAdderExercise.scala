package advancedScalaWithCats.chapter2.monoidWithCats

import cats.kernel.Monoid
import cats.syntax.monoid._
import cats.instances.int._
import cats.instances.option._
import cats.syntax.option._

import scala.annotation.tailrec

case class Order(totalCost: Double, quantity: Double)

class SuperAdder {
  import SuperAdder._

  def add[A: Monoid](items: List[A]): A = items match {
    case x::xs => x |+| add(xs)
    case _ => Monoid[A].empty
  }
}

object SuperAdder {
  def apply: SuperAdder = new SuperAdder()

  implicit val monoidOrder: Monoid[Order] = new Monoid[Order] {
    override def empty: Order = Order(0, 0)

    override def combine(x: Order, y: Order): Order = Order(
      totalCost = x.totalCost + y.totalCost,
      quantity = x.quantity + y.quantity
    )
  }
}

object SuperAdderExercise extends App {
  import SuperAdder._

  println(new SuperAdder().add(1::2::3::4::Nil))
  println(new SuperAdder().add(1.some::2.some::3.some::4.some::Nil))
  println(new SuperAdder().add(
    List(Order(1, 1), Order (1, 2), Order (2, 1))
  ))
}


