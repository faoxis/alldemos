trait Semigroup[A] {
  def combine(v1: A, v2: A): A
}

trait Monoid[A] extends Semigroup[A] {
  def empty: A
}

object Monoid {
  def apply[A](implicit monoid: Monoid[A]) =
    monoid
}

val monoid1 = new Monoid[Boolean] {
  override def combine(v1: Boolean,
                       v2: Boolean) =
    v1 && v2

  override def empty = true
}

val monoid2 = new Monoid[Boolean] {
  override def combine(v1: Boolean,
                       v2: Boolean) =
    v1 || v2

  override def empty = false
}





