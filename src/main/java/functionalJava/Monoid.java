package functionalJava;

public interface Monoid<T> extends Semigroup<T> {
    T empty();
}
