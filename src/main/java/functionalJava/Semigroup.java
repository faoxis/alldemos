package functionalJava;

public interface Semigroup<T> {
    T combine(T t1, T t2);
}
