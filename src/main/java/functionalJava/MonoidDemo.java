package functionalJava;

import reactor.core.publisher.Mono;
import scala.Int;

public class MonoidDemo {

    public static <T> boolean associativeLaw(T x, T y, T z, Monoid<T> monoid) {
        return monoid.combine(x, monoid.combine(y, z))
                .equals(monoid.combine(monoid.combine(x, y), z));
    }

    public static <T> boolean identityLaw(T x, Monoid<T> monoid) {
        return monoid.combine(x, monoid.empty())
                .equals(x)
                &&
               monoid.combine(monoid.empty(), x)
                .equals(x);
    }

    public static void main(String[] args) {
        Monoid<Boolean> booleanMonoid = new Monoid<Boolean>() {
            @Override
            public Boolean empty() {
                return true;
            }

            @Override
            public Boolean combine(Boolean t1, Boolean t2) {
                return t1 & t2;
            }
        };

        assert associativeLaw(false, false, false, booleanMonoid);
        assert associativeLaw(true, false, false, booleanMonoid);
        assert associativeLaw(true, false, false, booleanMonoid);
        assert identityLaw(true, booleanMonoid);
        assert identityLaw(false, booleanMonoid);

        Monoid<Integer> sumMonoid = new Monoid<Integer>() {
            @Override
            public Integer empty() {
                return 0;
            }

            @Override
            public Integer combine(Integer t1, Integer t2) {
                return t1 + t2;
            }
        };

        assert associativeLaw(1, 2, 3, sumMonoid);
        assert associativeLaw(2, 1, 3, sumMonoid);
        assert associativeLaw(1, 3, 3, sumMonoid);
        assert associativeLaw(1, 4, 2, sumMonoid);
        assert identityLaw(1, sumMonoid);


        Monoid<String> concatMooid = new Monoid<String>() {
            @Override
            public String empty() {
                return "";
            }

            @Override
            public String combine(String t1, String t2) {
                return t1 + t2;
            }
        };

        assert associativeLaw("Peter", "Griffin", "First", concatMooid);
        assert identityLaw("Peter", concatMooid);

    }

}
