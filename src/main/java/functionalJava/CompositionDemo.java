package functionalJava;

import akka.japi.function.Function2;
import io.vavr.Function1;

import java.util.function.Function;

public class CompositionDemo {
    public static void main(String[] args) {
        Function1<Integer, Double> func1 = Double::valueOf;
        Function1<Double, Double> func2 = number -> number * 2;

        Function<Integer, Double> func3 = func2.compose(func1);
        System.out.println(func3.apply(3));


    }
}