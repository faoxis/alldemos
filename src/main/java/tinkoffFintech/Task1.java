package tinkoffFintech;

import io.netty.util.internal.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * Задача 1
 * Роберт начал работать тайным агентом и теперь встреча с куратором начинается со стандартной процедуры подтверждения личности.
 * Куратор называет любое натуральное число XX, а Роберт должен ответить, сколько существует чисел-палиндромов \leqslant X⩽X.
 * Помогите Роберту написать программу, которая сможет дать ответ на такие запросы.
 * <p>
 * Входные данные
 * Первая и единственная строка содержит число XX (1 \leqslant X \leqslant100 000)(1⩽X⩽100000)
 * Результат работы
 * Выведите ответ для задачи.
 */
public class Task1 {


    static long countPalindromes(long n) {
        return LongStream
                .range(1, n + 1)
                .filter(x -> {
                    String s = String.valueOf(x);
                    return s.equals(new StringBuilder(s).reverse().toString());
                })
                .count();
    }

    public static void printList(List<Integer> list) {
        list.forEach(System.out::println);
    }

    public static void main(String[] args) {


    }
}
