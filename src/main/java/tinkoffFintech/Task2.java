package tinkoffFintech;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Task2 {
    public static void main(String[] args) {
        //        printList(Arrays.asList(1,2,3));
//        "sos"
//                .chars()
//                .distinct()
//                .forEach(x -> System.out.println(x));


        List<Integer> input = Arrays.asList(1, 2, 3, 4, 2, 4, 5, 8, 2);
//        List<Integer> result = input
//                .stream()
//                .parallel()
//                .filter(x -> input.stream().filter(x::equals).count() > 1)
//                .distinct()
//                .collect(Collectors.toList());
        List<Integer> result = input.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        System.out.println(result);
    }
}
