package asyncSocket.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class AsyncServerDemo {
    public static int PORT = 6669;


    public static void main(String[] args) throws IOException, InterruptedException, ExecutionException, TimeoutException {
        AsynchronousServerSocketChannel server = AsynchronousServerSocketChannel.open();
        InetSocketAddress hostAddress = new InetSocketAddress("localhost", PORT);
        server.bind(hostAddress);

        System.out.println("The sever starts working...");
        Attachment attach = new Attachment();
        server.accept(attach, new ConnectionHandler(server));

        Thread.currentThread().join();
    }


}
