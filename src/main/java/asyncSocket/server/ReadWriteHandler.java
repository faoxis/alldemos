package asyncSocket.server;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.Map;

public class ReadWriteHandler implements CompletionHandler<Integer, Attachment> {

    @Override
    public void completed(Integer result, Attachment attachment) {
        System.out.println("Was readed: " + result + " bytes");
        System.out.println("The message is: ");
        ByteBuffer byteBuffer = attachment.buffer;
        String msg = new String(byteBuffer.array());
        System.out.println(msg);

//            byteBuffer.put("Hello from server!".getBytes());
        attachment.client.write(ByteBuffer.wrap("Hello from server!".getBytes()));
    }

    @Override
    public void failed(Throwable exc, Attachment attachment) {
        System.out.println("fail!");
    }

}