package asyncSocket;

import asyncSocket.server.AsyncServerDemo;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class AsyncClientDemo {
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        AsynchronousSocketChannel client = AsynchronousSocketChannel.open();
        Future<Void> future =
                client.connect(new InetSocketAddress("localhost", AsyncServerDemo.PORT));
        future.get();

        while (true) {
            sendMessage(client, "heeeey!");
            getMessage(client);
            Thread.sleep(1000);
            System.out.println(client.isOpen());
        }

    }

    public static void getMessage(AsynchronousSocketChannel client) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(100);
        Future<Integer> future = client.read(byteBuffer);
        CompletableFuture.supplyAsync(() -> {
            try {
                int readedBytes = future.get();
                System.out.println(readedBytes + " bytes have been read");
                System.out.println(new String(byteBuffer.array()));
                byteBuffer.flip();
                return readedBytes;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return 0;
            }
        });
    }

    public static void sendMessage(AsynchronousSocketChannel client, String message) throws ExecutionException, InterruptedException {
        byte[] byteMsg = message.getBytes();
        ByteBuffer buffer = ByteBuffer.wrap(byteMsg);
        Future<Integer> writeResult = client.write(buffer);
        CompletableFuture.supplyAsync(() -> {
            try {
                int readedBytes = writeResult.get();
                System.out.println("message has been sent");
                System.out.println("bytes have been sent: " + readedBytes);
                return readedBytes;
            } catch (InterruptedException | ExecutionException e) {
                return 0;
            }
        });
        // do some computation

//        writeResult.get();
//        buffer.flip();
//        Future<Integer> readResult = client.read(buffer);

        // do some computation

//        readResult.get();
//        String echo = new String(buffer.array()).trim();
//        buffer.clear();


//        return echo;
    }
}
