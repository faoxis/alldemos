import java.util.stream.IntStream;
import java.util.stream.Stream;

import static io.vavr.API.*;
import static io.vavr.Predicates.*;

public class PatternMatchingDemo {
    public static void main(String[] args) {
        IntStream
                .iterate(0, x -> x + 1)
                .forEach(System.out::println);
    }
}
