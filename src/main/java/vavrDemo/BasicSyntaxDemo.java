package vavrDemo;

import io.vavr.*;
import io.vavr.collection.List;
import io.vavr.collection.Queue;
import io.vavr.collection.Stream;
import io.vavr.concurrent.Future;
import io.vavr.control.Option;
import io.vavr.control.Try;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.IntStream;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.API.Match;
import static io.vavr.Predicates.anyOf;
import static io.vavr.Predicates.is;
import static io.vavr.Predicates.isIn;

public class BasicSyntaxDemo {

    public static void main(String[] args) {
//        Optional<String> maybeFoo = Optional.of("foo");
//        Optional<String> maybeFooBar = maybeFoo.map(s -> (String)null)
//                .map(s -> s.toUpperCase() + "bar");
//        System.out.println(maybeFooBar);

        final int i = 1;
        String s = Match(1993).of(
                Case($(42), () -> "one"),
                Case($(anyOf(isIn(1990, 1991, 1992), is(1993))), "two"),
                Case($(), "?")
        );
        System.out.println(s); // one
    }



}
