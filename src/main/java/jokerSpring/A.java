package jokerSpring;

import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class A {
    private Optional<B> b;

    public A(Optional<B> b) {this.b = b;}

    @Override
    public String toString() {
        return b.toString();
    }

}
