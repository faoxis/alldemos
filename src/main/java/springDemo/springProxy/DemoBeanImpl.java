package springDemo.springProxy;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class DemoBeanImpl implements DemoBean {

    @Override
    @Transactional
    public void doSomething() {
        System.out.println("doing something");
    }
}
