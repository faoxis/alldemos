package springDemo.webfluxDemo.withoutBoot;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DemoUser {
    private String name;
    private int age;
}
