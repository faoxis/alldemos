package springDemo.webfluxDemo.withoutBoot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.ipc.netty.http.server.HttpServer;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

public class MyDemoFPSpringServer {

    private final static Logger logger = LogManager.getLogger(MyDemoFPSpringServer.class);

    public static final String HELLO_WORLD_URL = "/hello-world";

    private static HandlerFunction<ServerResponse> helloWorldHandler = r -> ok()
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(new DemoUser("Peter", 44)), DemoUser.class);

    public static RouterFunction<ServerResponse> helloWorldRouter = request -> {
        if (request.method() == GET && request.path().equals(HELLO_WORLD_URL)) {
            return Mono.just(helloWorldHandler);
        }
        return Mono.empty();
    };


    public static void main(String[] args) throws InterruptedException {

        HttpHandler httpHandler = RouterFunctions.toHttpHandler(helloWorldRouter);
        HttpServer.create(8667)
                .newHandler(new ReactorHttpHandlerAdapter(httpHandler))
                .block();

        WebClient webClient = WebClient.create("http://localhost:8667");

        while (true) {
            Mono user = webClient.get()
                    .uri("/hello-world")
                    .retrieve()
                    .bodyToMono(Object.class);

            user
                    .doOnError(e -> System.out.println("bad: " + e.toString()))
                    .subscribe(usr -> {
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println(usr);
            });
            Thread.sleep(10 * 1000);
        }
    }

}
