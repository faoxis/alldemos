package springDemo.webfluxDemo.withoutBoot;

import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.ipc.netty.http.server.HttpServer;

import java.util.concurrent.CountDownLatch;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

public class ExternalDemoFunctionalProgrammingModel {
    public static void main(String...a) throws InterruptedException {

        RouterFunction<ServerResponse> helloSpringRoute = request ->  {

                    if(request.method() == GET &&
                            request.path().equals("/hellospring")
                            ) {
                        return Mono.just(r ->
                                ok().body(
                                        Mono.just("Functional Spring"),
                                        String.class
                                )
                        );
                    } else {
                        return Mono.empty();
                    }
                };

        RouterFunction<ServerResponse> myRoute = request -> {
            if (request.method() == GET && request.path().equals("/blablabla")) return Mono.just(r -> ok().build());
            else return Mono.empty();
        };

        HandlerFunction helloWorld = request -> ok().body(Mono.just("Hello, World"), String.class);
        RouterFunction helloWorldRoute =
                RouterFunctions.route(
                        GET("/helloworld"), helloWorld);

        RouterFunction routes = helloSpringRoute
                .and(helloWorldRoute)
                .and(myRoute);

        HttpHandler httpHandler = RouterFunctions.toHttpHandler(routes);
        HttpServer.create(8580)
                .newHandler(new ReactorHttpHandlerAdapter(httpHandler))
                .block();

        waiteForever();
    }

    private static void waiteForever() {
        CountDownLatch latch = new CountDownLatch(1);
        Thread awaitThread = new Thread(() -> {
            try {
                latch.await();
            } catch (InterruptedException ex) {
            }

        });
        awaitThread.setDaemon(false);
        awaitThread.start();

    }
}
