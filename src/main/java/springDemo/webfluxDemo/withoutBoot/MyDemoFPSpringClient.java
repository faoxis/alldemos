package springDemo.webfluxDemo.withoutBoot;

import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public class MyDemoFPSpringClient {
    public static void main(String[] args) {
        WebClient webClient = WebClient.create("http://localhost");
        Mono<DemoUser> user = webClient.get()
                .uri("/hello-world")
                .retrieve()
                .bodyToMono(DemoUser.class);

        user.subscribe(System.out::println);
    }
}
