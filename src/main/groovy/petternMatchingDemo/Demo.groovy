class Demo {

    static def regExpMatcher = "[a-я]*[А-Я]*\\w*\\d*[!\"“#\$%&'()*+,-.\\/:;\\\\<=>?@ \\[\\]^_`ЄєЇїЎў°№¤ {|} +]*"

    public static void main(String[] args) {
        StringBuilder wrongSymbols = new StringBuilder()
        String testString = "ю☻д13☻2141251a☻sdAD!\"“#\$%&'()*+,-./:;<=>?@ [\\]^_`ЄєЇїЎў°№¤ {|} +"

        for (Character c : testString.toCharArray()) {
            if (!(c =~ regExpMatcher).matches()) {
                wrongSymbols.append(c)
            }
        }
        println(wrongSymbols)

//        println (("юд132141251asdAD!\"“#\$%&'()*+,-./:;<=>?@ [\\]^_`ЄєЇїЎў°№¤ {|} +" =~ regExpMatcher).matches())
    }
}