package monad;

import functor.Functor;

import java.util.function.Function;

public class Identity<T> implements Monad<T> {


    @Override
    public Monad<T> pure(T t) {
        return null;
    }

    @Override
    public <K> Monad<K> flatMap(Function<T, Monad<K>> f) {
        return null;
    }

    @Override
    public <R> Functor<R> map(Function<T, R> f) {
        return null;
    }
}
