package monad;

import functor.Functor;

import java.util.function.Function;

public interface Monad<T> extends Functor<T> {
    Monad<T> pure();
    <K> Monad<K> flatMap(Function<T, Monad<K>> f);

//    default <R> Functor<R> map(Function<T, R> f) {
//        pure().flatMap(x -> f.apply(x));
//
//    }

}

