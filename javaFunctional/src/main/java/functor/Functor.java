package functor;

import java.util.function.Function;

public interface Functor<T> {
    <R> Functor<R> map(Function<T, R> f);

    static <R> boolean checkIdentityLow(Functor<R> functor) {
        Function id = x -> x;
        return functor.map(id).equals(id.apply(functor));
    }

    static <A, B, C> boolean checkAssosiationLow(Functor<A> functor, Function<A, B> f, Function <B, C> g) {
        return functor.map(g.compose(f))
                .equals(functor.map(f).map(g));
    }

}
