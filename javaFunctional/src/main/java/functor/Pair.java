package functor;

import java.util.function.Function;

public class Pair<R, T> implements Functor<T> {

    final private R firstValue;
    final private T secondValue;

    public Pair(R firstValue, T secondValue) {
        this.firstValue = firstValue;
        this.secondValue = secondValue;
    }

    @Override
    public <K> Pair<R, K> map(Function<T, K> f) {
        return new Pair<>(firstValue, f.apply(secondValue));
    }

    public R getFirstValue() {
        return firstValue;
    }

    public T getSecondValue() {
        return secondValue;
    }

    @Override
    public String toString() {
        return "First value: " + firstValue + "\n" +
                "Second value: " + secondValue;
    }

    @Override
    public boolean equals(Object outer) {
        if (outer instanceof Pair) {
            Pair outerPair = (Pair) outer;
            return firstValue.equals(outerPair.firstValue)
                    && secondValue.equals(outerPair.secondValue);
        }
        return false;
    }

}
