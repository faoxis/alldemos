package functor;

import java.util.function.Function;

public class Container<T> implements Functor<T> {

    private final T element;

    public Container(T element) {
        this.element = element;
    }

    public T getElement() {
        return element;
    }

    @Override
    public <R> Functor<R> map(Function<T, R> f) {
        return new Container<>(f.apply(element));
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Container) {
            return element.equals(((Container) other).element);
        }
        return false;
    }
}
