package functor;

import org.junit.Test;

import java.util.function.Function;

import static org.junit.Assert.*;

public class ContainerTest {

    @Test
    public void testIdentityLow() {
        assertTrue(Functor.checkIdentityLow(new Container(1)));
        assertTrue(Functor.checkIdentityLow(new Container("123")));
        assertTrue(Functor.checkIdentityLow(new Container(1.0)));
        assertTrue(Functor.checkIdentityLow(new Container('c')));
    }

    @Test
    public void testAssosiationLow() {
        Container<Integer> container = new Container<>(42);

        Function<Integer, Double> f = Integer::doubleValue;
        Function<Double, String> g = String::valueOf;

        assertTrue(Functor.checkAssosiationLow(container, f, g));
    }
}
