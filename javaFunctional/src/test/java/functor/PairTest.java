package functor;

import org.junit.Test;

import java.util.function.Function;

import static org.junit.Assert.*;

public class PairTest {

    @Test
    public void testIdentityLow() {
        // for integer
        assertTrue(Functor.checkIdentityLow(new Pair(1, 2)));
        assertTrue(Functor.checkIdentityLow(new Pair(1, "one")));
        assertTrue(Functor.checkIdentityLow(new Pair(1, 2.0)));
    }

    @Test
    public void testAssosiationLow() {
        Pair<String, Integer> pair = new Pair<>("one", 1);

        Function<Integer, Double> f = Integer::doubleValue;
        Function<Double, String> g = String::valueOf;

        assertTrue(Functor.checkAssosiationLow(pair, f, g));
    }


    @Test
    public void testFunctorMap() {
        Pair<String, Integer> startPair = new Pair<>("one", 1);
        Function<Integer, Integer> plusOne = x -> x + 1;

        assertEquals(
                startPair
                        .map(plusOne)
                        .getSecondValue(),
                Integer.valueOf(2));

        assertEquals(
                startPair
                    .map(plusOne)
                    .map(plusOne)
                    .getSecondValue(),
                Integer.valueOf(3));

        assertEquals(
                startPair
                        .map(plusOne)
                        .map(plusOne)
                        .map(plusOne)
                        .getSecondValue(),
                Integer.valueOf(4));
    }



}