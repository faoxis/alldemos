package ru.faoxis.streams.quickStart

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Keep, RunnableGraph, Sink, Source}

import scala.concurrent.Future

object Part2BasicAndWorkingWithFlows extends App {

  implicit val system       = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val source = Source(1 to 10)
  val sink   = Sink.fold[Int, Int](0)(_ + _)

  val runnable: RunnableGraph[Future[Int]] = source.toMat(sink)(Keep.right)

  val sum: Future[Int] = runnable.run()
  sum.foreach(println)

}
