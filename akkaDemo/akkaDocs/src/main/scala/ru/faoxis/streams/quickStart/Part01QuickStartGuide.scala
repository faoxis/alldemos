package ru.faoxis.streams.quickStart

import akka.stream._
import akka.stream.scaladsl._

import akka.{ NotUsed, Done }
import akka.actor.ActorSystem
import akka.util.ByteString
import scala.concurrent._
import java.nio.file.Paths
import scala.concurrent.duration._

final case class Author(handle: String)

final case class Hashtag(name: String)

final case class Tweet(author: Author, timestamp: Long, body: String) {
  def hashtags: Set[Hashtag] = body.split(" ").collect {
    case t if t.startsWith("#") ⇒ Hashtag(t.replaceAll("[^#\\w]", ""))
  }.toSet
}

object Part01QuickStartGuide extends App {

  implicit val system = ActorSystem("quick-start")
  implicit val materializer = ActorMaterializer() // event loop

  // Источника данных.
  // Первый аргумент - тип данных,
  // второй - долонительные параметры для запуска источника (например, начстройки БД)
  val source: Source[Int, NotUsed] = Source(1 to 100)

  val done: Future[Done] = source.runForeach(println)
  implicit val ex = system.dispatcher

//  done.onComplete { x =>
//    println(x) // Success(Done)
//    system.terminate()
//  }

  // Источник данных можно переиспользовать

  // Генерируем стартовое значение и указываем способо генерации остальных
  val factorials = source.scan(BigInt(1))((acc, next) => acc * next)

  val pathToFile = "akkaDemo/akkaDocs"
  val result: Future[IOResult] =
    factorials
      .map(num => ByteString(s"$num\n"))
      .runWith(FileIO.toPath(Paths.get(s"$pathToFile/factorials.txt")))


  // Еще один пример использования стримов
  val tweets: Source[Tweet, NotUsed] = Source(
    Tweet(Author("rolandkuhn"), System.currentTimeMillis, "#akka rocks!") ::
      Tweet(Author("patriknw"), System.currentTimeMillis, "#akka !") ::
      Tweet(Author("bantonsson"), System.currentTimeMillis, "#akka !") ::
      Tweet(Author("drewhk"), System.currentTimeMillis, "#akka !") ::
      Tweet(Author("ktosopl"), System.currentTimeMillis, "#akka on the rocks!") ::
      Tweet(Author("mmartynas"), System.currentTimeMillis, "wow #akka !") ::
      Tweet(Author("akkateam"), System.currentTimeMillis, "#akka rocks!") ::
      Tweet(Author("bananaman"), System.currentTimeMillis, "#bananas rock!") ::
      Tweet(Author("appleman"), System.currentTimeMillis, "#apples rock!") ::
      Tweet(Author("drama"), System.currentTimeMillis, "we compared #apples to #oranges!") ::
      Nil)

  tweets
    .map(_.hashtags) // Get all sets of hashtags ...
    .reduce(_ ++ _) // ... and reduce them to a single set, removing duplicates across all tweets
    .mapConcat(identity) // Flatten the set of hashtags to a stream of hashtags
    .map(_.name.toUpperCase) // Convert all hashtags to upper case
    .runWith(Sink.foreach(println)) // Attach the Flow to a Sink that will finally print the hashtags

  // Все компоненты стрима могут быть переиспользованы
  def lineSink(filename: String): Sink[String, Future[IOResult]] =
    Flow[String]
        .map(s => ByteString(s + "\n"))
        .toMat(FileIO.toPath(Paths.get(filename)))(Keep.right)

  factorials
    .map(_.toString)
    .runWith(lineSink(s"$pathToFile/factorials2.txt"))

  // Процесс основанный на времени
  factorials
    .zipWith(Source(0 to 100))((num, idx) => s"$idx = $num")
    .throttle(1, 1.second)
    .runForeach(println)

  // Реактивные твиты
  val akkaTag = Hashtag("#akka")

  val authors: Source[Author, NotUsed] =
    tweets
      .filter(_.hashtags.contains(akkaTag))
      .map(_.author)
  authors.runWith(Sink.foreach(println))

}

