package workPullingPattern

import akka.actor.{Actor, ActorRef}
import workPullingPattern.ProjectWorker.{ProjectScheduled, ScheduleProject}

object ProjectWorker {
  case class ScheduleProject(project: Project)
  case class ProjectScheduled(project: Project)

  def props(projectMaster: ActorRef): Props =
    Props(new ProjectWorker(projectMaster))
}

class ProjectWorker(projectMaster: ActorRef) extends Actor {
  projectMaster ! ProjectMaster.RegisterWorker(self)

  override def receive: Actor.Receive = {
    case ScheduleProject(project) =>
      scheduleProject(project)
      projectMaster ! ProjectScheduled(project)
  }

  private def scheduleProject(project: Project) = {
    // perform project scheduling tasks
  }
}