package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.selection

import akka.actor.{ActorSystem, Props}
import akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.path.Counter

object Watch extends App {
  val system = ActorSystem("Watch-actor-selection")

  val counter = system.actorOf(Props[Counter], "counter")
  val watcher = system.actorOf(Props[Watcher], "watcher")

  Thread.sleep(100)
  system.terminate()

}
