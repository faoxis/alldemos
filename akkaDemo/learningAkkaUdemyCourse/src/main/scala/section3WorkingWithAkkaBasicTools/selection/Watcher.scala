package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.selection

import akka.actor.{Actor, ActorIdentity, ActorRef, ActorSelection, Identify}

class Watcher extends Actor {

  var counterRef: ActorRef = _
  val selection: ActorSelection = context.actorSelection("/user/counter")
  selection ! Identify(None)

  def receive = {
    case ActorIdentity(_, Some(ref)) =>
      println(s"Actor Reference for counter is ${ref}")
    case ActorIdentity(_, None) =>
      println("Actor selection for actor doesn't live :(")

  }

}
