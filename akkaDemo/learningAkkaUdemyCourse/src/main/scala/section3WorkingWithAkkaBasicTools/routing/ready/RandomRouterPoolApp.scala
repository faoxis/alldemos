package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.ready

import akka.actor.{ActorSystem, Props}
import akka.routing.FromConfig
import akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.Worker
import akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.Worker.Work

object RandomRouterPoolApp extends App {

  val system = ActorSystem("Random-Router")
  val routerPool = system
    .actorOf(FromConfig.props(Props[Worker]), "random-router-pool")

  routerPool ! Work()
  routerPool ! Work()
  routerPool ! Work()
  routerPool ! Work()

  Thread.sleep(100)
  system.terminate()

}
