package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.ready

import akka.actor.{ActorSystem, Props}
import akka.routing.RandomGroup
import akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.Worker
import akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.Worker.Work

object RandomRouterGroupApp extends App {

  val system = ActorSystem("Random-Router")

  for (i <- 1 to 3)
    system.actorOf(Props[Worker], "w" + i)

  val paths = for (i <- 1 to 3) yield "/user/w" + i
  val routerGroup = system
    .actorOf(
      RandomGroup(paths).props(),
      "random-router-group"
    )

  for (_ <- 1 to 4) routerGroup ! Work()

  system.terminate()
}
