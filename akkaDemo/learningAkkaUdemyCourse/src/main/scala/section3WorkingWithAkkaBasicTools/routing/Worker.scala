package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing

import akka.actor.Actor

class Worker extends Actor {
  import Worker._

  def receive = {
    case msg: Work =>
      println(s"I received Work Message and My ActorRef: " +
        s"${self}")
  }

}

object Worker {
  case class Work()
}
