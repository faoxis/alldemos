package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.my.pool

import akka.actor.{ActorSystem, Props}
import akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.Worker.Work

object RouterApp extends App {

  val system = ActorSystem("router")

  val router = system.actorOf(Props[RouterPool])

  router ! Work()
  router ! Work()
  router ! Work()

  Thread.sleep(100)
  system.terminate()
}
