package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.my.group

import akka.actor.{Actor, ActorRef, Props}
import akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.Worker
import akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.Worker.Work

class RouterGroup(routees: Seq[String]) extends Actor {

  def receive = {
    case msg: Work =>
      println(s"I'm A Router Group and Receive Work Message")
      context.actorSelection(routees(
              util.Random.nextInt(routees.size)
            )) forward msg
  }
}
