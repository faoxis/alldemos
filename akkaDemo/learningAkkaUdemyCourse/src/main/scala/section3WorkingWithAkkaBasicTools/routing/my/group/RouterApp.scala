package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.my.group

import akka.actor.{ActorSystem, Props}
import akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.Worker
import akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.routing.Worker.Work

object RouterApp extends App {

  val system = ActorSystem("router")

  for (i <- 0 to 5)
    system.actorOf(Props[Worker], "w" + i)

  val workers: Seq[String] =
    for (i <- 0 to 5) yield "/user/w" + i

  val routerGroup = system
    .actorOf(Props(classOf[RouterGroup], workers))

  routerGroup ! Work()
  routerGroup ! Work()

  Thread.sleep(100)
  system.terminate()
}
