package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.changingActorBehaviorViaBecomeUnbecome

import akka.actor.{Actor, Stash}

case class User(username: String, email: String)

class UserStorage extends Actor with Stash {
  import UserStorage._

  override def receive = disconnected

  def connected: Actor.Receive = {
    case Disconnect =>
      println("User Storage Disconnect from DB")
      context.unbecome()
    case Operation(op, user) =>
      println(s"User Storage receive ${op} " +
        s"to do in user: ${user}")
  }

  def disconnected: Actor.Receive = {
    case Connect =>
      println(s"User Storage connected to DB")
      unstashAll()
      context.become(connected)
    case _ =>
      stash()
  }
}



object UserStorage {
  trait DBOperation
  object DBOperation {
    case object Create extends DBOperation
    case object Update extends DBOperation
    case object Read extends DBOperation
    case object Delete extends DBOperation
  }

  case object Connect
  case object Disconnect
  case class Operation(DBOperation: DBOperation, user: Option[User])

}




