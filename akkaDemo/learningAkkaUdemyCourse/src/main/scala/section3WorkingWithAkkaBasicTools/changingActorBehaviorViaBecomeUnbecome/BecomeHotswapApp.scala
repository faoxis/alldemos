package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.changingActorBehaviorViaBecomeUnbecome

import akka.actor.{ActorSystem, Props}

object BecomeHotswapApp extends App {

  import UserStorage._

  val system = ActorSystem("Hotswap-Become")
  val userStorage = system.actorOf(Props[UserStorage],
    "userStorage")

  userStorage ! Operation(DBOperation.Create,
      Some(User("Admin", "admin@gmail.com")))
  userStorage ! Connect
  userStorage ! Disconnect

  Thread.sleep(100)
  system.terminate()
}
