package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.path

import akka.actor.Actor

class Counter extends Actor {
  import Counter._

  var count = 0

  def receive = {
    case  Inc(x) =>
      count += x
    case Dec(x) =>
      count -= x
  }


}

object Counter {
  final case class Inc(numb: Int)
  final case class Dec(num: Int)
}
