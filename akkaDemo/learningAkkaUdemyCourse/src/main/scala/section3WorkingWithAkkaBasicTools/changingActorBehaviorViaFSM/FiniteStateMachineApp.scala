package akkademo.learningAkkaUdemyCourse.section3WorkingWithAkkaBasicTools.changingActorBehaviorViaFSM

import akka.actor.{ActorSystem, Props}

object FiniteStateMachineApp extends App {

  import UserStorageFSM._

  val system = ActorSystem("Hotswap-FSM")
  val userStorage = system.actorOf(Props[UserStorageFSM], "userStorage-fsm")

  userStorage ! Connect
  userStorage ! Operation(DBOperation.Create,
    Some(User("Admin", "admin@gmail.com")))
  userStorage ! Disconnect

  Thread.sleep(100)
  system.terminate()
}
