package section4AkkaPersistence.selectQuery

import akka.actor.ActorSystem
import akka.persistence.query.{EventEnvelope, PersistenceQuery}
import akka.persistence.query.journal.leveldb.javadsl.LeveldbReadJournal
import akka.stream.ActorMaterializer
import akka.stream.javadsl.Source

object ReporterApp extends App {
  val system = ActorSystem("persistent-query")
  implicit val mat = ActorMaterializer()(system)

  val queries = PersistenceQuery(system).readJournalFor[LeveldbReadJournal](
    LeveldbReadJournal.Identifier
  )

  val evts: Source[EventEnvelope, Unit] =
    queries.eventsByPersistenceId("user-1337", 0, Long.MaxValue)

  evts.runForeach { evts => println(s"$evts") }

  Thread.sleep(1000)
  system.terminate()
}
