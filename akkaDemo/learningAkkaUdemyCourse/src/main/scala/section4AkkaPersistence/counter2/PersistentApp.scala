package section4AkkaPersistence.counter2

import akka.actor.{ActorSystem, Props}

object PersistentApp extends App {
  import Counter._

  val system = ActorSystem("persistent-actors2")
  val counter = system.actorOf(Props[Counter])

  counter ! Cmd(Increment(1))
//  counter ! Cmd(Increment(5))
//  counter ! Cmd(Decrement(3))
  counter ! "print"

  Thread.sleep(1000)
  system.terminate()
}
