package shaurma

import akka.actor.{Actor, ActorRef, ActorSystem, Props}

class ShourmaSeller extends Actor {
  override def receive: Receive = {
    case "MAKE!!!" => {
      Thread.sleep(5000)
      sender ! "DONE!!!"
    }

  }
}

class ShourmaBuyer(seller: ActorRef, name: String) extends Actor {

  override def receive: Receive = {
    case "GIVE_ME!!!" =>
      seller ! "MAKE!!!"

    case "DONE!!!" =>
      println(s"$name: Mmmm... so testy!")
  }
}

object ShaurmaApp extends App {

  implicit val system = ActorSystem("shourma-system")

  val shourmaSeller = system.actorOf(Props(new ShourmaSeller()))

  val shourmaBuyer1 = system.actorOf(Props(new ShourmaBuyer(shourmaSeller, "Kapibara")))
  val shourmaBuyer2 = system.actorOf(Props(new ShourmaBuyer(shourmaSeller, "Arnold")))

  val timeBeforeAnyBuying = System.currentTimeMillis()

  shourmaBuyer1 ! "GIVE_ME!!!"
  println("First buying: " + (System.currentTimeMillis() - timeBeforeAnyBuying))

  shourmaBuyer2 ! "GIVE_ME!!!"
  println("Second buying:" + (System.currentTimeMillis() - timeBeforeAnyBuying))

}
