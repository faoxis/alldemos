package shaurma

object A0ScalaBasic extends App {

  // companion
//  class Foo {
//    val field: String = "Hello"
//
//    def doSomething(someValue: String): Unit = {
//      someValue.toInt
//    }
//
//  }
//
//  object Foo {
//    val CONST_FIELD = "THIS IS CONST FIELD"
//  }

  // pattern matching
  val someValue: String = "baz"
//  val patternMatchingResult = someValue match {
//    case "foo" => 1
//    case "bar" => 2
//    case "baz" => 3
//  }
//  println(patternMatchingResult)


  // partial function
//  val doSomeActions: PartialFunction[String, Int] = {
//    case "foo" => 1
//    case "bar" => 2
//    case "baz" => 3
//  }
//  println(doSomeActions(someValue)) // correct

  // helper methods
//  println(doSomeActions.isDefinedAt("blabla"))
//  println(doSomeActions.isDefinedAt("baz"))


  // implicit
//  implicit val implicitName: String = "John"
//
//  def sayHello(implicit name: String): Unit = {
//    println(s"Hello, $name!")
//  }
//  sayHello("Sergei") // Hello, Sergei!
//  sayHello // Hello, John!

  // case classes
//  case class User(name: String, age: Int)
//  val john = User("John", 42)
//  println(john)
//  println(john.name)
//  println(john.age)


}
