package shaurma

import java.math.BigInteger

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.routing.RoundRobinPool

object FibonacciActor {

  def props() = Props[FibonacciActor]

  case class Number(n: Int)
  case class NumberWithAcc(n: Int, before: BigInteger, current: BigInteger)
  case class Answer(n: BigInteger)
}

class FibonacciActor extends Actor {
  import FibonacciActor._

  override def receive: Receive = {
    case Number(n) =>
      self ! NumberWithAcc(n, BigInteger.valueOf(0), BigInteger.valueOf(1))

    case NumberWithAcc(n, before, current) =>
      n match {
        case 0 => self ! Answer(before)
        case _ => self ! NumberWithAcc(n - 1, current, before.add(current))
      }

    case Answer(n) =>
      println(n)

  }
}

object FibonacciActorDemo extends App {
  import FibonacciActor._

  val actorSystem = ActorSystem("fibonacci-actor-test")

  val fibonacciActor: ActorRef = actorSystem
    .actorOf(RoundRobinPool(100).props(FibonacciActor.props()))
//  for (n <- 1 to 1000000) yield fibonacciActor ! Number(1126)
  fibonacciActor ! Number(1000)



}
