package akkademo.chat.support

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akkademo.chat.domain.{User, UserCredential}
import spray.json.DefaultJsonProtocol

object JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val userFormat = jsonFormat2(User)
  implicit val userCredentialFormat = jsonFormat2(UserCredential)
}
