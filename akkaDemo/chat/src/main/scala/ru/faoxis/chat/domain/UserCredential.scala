package akkademo.chat.domain

case class UserCredential(name: String, password: String)
