package akkademo.chat.repository

import akkademo.chat.domain.{User, UserCredential}

import scala.concurrent.{ExecutionContext, Future}

object UserRepository {

  var users: Map[String, User] = Map()
  var usersPasswords: Map[String, String] = Map()

  def createUser(userCredential: UserCredential)(implicit ex: ExecutionContext): Future[Option[User]] = Future {
      val name = userCredential.name
      val password = userCredential.password

      if (name == null
        || password == null
        || usersPasswords.get(name).isDefined) None
      else {
        usersPasswords += name -> password

        val user = User(name)
        users += name -> user
        Some(user)
      }
    }

  def auth(userCredential: UserCredential)(implicit ex: ExecutionContext): Future[Option[User]] = Future {
      if (usersPasswords
        .get(userCredential.name)
        .contains(userCredential.password)) users.get(userCredential.name)
      else None
    }
}
