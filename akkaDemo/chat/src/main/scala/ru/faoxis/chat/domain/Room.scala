package akkademo.chat.domain

case class Room(name: String, users: List[User])

trait RoomError
case object ThereIsNoSuchRoom extends RoomError
case object AlreadyExists extends RoomError

