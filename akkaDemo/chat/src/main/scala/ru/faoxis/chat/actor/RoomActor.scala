package akkademo.chat.actor

import akka.actor.Actor
import akkademo.chat.domain.User
import akkademo.chat.repository.RoomRepository


object RoomActor {


  case class Create(name: String)
  trait CreateResult
  case object ThereIsSuchRoom extends CreateResult
  case object SuccessCreatingRoom extends CreateResult

  case class DeleteRoom(name: String)
  case object GetAllRooms
  case class AddUserToRoom(user: User, roomName: String)
}

class RoomActor extends Actor {
  import RoomActor._
  import akka.pattern.pipe
  import context.dispatcher

  override def receive: PartialFunction[Any, Unit] = {
    case Create(name) => RoomRepository.createRoom(name) pipeTo sender()
    case GetAllRooms => RoomRepository.getAllRooms() pipeTo sender()
    case AddUserToRoom(user, roomName) => RoomRepository.addUser(roomName, user) pipeTo sender()
  }


}
