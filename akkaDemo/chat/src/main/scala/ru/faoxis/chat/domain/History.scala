package akkademo.chat.domain

case class History(room: Room, messages: List[Message])
