package akkademo.chat

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.stream.ActorMaterializer
//import akkademo.chat.AuthActor.Register
import akkademo.chat.domain.{User, UserCredential}
import akka.pattern.ask
import akka.util.Timeout
import akkademo.chat.actor.AuthActor

import scala.concurrent.{Future, duration}
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.io.StdIn
import akkademo.chat.support.JsonSupport._

object ChatApp extends App {

  implicit val system = ActorSystem("my-system")
  implicit val materializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val ec = system.dispatcher



  val duration = Duration("15s")
  implicit val timeout: Timeout = FiniteDuration(duration.length, duration.unit)

  val authActor = system.actorOf(AuthActor.props())
  val route =
    path("register") {
      post {
        import AuthActor._
        entity(as[UserCredential]) { userCredential =>
          complete(authActor.ask(Register(userCredential)).mapTo[Option[User]])
        }
      }
    }
//    path("")

  val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

  println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done

}
