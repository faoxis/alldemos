package akkademo.chat.domain

case class User(name: String, room: Option[String] = None)

trait UserError
case object ThereIsSuchUser extends UserError
