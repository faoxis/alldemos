package akkademo.chat.actor

import akka.actor.{Actor, Props}
import akkademo.chat.domain.UserCredential
import akkademo.chat.repository.UserRepository

object AuthActor {

  def props() = Props(new AuthActor())

  case class Register(userCredential: UserCredential)
  case class Auth(userCredential: UserCredential)

}

class AuthActor extends Actor {
  import AuthActor._
  import akka.pattern.pipe
  import context.dispatcher

  override def receive: Receive = {
    case Register(userCredential) => UserRepository.createUser(userCredential) pipeTo sender()
    case Auth(userCredential) => UserRepository.auth(userCredential) pipeTo sender()
  }
}
