package akkademo.chat.repository

import akkademo.chat.domain._

import scala.concurrent.{ExecutionContext, Future}

object RoomRepository {

  type RoomResult = Either[RoomError, Room]

  var rooms: Map[String, Room] = Map()

  def getAllRooms()(implicit ec: ExecutionContext): Future[Iterable[String]] = Future { rooms.keys }

  def createRoom(name: String)(implicit ec: ExecutionContext): Future[RoomResult] = Future {
    rooms.get(name) match {
      case Some(_) => Left(AlreadyExists)
      case _ =>
        val room = Room(name, List[User]())
        rooms += name -> room
        Right(room)
    }
  }

  def addUser(roomName: String, user: User)(implicit ec: ExecutionContext): Future[RoomResult] = Future {
    rooms.get(roomName) match {
      case Some(room) =>
        val newRoom = Room(roomName, user::room.users)
        rooms += roomName -> newRoom
        Right(newRoom)
      case _ =>
        Left(ThereIsNoSuchRoom)
    }
  }

}
