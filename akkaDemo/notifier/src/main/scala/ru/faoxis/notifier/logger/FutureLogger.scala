package ru.faoxis.notifier.logger

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class FutureLogger extends Logger[Future] {
  override def error(msg: String): Future[Unit] =
    Future {
      println(s"ERROR: $msg")
    }


  override def info(msg: String): Future[Unit] =
    Future {
      println(s"INFO: $msg")
    }
}
