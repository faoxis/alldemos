package ru.faoxis.notifier.client.joke


import ru.faoxis.notifier.model.Joke

trait JokeClient[F[_]] {
  def getJoke(): F[Either[Throwable, Joke]]
}

object JokeClient {
  def apply[F[_]](implicit client: JokeClient[F]) = client
}

