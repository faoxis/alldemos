package ru.faoxis.notifier.client.joke

import java.util.concurrent.Future

import akka.stream.ActorMaterializer
import cats.effect.IO
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal
import doobie.util.log.Success
import ru.faoxis.notifier.model._
import spray.json._

import scala.concurrent
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class AkkaHttpJokeClient(url: String)(implicit system: ActorSystem) extends JokeClient[IO] {
  override def getJoke(): IO[Either[Throwable, Joke]] = {
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    implicit val ec: ExecutionContext = system.dispatcher

    IO.fromFuture(IO {
      Http()
        .singleRequest(HttpRequest(uri = url))
        .flatMap(Unmarshal(_).to[String])
        .map(x => Try{x.parseJson.convertTo[Joke]}.toEither)
      })
  }
}
