package ru.faoxis.notifier.doobieDemo

import doobie._
import doobie.implicits._
import cats._
import cats.data._
import cats.effect.IO
import cats.implicits._
import scala.concurrent.ExecutionContext

object Part2SelectingData extends App {

  // We need a ContextShift[IO] before we can construct a Transactor[IO]. The passed ExecutionContext
  // is where nonblocking operations will be executed.
  implicit val cs = IO.contextShift(ExecutionContext.global)

  // A transactor that gets connections from java.sql.DriverManager and excutes blocking operations
  // on an unbounded pool of daemon threads. See the chapter on connection handling for more info.
  val xa = Transactor.fromDriverManager[IO](
    "org.postgresql.Driver", "jdbc:postgresql:cian", "postgres", "postgres"
  )

  // Пример с выгрузкой данных из базы и последующей обработкой
  println("Handling result:")
  sql"select state from application"
    .query[String]    // Query0[String]
    .to[List]         // ConnectionIO[List[String]]
    .transact(xa)     // IO[List[String]]
    .unsafeRunSync    // List[String]
    .take(1)          // List[String]
    .foreach(println) // Unit

  // Пример с выгрузкой только нужных данных
  println("\nHandling each row")
  sql"select state from application"
    .query[String]
    .stream
    .take(1)
    .compile
    .toList
    .transact(xa)
    .unsafeRunSync
    .foreach(println)

  println("\nTaking few values")
  sql"select state, status from application"
    .query[(String, String)]
    .stream
    .take(1)
    .compile.toList
    .transact(xa)
    .unsafeRunSync
    .foreach(println)

  println("\nTaking object:")
  case class Application(state: String, status: String)
  sql"select state, status from application"
    .query[Application]
    .stream
    .take(1)
    .compile.toList
    .transact(xa)
    .unsafeRunSync
    .foreach(println)

}
