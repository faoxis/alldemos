package ru.faoxis.notifier.doobieDemo

import doobie._
import doobie.implicits._
import cats.effect.IO
import cats.implicits._
import scala.concurrent.ExecutionContext

object Part1HelloWorldDemo extends App {
  implicit val cs = IO.contextShift(ExecutionContext.global)

  val xa = Transactor.fromDriverManager[IO](
    "org.postgresql.Driver", "jdbc:postgresql:cian", "postgres", "postgres"
  )
  case class Application(state: String)

  def find(): ConnectionIO[Option[Application]] =
    sql"select state from application limit 1".query[Application].option

  val v = find().transact(xa).unsafeRunSync.get
  println(v)



  val program3: ConnectionIO[(Int, Double)] =
    for {
      a <- sql"select 42".query[Int].unique
      b <- sql"select random()".query[Double].unique
    } yield (a, b)
//  println(program3.transact(xa).unsafeRunSync)
//  println(program3)


  val interpreter = KleisliInterpreter[IO](ExecutionContext.global).ConnectionInterpreter

  val return42 = 42.pure[ConnectionIO]
  val kleisli = return42.foldMap(interpreter)


  val io = IO(null: java.sql.Connection) >>= kleisli.run
  println(io.unsafeRunSync)

}
