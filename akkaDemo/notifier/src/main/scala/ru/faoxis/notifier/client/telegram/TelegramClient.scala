package ru.faoxis.notifier.client.telegram

import scala.language.higherKinds


trait TelegramClient[F[_]] {
  def sendMessage(chatId: String, message: String): F[Either[Throwable, String]]
}

object TelegramClient {
  def apply[F[_]](implicit telegramClient: TelegramClient[F]) = telegramClient
}

