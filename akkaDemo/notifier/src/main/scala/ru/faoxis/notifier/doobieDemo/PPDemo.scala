package ru.faoxis.notifier.doobieDemo

import java.time.{Instant, LocalDate, LocalDateTime}
import java.util.UUID

import cats.effect.IO
import doobie.Transactor
import doobie._
import doobie.implicits._

import scala.concurrent.ExecutionContext

object PPDemo extends App {
  implicit val cs = IO.contextShift(ExecutionContext.global)

//  val xa = Transactor.fromDriverManager[IO](
//    "org.postgresql.Driver",
//    "jdbc:postgresql://s-msk-d-mlp-db1.raiffeisen.ru:5432/personal_portal_v2",
//    "perspdev1",
//    "123qwe"
//  )

  val xa = Transactor.fromDriverManager[IO](
    "org.postgresql.Driver",
    "jdbc:postgresql:personal_portal_v2",
    "postgres",
    "postgres"
  )

  case class Passport(
                       series: Option[String],
                       number: Option[String],
                       unitCode: Option[String],
                       issuedDate: Option[LocalDate],
                       issuedBy: Option[String]
                     )
  case class Agreement(
                        report: Option[Boolean],
                        condition: Option[Boolean],
                        dataProcessing: Option[Boolean],
                        publicPerson: Option[Boolean],
                        bankruptcy: Option[Boolean]
                      )
  case class Work (companyName: Option[String],
                   companyActivity: Option[String],
                   companyStaffCount: Option[String],
                   companyPhone: Option[String],
                   position: Option[String],
                   start: Option[Instant],
                   seniority: Option[Int],
                   companyAddress: Option[FormAddress]
                  )
  case class FormAddress(
                          postCode: Option[String],
                          countryCode: Option[String],
                          building: Option[String],
                          house: Option[String],
                          block: Option[String],
                          apartment: Option[String],
                          fullAddress: Option[String],
                          region: Option[AddressObject],
                          district: Option[AddressObject],
                          city: Option[AddressObject],
                          settlement: Option[AddressObject],
                          street: Option[AddressObject],
                          kladrCode: Option[String]
                        )
  case class AddressObject (code: Option[String], nameWithTitleRu: Option[String])

  case class Borrower(id: Option[String] = None,
                      firstName: String,
                      lastName: String,
                      middleName: Option[String] = None,
                      phone: String,
                      birthDate: Option[LocalDate] = None,
                      email: String,
                      isPayroll: Option[Boolean],
                      borrowerType: Int,
                      applicationId: String,
                      createTime: Option[Instant] = None,
                      updateTime: Option[Instant] = None,
                      loanProgram: Option[Int] = None ,
                      maritalStatus: Option[Int] = None,
                      businessOwner: Option[Boolean] = None,
                      taxType: Option[Int] = None,
                      marriageContract: Option[Int] = None,
                      financePeriod: Option[Int] = None,
                      investor: Option[Boolean] = None,
                      snils: Option[String] = None,
                      livingAddress: Option[FormAddress] = None,
                      registrationAddress: Option[FormAddress] = None,
                      childrenCount: Option[Int] = None,
                      work: Option[Work] = None,
                      agreement: Option[Agreement] = None,
                      passport: Option[Passport] = None
                     )

  sql"""
    select *
    from borrowers
    limit 10
  """
  .query[Borrower]    // Query0[String]
  .stream
  .take(10)
  .compile.toList
  .transact(xa)
//  .map(_.toMap)
  .unsafeRunSync    // List[String]
  .foreach(println) // Unit

  def insertBorrower(borr: Borrower): ConnectionIO[Borrower] = {
    val id = UUID
      .randomUUID()
      .toString
      .replace("-", "")
      .substring(0, 30)
    val sql = sql"""
      insert into borrowers (
        id, first_name, last_name,
        middle_name, phone, birth_date,
        email, is_payroll, borrower_type_id,
        application_id, created_at
      )
      values (
        $id, ${borr.firstName}, ${borr.lastName},
        ${borr.middleName}, ${borr.phone}, ${borr.birthDate},
        ${borr.email}, ${borr.isPayroll}, ${borr.borrowerType},
        ${borr.applicationId}, NOW()
      )
    """
    for {
      _  <- sql.update.run
//      id <- sql"select lastval()".query[String].unique
      borr  <- sql"select * from borrowers where id = $id".query[Borrower].unique
    } yield borr
  }

  def updateBorrower(borr: Borrower): ConnectionIO[Borrower] = {
    val sql = sql"""
      update borrowers
        set
      (
        id, first_name, last_name,
        middle_name, phone, birth_date,
        email, is_payroll, borrower_type_id,
        application_id, created_at
      )
      values (
        $id, ${borr.firstName}, ${borr.lastName},
        ${borr.middleName}, ${borr.phone}, ${borr.birthDate},
        ${borr.email}, ${borr.isPayroll}, ${borr.borrowerType},
        ${borr.applicationId}, NOW()
      )
    """
    for {
      _  <- sql.update.run
      //      id <- sql"select lastval()".query[String].unique
      borr  <- sql"select * from borrowers where id = $id".query[Borrower].unique
    } yield borr
  }


  println(insertBorrower(Borrower(firstName = "Сергей",
    lastName = "Самойлов",
    phone = "+79190325095",
    email = "adsad@maill.ru",
    borrowerType = 2,
    applicationId = "MLO240419000000014",
    isPayroll = Some(true)
  )).transact(xa).unsafeRunSync())
}
