package ru.faoxis.notifier.logger

trait Logger[F[_]] {
  def error(msg: String): F[Unit]
  def info(msg: String): F[Unit]
}

object Logger {
  def apply[F[_]](implicit logger: Logger[F]): Logger[F] = logger
}
