package ru.faoxis.notifier

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import cats.Monad
import cats.data.EitherT
import cats.effect.IO
import ru.faoxis.notifier.client.joke.{AkkaHttpJokeClient, JokeClient}
import ru.faoxis.notifier.model.Joke
import cats.syntax.flatMap._
import com.typesafe.config.ConfigFactory
import ru.faoxis.notifier.client.telegram.{AkkaHttpTelegramClient, TelegramClient}
import ru.faoxis.notifier.logger.{FutureLogger, Logger}

import language.higherKinds
import scala.concurrent.Future

object NotifierApp extends App {

  val config        = ConfigFactory.load()
  val jokeUrl      = config.getString("urls.joke")

  implicit val system = ActorSystem("notifier")
  implicit val materializer = ActorMaterializer()

  implicit val akkaHttpJokeClient: JokeClient[IO] =
    new AkkaHttpJokeClient(jokeUrl)

  implicit val akkaHttpTelegramClient: TelegramClient[IO] =
    new AkkaHttpTelegramClient("-303941202")

  implicit val futureLogger: Logger[Future] =
    new FutureLogger

//  (for {
//    joke <- JokeClient[IO].getJoke()
//    _ <- IO { Logger[Future].info(joke.toString) }
//    jokeObj = joke match {
//      case Right(value) => value
//    }
//    res <- TelegramClient[IO].sendMessage("-303941202", jokeObj.value.joke)
//    //    jokeObj
////    _ <- TelegramClient[IO].sendMessage("-303941202", joke.value.joke)
//  } yield res).unsafeRunSync()


  TelegramClient[IO]
    .sendMessage("-303941202", "Heeey!")
    .unsafeRunSync()

//  println(JokeClient[IO].getJoke().unsafeRunSync())
}
