package ru.faoxis.notifier.client.telegram

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import cats.effect.IO

import scala.concurrent.ExecutionContext
import scala.util.Try

class AkkaHttpTelegramClient(token: String)(implicit val system: ActorSystem) extends TelegramClient[IO] {

  val url: String = "https://api.telegram.org/bot" + token + "/getUpdates"

  override def sendMessage(chatId: String, message: String): IO[Either[Throwable, String]] = {
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    implicit val ec: ExecutionContext = system.dispatcher

    IO.fromFuture(IO {
      Http()
        .singleRequest(HttpRequest(uri = url))
        .flatMap(Unmarshal(_).to[String])
        .map(Try(_).toEither)
    })
  }
}
