package ru.faoxis.notifier

import spray.json.DefaultJsonProtocol



object model extends DefaultJsonProtocol {
  final case class Joke(`type`: String, value: Value)
  final case class Value(id: Int, joke: String)

  implicit val valueFormat = jsonFormat(Value, "id", "joke")
  implicit val jokeFormat = jsonFormat(Joke, "type", "value")
}
