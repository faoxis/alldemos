import org.scalatest.{FlatSpec, Matchers}
import ru.faoxis.notifier.model.Joke
import spray.json._

class ChuckJokeParseSpec extends FlatSpec with Matchers {
  "A joke message" should "be convertable from json" in {
    val joke = json.parseJson.convertTo[Joke]

    joke.`type` should be ("success")
    joke.value.id should be (388)
    joke.value.joke should be ("They didn't even come close.")
  }


  val json =
    """
      |{
      |   "type":"success",
      |   "value":{
      |      "id":388,
      |      "joke":"They didn't even come close.",
      |      "categories":[
      |
      |      ]
      |   }
      |}
    """.stripMargin
}