package ru.faoxis.telegramBot.value

import org.scalatest.{FlatSpec, Matchers}
import spray.json._
import ru.faoxis.telegramBot.value.MessageWrapperProtocol._

class TelegramMessageSpec extends FlatSpec with Matchers {

  "A telegram message" should "be convertable from json" in {
    val telegramMessage = json.parseJson.convertTo[MessageWrapper]
    telegramMessage.ok should be (true)

    val result = telegramMessage.result.head
    result.updateId should be (7072654)

    val message = result.message.get
    message.messageId should be (97)
    message.date should be (1551548621)
    message.text should be ("ghb")

    val from = message.from
    from.id should be (126927462)
    from.isBot should be (false)
    from.firstName should be ("Sergei")
    from.lastName should be ("Samoilov")
    from.username should be ("faoxis")
    from.languageCode should be ("en")

    val chat = message.chat
    chat.id should be (126927462)
    chat.firstName should be ("Sergei")
    chat.lastName should be ("Samoilov")
    chat.username should be ("faoxis")
    chat.`type` should be ("private")
  }

  val json =
    """
      |{
      |   "ok":true,
      |   "result":[
      |      {
      |         "update_id":7072654,
      |         "message":{
      |            "message_id":97,
      |            "from":{
      |               "id":126927462,
      |               "is_bot":false,
      |               "first_name":"Sergei",
      |               "last_name":"Samoilov",
      |               "username":"faoxis",
      |               "language_code":"en"
      |            },
      |            "chat":{
      |               "id":126927462,
      |               "first_name":"Sergei",
      |               "last_name":"Samoilov",
      |               "username":"faoxis",
      |               "type":"private"
      |            },
      |            "date":1551548621,
      |            "text":"ghb"
      |         }
      |      }
      |   ]
      |}
    """.stripMargin

}
