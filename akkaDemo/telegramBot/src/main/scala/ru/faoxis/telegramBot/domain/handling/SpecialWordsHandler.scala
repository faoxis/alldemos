package ru.faoxis.telegramBot.domain.handling

import akka.actor.{Actor, ActorRef, Props}
import ru.faoxis.telegramBot.value.Message
import SpecialWordsHandler._
import ru.faoxis.telegramBot.domain.handling.Sender.Send

object SpecialWordsHandler {
  def props = Props(new SpecialWordsHandler)

  final case class Handle(message: Message)
}

class SpecialWordsHandler extends Actor {

  val messageSender: ActorRef = context.actorOf(Sender.props, "message-sender")

  override def receive: Receive = {
    case Handle(message) =>
      if (List("пардон", "извините", "пожалуйста").contains(message.text.toLowerCase())) {
        messageSender ! Send(message.chat.id, "Какая вежливость!")
      }
  }
}
