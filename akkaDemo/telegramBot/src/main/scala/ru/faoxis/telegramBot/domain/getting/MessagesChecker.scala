package ru.faoxis.telegramBot.domain.getting

import akka.actor.{Actor, ActorRef, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.ActorMaterializer
import ru.faoxis.telegramBot.domain.getting.MessageParser.Parse

import scala.concurrent.Future
import scala.concurrent.duration._

object MessagesChecker {
  def props(implicit materializer: ActorMaterializer) = Props(new MessagesChecker)

  object CheckMessages
}

class MessagesChecker(implicit materializer: ActorMaterializer) extends Actor {
  import MessagesChecker._
  import akka.pattern.pipe
  import context.{dispatcher, system}

  val token: String = system.settings.config.getString("telegramToken")
  val url: String = "http://165.22.73.204/telegram/bot" + token + "/getUpdates"
  val parser: ActorRef = context.actorOf(MessageParser.props, "parser")

  context
    .system
    .scheduler
    .schedule(
      0 milliseconds,
      1 seconds,
      self,
      CheckMessages
    )

  override def receive: Receive = {

    case CheckMessages =>
      val telegramResponse: Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = url))
      telegramResponse.map(x => Parse(x)) pipeTo parser


  }
}
