package ru.faoxis.telegramBot.domain.handling

import akka.actor.{Actor, Props}
import ru.faoxis.telegramBot.domain.handling.CommonMessageHandler._
import ru.faoxis.telegramBot.value.Message

object CommonMessageHandler {
  def props = Props(new CommonMessageHandler)

  final case class Handle(message: Message)
}

class CommonMessageHandler extends Actor {

  val specialWordsHandler = context
    .actorOf(SpecialWordsHandler.props, "special-words-handler")

  override def receive: Receive = {
    case Handle(message) =>
      specialWordsHandler ! SpecialWordsHandler.Handle(message)
  }
}
