import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import ru.faoxis.telegramBot.domain.getting.MessagesChecker


object BotApp extends App {
  implicit val system = ActorSystem("bot-system")
  implicit val materializer = ActorMaterializer()
  println("heeey!")

  implicit val ex = system.dispatcher
  system.actorOf(MessagesChecker.props, "checker")
//  println(system.settings.config.getString("telegramToken"))

}
