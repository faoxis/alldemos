package ru.faoxis.telegramBot.domain.getting

import akka.actor.{Actor, Props}
import ru.faoxis.telegramBot.domain.handling.CommonMessageHandler
import ru.faoxis.telegramBot.value.{Message, MessageWrapper}

object MessageUniqueFilter {
  def props = Props(new MessageUniqueFilter())

  case class Filter(wrapper: MessageWrapper)
}

class MessageUniqueFilter extends Actor {
  import MessageUniqueFilter._
  import CommonMessageHandler._

  val handler = context.actorOf(CommonMessageHandler.props, "handler")

  var messages = Set[Message]()

  override def receive: Receive = {
    case Filter(wrapper) =>
      wrapper
        .result
        .map(_.message)
        .filter(_.isDefined)
        .map(_.get)
        .foreach { msg =>
          if (!messages.contains(msg)) {
            messages = messages + msg
            handler ! Handle(msg)
          }
        }
  }
}
