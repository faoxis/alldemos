package ru.faoxis.telegramBot.domain.handling

import akka.actor.{Actor, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest}
import Sender._
import akka.event.Logging

import scala.util.{Failure, Success}

object Sender {
  def props = Props(new Sender)

  final case class Send(chatId: Int, text: String)
}

class Sender extends Actor {
  import context.system
  import context.dispatcher

  val log = Logging(context.system, this)

  private val token = context.system.settings.config.getString("telegramToken")
  val url = s"http://165.22.73.204/telegram/bot$token/sendMessage"

  override def receive: Receive = {
    case Send(chatId, text) =>
      val msg = s"""{"chat_id": "$chatId", "text": "$text"}"""
      Http(system).singleRequest(
        HttpRequest(
          HttpMethods.POST,
          url,
          entity = HttpEntity(
            ContentTypes.`application/json`,
            msg.getBytes())
        )
      ).onComplete {
        case Success(_) => log.info(s"Message was sent: $msg")
        case Failure(exception) => log.error(s"Error with sending message: ${exception.getMessage}")
      }
  }
}
