package ru.faoxis.telegramBot.domain.getting

import akka.actor.{Actor, ActorRef, Props}
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import ru.faoxis.telegramBot.value.MessageWrapper
import ru.faoxis.telegramBot.value.MessageWrapperProtocol._
import spray.json._

import scala.util.{Failure, Success}


object MessageParser {
  def props(implicit materializer: ActorMaterializer) = Props(new MessageParser())

  final case class Parse(httpResponse: HttpResponse)
}

class MessageParser(implicit val materializer: ActorMaterializer) extends Actor {
  import context.dispatcher
  import MessageParser._
  import MessageUniqueFilter._

  val uniqueMessageFinder: ActorRef = context.actorOf(MessageUniqueFilter.props, "filter")

  override def receive: Receive = {
    case Parse(response) =>
      Unmarshal(response)
        .to[String]
        .map(_.parseJson.convertTo[MessageWrapper])
        .onComplete {
          case Success(res) => uniqueMessageFinder ! Filter(res)
          case Failure(error) => println(error)
        }
  }
}
