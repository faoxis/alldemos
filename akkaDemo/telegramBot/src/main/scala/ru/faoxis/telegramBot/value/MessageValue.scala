package ru.faoxis.telegramBot.value

import spray.json._

final case class MessageWrapper(ok: Boolean, result: List[Result])
final case class Result (updateId: Int, message: Option[Message])
final case class Message (messageId: Int, from: MessageFrom, chat: MessageChat, date: Long, text: String)
final case class MessageFrom (
                               id: Long,
                               isBot: Boolean,
                               firstName: Option[String],
                               lastName: Option[String],
                               username: Option[String],
                               languageCode: Option[String])
final case class MessageChat (
                               id: Int,
                               firstName: Option[String],
                               lastName: Option[String],
                               username: Option[String],
                               `type`: String
                             )

final case class Answer(chatId: Int, text: String)

object MessageWrapperProtocol extends DefaultJsonProtocol {

  implicit val answer = jsonFormat(Answer, "chat_id", "text")

  implicit val messageChatFormat = jsonFormat(MessageChat,
    "id", "first_name", "last_name", "username", "type"
  )

  implicit val telegramMessageFromFormat = jsonFormat(MessageFrom,
    "id", "is_bot", "first_name", "last_name", "username", "language_code")
  implicit val telegramMessageFormat = jsonFormat(Message, "message_id", "from", "chat", "date", "text")
  implicit val telegramResultFormat = jsonFormat(Result, "update_id", "message")
  implicit val telegramMessageWrapperFormat = jsonFormat(MessageWrapper, "ok", "result")
}



